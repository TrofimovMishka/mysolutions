package com.javarush.task.task20.task2021;

import javax.naming.Context;
import java.io.*;

/* 
Сериализация под запретом
*/

public class Solution implements Serializable {
    public static class SubSolution extends Solution {
       private void readObject(ObjectInput oi) throws IOException {
           throw  new NotSerializableException("NOt");
       }
        private void writeObject(ObjectOutput oo) throws IOException {
            throw  new NotSerializableException("NOt");
        }
    }

    public static void main(String[] args) {

    }
}
