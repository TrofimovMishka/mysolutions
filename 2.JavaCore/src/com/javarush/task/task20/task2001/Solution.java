package com.javarush.task.task20.task2001;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* 
Читаем и пишем в файл: Human
C:\Users\Mike\OneDrive\Рабочий стол\first.txt
*/

public class Solution {
    public static void main(String[] args) {
        //исправьте outputStream/inputStream в соответствии с путем к вашему реальному файлу
        try {
            File your_file_name =
                    File.createTempFile("C:\\Users\\Mike\\OneDrive\\Рабочий стол\\first.txt", null);
            OutputStream outputStream = new FileOutputStream(your_file_name);
            InputStream inputStream = new FileInputStream(your_file_name);

            Human ivanov = new Human("Ivanov");
            ivanov.save(outputStream);
            outputStream.flush();

            Human somePerson = new Human();
            somePerson.load(inputStream);
            inputStream.close();
            //check here that ivanov equals to somePerson - проверьте тут, что ivanov и somePerson равны
            System.out.println(somePerson.equals(ivanov));

        } catch (IOException e) {
            //e.printStackTrace();
            System.out.println("Oops, something wrong with my file");
        } catch (Exception e) {
            //e.printStackTrace();
            System.out.println("Oops, something wrong with save/load method");
        }
    }

    public static class Human {
        public String name;
        public List<Asset> assets = new ArrayList<>();

        public Human() {
        }

        public Human(String name, Asset... assets) {
            this.name = name;
            if (assets != null) {
                this.assets.addAll(Arrays.asList(assets));
            }
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Human human = (Human) o;

            if (name != null ? !name.equals(human.name) : human.name != null) return false;
            return assets != null ? assets.equals(human.assets) : human.assets == null;
        }

        @Override
        public int hashCode() {
            int result = name != null ? name.hashCode() : 0;
            result = 31 * result + (assets != null ? assets.hashCode() : 0);
            return result;
        }

        public void save(OutputStream outputStream) throws Exception {
            //implement this method - реализуйте этот метод
            outputStream.write(this.name.getBytes());
            outputStream.write("\n".getBytes());
            String isAssetPresent = !assets.isEmpty() ? "yes" : "no";
            outputStream.write(isAssetPresent.getBytes());
            outputStream.write("\n".getBytes());

            if (!assets.isEmpty()) {
                for (Asset as :
                        this.assets) {
                    String name = as.getName() + " ";
                    String salary = as.getPrice() + " ";
                    outputStream.write(name.getBytes());
                    outputStream.write(salary.getBytes());
                }
            }
        }

        public void load(InputStream inputStream) throws Exception {
            //implement this method - реализуйте этот метод
            StringBuilder sb = new StringBuilder();
            int i = -1;
            while ((i = inputStream.read()) != -1) { // read all File
                sb.append((char) i);
            }

            String[] arr = sb.toString().split("\n");
            String nameFromFile = "";
            if (arr.length > 0) {
                nameFromFile = arr[0];
            }

            List<Asset> assetFromFile = new ArrayList<>();
            for (int j = 1; j < arr.length; j++) {
                if (j % 2 == 1) {
                    if (arr[j].equals("yes")) {
                        String[] array = arr[j + 1].split(" ");
                        for (int k = 1; k < array.length; k+=2) {
                            assetFromFile.add(new Asset(array[k - 1], Double.parseDouble(array[k].trim())));
                        }
                    }
                }
            }

            this.name = nameFromFile;
            if(!assetFromFile.isEmpty()){
                this.assets = assetFromFile;
            }
        }
    }
}
/* JR
public void save(OutputStream outputStream) throws Exception {
            //implement this method - реализуйте этот метод
            PrintWriter printWriter = new PrintWriter(outputStream);
            printWriter.println(this.name);
            if (this.assets.size() > 0) {
                for (Asset current : this.assets) {
                    printWriter.println(current.getName());
                    printWriter.println(current.getPrice());
                }
            }
            printWriter.close();
        }

        public void load(InputStream inputStream) throws Exception {
            //implement this method - реализуйте этот метод
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            this.name = reader.readLine();
            while (reader.ready()) {
                String assetName = reader.readLine();
                double assetPrice = Double.parseDouble(reader.readLine());
                this.assets.add(new Asset(assetName, assetPrice));
            }
            reader.close();
        }
    }
 */
