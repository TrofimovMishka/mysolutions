package com.javarush.task.task20.task2026;

/* 
Алгоритмы-прямоугольники
*/

public class Solution {
    public static void main(String[] args) {
        byte[][] a1 = new byte[][]{
                {1, 1, 0, 1},
                {1, 1, 0, 0},
                {1, 1, 0, 0},
                {1, 1, 0, 1}
        };
        byte[][] a2 = new byte[][]{
                {1, 0, 0, 1},
                {0, 0, 0, 0},
                {0, 0, 0, 0},
                {1, 0, 0, 1}
        };

        int count1 = getRectangleCount(a1);
        System.out.println("count = " + count1 + ". Должно быть 2");
        int count2 = getRectangleCount(a2);
        System.out.println("count = " + count2 + ". Должно быть 4");
    }

    public static int getRectangleCount(byte[][] a) {
        byte b [][] = a.clone();
        int count =0;

        for (int i = 0; i < b.length; i++) {
            for (int j = 0; j < b[i].length; j++) {
                if(b[i][j]==1){
                    count++;
                    for (int k = j; k < b[i].length; k++) {
                        if(b[i][k]==1){
                            for (int e = i; e < b.length; e++) {
                                if(b[e][k]==1){
                                    b[e][k]=0;
                                }else{
                                    break;
                                }
                            }
                            b[i][k]=0;
                        }else{
                            break;
                        }
                    }
                }
            }
        }
        return count;
    }
}
