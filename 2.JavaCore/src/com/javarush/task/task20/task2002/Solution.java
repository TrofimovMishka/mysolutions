package com.javarush.task.task20.task2002;

import java.io.*;
import java.util.*;

/* 
Читаем и пишем в файл: JavaRush
*/

public class Solution {
    public static void main(String[] args) {
        //you can find your_file_name.tmp in your TMP directory or adjust outputStream/inputStream according to your
        // file's actual location
        //вы можете найти your_file_name.tmp в папке TMP или исправьте outputStream/inputStream
        // в соответствии с путем к вашему реальному файлу
        try {
            File yourFile = File.createTempFile("C:\\Users\\Mike\\OneDrive\\Рабочий стол\\testWriteObject", null);
            OutputStream outputStream = new FileOutputStream(yourFile);
            InputStream inputStream = new FileInputStream(yourFile);

            JavaRush javaRush = new JavaRush();
            //initialize users field for the javaRush object here - инициализируйте поле users для объекта javaRush тут
//            User petrov = new User();
//            petrov.setFirstName("Petro");
//            petrov.setLastName("Petrov");
//            petrov.setBirthDate(new GregorianCalendar(2012, 11, 25).getTime());
//            petrov.setMale(true);
//            petrov.setCountry(User.Country.UKRAINE);
//
//            javaRush.users.add(petrov);

            javaRush.save(outputStream);
            outputStream.flush();

            JavaRush loadedObject = new JavaRush();
            loadedObject.load(inputStream);
            //here check that the javaRush object is equal to the loadedObject object - проверьте тут,
            // что javaRush и loadedObject равны
            System.out.println(javaRush.equals(loadedObject));

            outputStream.close();
            inputStream.close();

        } catch (IOException e) {
            //e.printStackTrace();
            System.out.println("Oops, something is wrong with my file");
        } catch (Exception e) {
            //e.printStackTrace();
            System.out.println("Oops, something is wrong with the save/load method");
        }
    }

    public static class JavaRush {
        public List<User> users = new ArrayList<>();

        public void save(OutputStream outputStream) throws Exception {
            //implement this method - реализуйте этот метод
            try (PrintWriter printWriter = new PrintWriter(outputStream)) {
                if (!users.isEmpty()) {
                    for (User user :
                            users) {
                        printWriter.println(user.getFirstName());
                        printWriter.println(user.getLastName());
                        printWriter.println(user.getBirthDate().getTime());
                        printWriter.println(user.isMale());
                        printWriter.println(user.getCountry().getDisplayName());
                    }
                }
            }
        }

        public void load(InputStream inputStream) throws Exception {
            //implement this method - реализуйте этот метод

            try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
                while (reader.ready()) {
                    User person = new User();
                    person.setFirstName(reader.readLine());
                    person.setLastName(reader.readLine());

                    Date date = new Date();
                    date.setTime(Long.parseLong(reader.readLine()));
                    person.setBirthDate(date);

                    String isMale_ = reader.readLine();
                    if (isMale_.equals("true")) {
                        person.setMale(true);
                    } else {
                        person.setMale(false);
                    }
                    switch (reader.readLine()) {
                        case "Ukraine":
                            person.setCountry(User.Country.UKRAINE);
                            break;
                        case "Russia":
                            person.setCountry(User.Country.RUSSIA);
                            break;
                        case "Other":
                            person.setCountry(User.Country.OTHER);
                            break;
                    }
                    users.add(person);
                }
            }
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            JavaRush javaRush = (JavaRush) o;

            return users != null ? users.equals(javaRush.users) : javaRush.users == null;

        }

        @Override
        public int hashCode() {
            return users != null ? users.hashCode() : 0;
        }
    }
}
/*
public void save(OutputStream outputStream) throws Exception {
            //implement this method - реализуйте этот метод
            PrintWriter printWriter = new PrintWriter(outputStream);
            printWriter.println(this.name);
            if (this.assets.size() > 0) {
                for (Asset current : this.assets) {
                    printWriter.println(current.getName());
                    printWriter.println(current.getPrice());
                }
            }
            printWriter.close();
        }

        public void load(InputStream inputStream) throws Exception {
            //implement this method - реализуйте этот метод
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            this.name = reader.readLine();
            while (reader.ready()) {
                String assetName = reader.readLine();
                double assetPrice = Double.parseDouble(reader.readLine());
                this.assets.add(new Asset(assetName, assetPrice));
            }
            reader.close();
        }
    }
 */
