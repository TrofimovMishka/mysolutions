package com.javarush.task.task20.task2027;

import java.util.ArrayList;
import java.util.List;

/* 
Кроссворд
*/

public class Solution {
    public static void main(String[] args) {
        int[][] crossword = new int[][]{
                {'s', 'd', 'e', 'r', 'l', 'k'},
                {'u', 'a', 'a', 'e', 'e', 'o'},
                {'l', 'n', 'm', 'r', 'm', 'v'},
                {'m', 'o', 'p', 'e', 'o', 'h'},
                {'h', 'o', 'e', 'e', 'h', 'j'}
        };
        detectAllWords(crossword, "home", "same");
        /*
Ожидаемый результат
home - (5, 3) - (2, 0)
same - (1, 1) - (4, 1)
         */
    }

    public static List<Word> detectAllWords(int[][] crossword, String... words) {
        List<Word> allWords = new ArrayList<>();

        for(String s: words){
            for (int j = 0; j < crossword.length; j++) {
                for (int k = 0; k < crossword[j].length; k++) {
                    if(crossword[j][k] == s.charAt(0)){
                        allWords.addAll(horisontalRight(k, j, s, crossword));
                        allWords.addAll(horisontalLeft(k, j, s, crossword));
                        allWords.addAll(verticalDown(k, j, s, crossword));
                        allWords.addAll(verticalUp(k, j, s, crossword));
                        allWords.addAll(diagonalRDown(k, j, s, crossword));
                        allWords.addAll(diagonalRUp(k, j, s, crossword));
                        allWords.addAll(diagonalLUp(k, j, s, crossword));
                        allWords.addAll(diagonalLDown(k, j, s, crossword));
                    }
                }
            }
        }

        return allWords;
    }
    // x - horisont------
    // y - vertical |||||
    public static List<Word> diagonalLDown(int horisontal, int vertical, String word, int[][] arr){
        List<Word> dRDWords = new ArrayList<>();

        int endX =-1;
        int endY =-1;
        int len = word.length();
        StringBuilder sb = new StringBuilder();

        if(len<=(arr.length-vertical) && (horisontal - len+1)>=0){
            for (int j = vertical, i = horisontal; j < vertical+len || i>= horisontal-len+1; j++, i--) {
                sb.append((char)arr[j][i]);
            }
        }
        if(sb.toString().equals(word)){
            endX = horisontal-len+1;
            endY = vertical+len-1;

            //Create and add new obj. Word
            Word first = new Word(word);
            first.setStartPoint(horisontal, vertical);
            first.setEndPoint(endX, endY);
            dRDWords.add(first);
        }

        return dRDWords;
    }

    public static List<Word> diagonalLUp(int horisontal, int vertical, String word, int[][] arr){
        List<Word> dRDWords = new ArrayList<>();

        int endX =-1;
        int endY =-1;
        int len = word.length();
        StringBuilder sb = new StringBuilder();

        if((vertical-len+1)>=0 && (horisontal - len+1)>=0){
            for (int j = vertical, i = horisontal; j >= arr.length-len || i>= horisontal-len+1; j--, i--) {
                sb.append((char)arr[j][i]);
            }
        }
        if(sb.toString().equals(word)){
            endX = horisontal-len+1;
            endY = vertical-len+1;

            //Create and add new obj. Word
            Word first = new Word(word);
            first.setStartPoint(horisontal, vertical);
            first.setEndPoint(endX, endY);
            dRDWords.add(first);
        }

        return dRDWords;
    }
    public static List<Word> diagonalRUp(int horisontal, int vertical, String word, int[][] arr){
        List<Word> dRDWords = new ArrayList<>();

        int endX =-1;
        int endY =-1;
        int len = word.length();
        StringBuilder sb = new StringBuilder();

        if((vertical-len+1)>=0 && len<=(arr[vertical].length-horisontal)){
            for (int j = vertical, i = horisontal; j >= arr.length-len || i<len+horisontal; j--, i++) {
                sb.append((char)arr[j][i]);
            }
        }
        if(sb.toString().equals(word)){
            endX = horisontal+len-1;
            endY = vertical-len+1;

            //Create and add new obj. Word
            Word first = new Word(word);
            first.setStartPoint(horisontal, vertical);
            first.setEndPoint(endX, endY);
            dRDWords.add(first);
        }

        return dRDWords;
    }

    public static List<Word> diagonalRDown(int horisontal, int vertical, String word, int[][] arr){
        List<Word> dRDWords = new ArrayList<>();

        int endX =-1;
        int endY =-1;
        int len = word.length();
        StringBuilder sb = new StringBuilder();

        if(len<=(arr.length-vertical) && len<=(arr[vertical].length-horisontal)){
            for (int j = vertical, i = horisontal; j<vertical+len || i<len+horisontal; j++, i++) {
                sb.append((char)arr[j][i]);
            }
        }
        if(sb.toString().equals(word)){
            endX = horisontal+len-1;
            endY = vertical+len-1;

            //Create and add new obj. Word
            Word first = new Word(word);
            first.setStartPoint(horisontal, vertical);
            first.setEndPoint(endX, endY);
            dRDWords.add(first);
        }

        return dRDWords;
    }

    public static List<Word> verticalUp(int horisontal, int vertical, String word, int[][] arr){
        List<Word> vUpWords = new ArrayList<>();

        int endY=-1;
        int len = word.length();
        StringBuilder sb = new StringBuilder();

        if((vertical-len+1)>=0){
            for (int j = vertical; j >= arr.length-len; j--) {
                sb.append((char)arr[j][horisontal]);
            }
        }
        if(sb.toString().equals(word)){
            endY = vertical-len+1;

            //Create and add new obj. Word
            Word first = new Word(word);
            first.setStartPoint(horisontal, vertical);
            first.setEndPoint(horisontal, endY);
            vUpWords.add(first);

        }
        return vUpWords;
    }
    public static List<Word> verticalDown(int horisontal, int vertical, String word, int[][] arr){
        List<Word> vDownWords = new ArrayList<>();

        int endY=-1;
        int len = word.length();
        StringBuilder sb = new StringBuilder();

        if(len<=(arr.length-vertical)){
            for (int j = vertical; j < vertical+len; j++) {
                sb.append((char)arr[j][horisontal]);
            }
        }
        if(sb.toString().equals(word)){
            endY = vertical+len-1;

            //Create and add new obj. Word
            Word first = new Word(word);
            first.setStartPoint(horisontal, vertical);
            first.setEndPoint(horisontal, endY);
            vDownWords.add(first);

        }
        return vDownWords;
    }

    public static List<Word> horisontalRight(int horisontal, int vertical, String word, int[][] arr){
        List<Word> hRightWords = new ArrayList<>();

        int endX=-1;
        int len = word.length();
        StringBuilder sb = new StringBuilder();

        if(len<=(arr[vertical].length-horisontal)){
            for (int j = horisontal; j < len+horisontal; j++) {
                sb.append((char)arr[vertical][j]);
            }
        }
        if(sb.toString().equals(word)){
            endX = horisontal+len-1;

            //Create and add new obj. Word
            Word first = new Word(word);
            first.setStartPoint(horisontal, vertical);
            first.setEndPoint(endX, vertical);
            hRightWords.add(first);

        }
        return hRightWords;
    }
    public static List<Word> horisontalLeft(int horisontal, int vertical, String word, int[][] arr){
        List<Word> hLeftWords = new ArrayList<>();

        int endX=-1;
        int len = word.length();
        StringBuilder sb = new StringBuilder();

        if((horisontal - len+1)>=0){
            for (int j = horisontal; j >= horisontal-len+1; j--) {
                sb.append((char)arr[vertical][j]);
            }
        }
        if(sb.toString().equals(word)){
            endX = horisontal-len+1;
            //Create and add new obj. Word
            Word first = new Word(word);
            first.setStartPoint(horisontal, vertical);
            first.setEndPoint(endX, vertical);
            hLeftWords.add(first);
        }
        return hLeftWords;
    }



    public static class Word {
        private String text;
        private int startX;
        private int startY;
        private int endX;
        private int endY;

        public Word(String text) {
            this.text = text;
        }

        public void setStartPoint(int i, int j) {
            startX = i;
            startY = j;
        }

        public void setEndPoint(int i, int j) {
            endX = i;
            endY = j;
        }

        @Override
        public String toString() {
            return String.format("%s - (%d, %d) - (%d, %d)", text, startX, startY, endX, endY);
        }
    }
}