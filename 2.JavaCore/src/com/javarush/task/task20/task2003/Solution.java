package com.javarush.task.task20.task2003;

import java.io.*;
import java.util.*;

/* 
Знакомство с properties
C:\Users\Mike\Documents\JavaRushTasks\2.JavaCore\testForProperties.properties
{site=http://example.com, password=some*()Pasword_89, login=login@to@site}
*/

public class Solution {

    public static Map<String, String> runtimeStorage = new HashMap<>();
//    static {
//        runtimeStorage.put("site", "http://example.com");
//        runtimeStorage.put("password", "some*()Pasword_89");
//        runtimeStorage.put("login", "login@to@site}");
//    }

    public static void save(OutputStream outputStream) throws Exception {
        //напишите тут ваш код
        Properties storage = new Properties();
        storage.putAll(runtimeStorage);
        try (PrintWriter writer = new PrintWriter(outputStream)) {
            storage.store(writer,"");
        }
    }

    public static void load(InputStream inputStream) throws IOException {
        //напишите тут ваш код
        Properties storage = new Properties();
        storage.load(inputStream);
        Set allKeys = storage.keySet();
        Iterator itr = allKeys.iterator();
        String str;
        while(itr.hasNext()){
            str = (String)itr.next();
            runtimeStorage.put(str, storage.getProperty(str));
        }
    }

    public static void main(String[] args) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
             FileInputStream fos = new FileInputStream(reader.readLine())) {
            load(fos);
        } catch (IOException e) {
            e.printStackTrace();
        }

//        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//             FileOutputStream outputStream = new FileOutputStream(reader.readLine())) {
//            save(outputStream);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        System.out.println(runtimeStorage);
    }
}
