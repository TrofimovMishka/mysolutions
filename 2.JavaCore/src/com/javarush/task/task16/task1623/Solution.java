package com.javarush.task.task16.task1623;

/* 
Рекурсивное создание нитей
*/

import java.util.concurrent.locks.Lock;

public class Solution {
    static int count = 15;
    static volatile int createdThreadCount;

    public static void main(String[] args) throws InterruptedException {
        System.out.println(new GenerateThread());

    }

    public static class GenerateThread extends Thread {
    Object lock = new Object();

        public GenerateThread() {
            super(String.valueOf(++createdThreadCount));
            this.start();
        }

        @Override
        public void run() {
            if(Solution.createdThreadCount < Solution.count){
                System.out.println(new GenerateThread());
            }
        }

        @Override
        public String toString() {
            return this.getName()+" created";
        }

    }
}
