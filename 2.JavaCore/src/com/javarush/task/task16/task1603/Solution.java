package com.javarush.task.task16.task1603;

import java.util.ArrayList;
import java.util.List;

/* 
Список и нити
*/

public class Solution {
    public static volatile List<Thread> list = new ArrayList<Thread>(5);

    public static void main(String[] args) {
        //напишите тут ваш код
        SpecialThread thread1 = new SpecialThread();
        Thread thread11 = new Thread(thread1);
        list.add(thread11);
        SpecialThread thread2 = new SpecialThread();
        Thread thread22 = new Thread(thread2);
        list.add(thread22);
        SpecialThread thread3 = new SpecialThread();
        Thread thread33 = new Thread(thread3);
        list.add(thread33);
        SpecialThread thread4 = new SpecialThread();
        Thread thread44 = new Thread(thread4);
        list.add(thread44);
        SpecialThread thread5 = new SpecialThread();
        Thread thread55 = new Thread(thread5);
        list.add(thread55);

    }

    public static class SpecialThread implements Runnable {
        public void run() {
            System.out.println("it's a run method inside SpecialThread");
        }
    }
}
