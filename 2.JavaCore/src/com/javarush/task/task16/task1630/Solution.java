package com.javarush.task.task16.task1630;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/*
Последовательный вывод файлов

C:\Users\Mike\OneDrive\Рабочий стол\first.txt
C:\Users\Mike\OneDrive\Рабочий стол\second.txt

*/

public class Solution {
    public static String firstFileName;
    public static String secondFileName;

    //напишите тут ваш код
    static {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            firstFileName = reader.readLine();
            secondFileName = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) throws InterruptedException {
        systemOutPrintln(firstFileName);
        systemOutPrintln(secondFileName);

    }


    public static void systemOutPrintln(String fileName) throws InterruptedException {
        ReadFileThread f = new ReadFileThread();
        f.setFileName(fileName);
        f.start();
        f.join();
        f.interrupt();
        System.out.println(f.getFileContent());

    }

    public interface ReadFileInterface {

        void setFileName(String fullFileName);

        String getFileContent();

        void join() throws InterruptedException;

        void start();

    }

    //напишите тут ваш код
    public static class ReadFileThread extends Thread implements ReadFileInterface {
        private String fileName = " ";
        private List<String> list = new ArrayList<>();


        @Override
        public void setFileName(String fullFileName) {
            fileName = fullFileName;
        }

        @Override
        public String getFileContent() {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < list.size(); i++) {
                if (i != list.size() - 1) {
                    sb.append(list.get(i) + " ");
                } else {
                    sb.append(list.get(i));
                }
            }
            return sb.toString();
        }

        @Override
        public void run() {
            if (!(Thread.currentThread().isInterrupted())) {
                if (fileName != null) {
                    try (BufferedReader reader2 = new BufferedReader(new FileReader(fileName));
                    ) {
                        String s;
                        while ((s = reader2.readLine()) != null)
                            list.add(s);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
