package com.javarush.task.task16.task1632;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* 
Клубок
*/

public class Solution {
    public static List<Thread> threads = new ArrayList<>(5);

    static {
        threads.add(new One());
        threads.add(new Two());
        threads.add(new Three());
        threads.add(new Four());
        threads.add(new Five());
    }

    public static void main(String[] args) {
    }
}

class One extends Thread {

    @Override
    public void run() {
        int count = 1;

        while (true) {
            count--;
            count++;
        }
    }
}

class Two extends Thread {
    @Override
    public void run() {
        try {
            throw new InterruptedException();
        } catch (InterruptedException e) {
            System.out.println(e);
        }
    }
}

class Three extends Thread {
    @Override
    public void run() {
        while (!isInterrupted()) {
            try {
                System.out.println("Ура");
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
                this.interrupt();
            }
        }
    }
}

class Four extends Thread implements Message {
    @Override
    public void run() {
        while (true) {
            if (isInterrupted()) {
                return;
            }
        }
    }

    @Override
    public void showWarning() {
        this.interrupt();
    }
}

class Five extends Thread {
    @Override
    public void run() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String info;
        int sum = 0;
        while (!isInterrupted()) {
            try {
                while (!((info = reader.readLine()).equals("N"))) {
                    sum += Integer.parseInt(info);
                }
                System.out.println(sum);

            } catch (IOException e) {
                e.printStackTrace();
                this.interrupt();

            } catch (NumberFormatException ex) {
                ex.printStackTrace();
                this.interrupt();
            }
        }
    }
}