package com.javarush.task.task14.task1419;

import javax.naming.OperationNotSupportedException;
import java.io.*;
import java.lang.invoke.WrongMethodTypeException;
import java.util.ArrayList;
import java.util.List;

/* 
Нашествие исключений
*/

public class Solution {
    public static List<Exception> exceptions = new ArrayList<Exception>();

    public static void main(String[] args) {
        initExceptions();

        for (Exception exception : exceptions) {
            System.out.println(exception);
        }
    }

    private static void initExceptions() {   //the first exception
        try {
            float i = 1 / 0;

        } catch (Exception e) {
            exceptions.add(e);
        }
        try {
            String s =null;
            char a = s.charAt(4);
        } catch (NullPointerException e) {
            exceptions.add(e);
        }
        try {
            int [] ar = new int[1];
            int a = ar[10];

        } catch (ArrayIndexOutOfBoundsException e) {
            exceptions.add(e);
        }
        try {
            new BufferedReader(new FileReader("C:\\file.txt"));

        } catch (FileNotFoundException e) {
            exceptions.add(e);
        }
        exceptions.add(new IllegalArgumentException());
        exceptions.add(new ClassCastException());
        exceptions.add(new IllegalStateException());
        exceptions.add(new NegativeArraySizeException());
        exceptions.add(new OperationNotSupportedException());
        exceptions.add(new WrongMethodTypeException());

        //напишите тут ваш код
    }
}
