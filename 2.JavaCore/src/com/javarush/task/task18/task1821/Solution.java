package com.javarush.task.task18.task1821;

import java.io.*;
import java.util.*;

/* 
Встречаемость символов
"C:\Users\Mike\OneDrive\Рабочий стол\first.txt"
*/

public class Solution {
    public static void main(String[] args){
        try (FileInputStream reader = new FileInputStream(args[0])) {
            List<Byte> arrayList = new ArrayList<>();
            while (reader.available()>0){
                arrayList.add((byte)reader.read());
            }
            Collections.sort(arrayList);
            Set<Byte> bytes = new TreeSet<>();
            bytes.addAll(arrayList);

            bytes.stream().forEach(ele -> {
                int em = ele;
                int count = 0;
                for (int i = 0; i < arrayList.size(); i++) {
                    if(arrayList.get(i)==ele){
                        count++;
                    }
                }
                System.out.println((char)em +" "+count);
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
