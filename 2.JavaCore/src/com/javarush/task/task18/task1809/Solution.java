package com.javarush.task.task18.task1809;

import java.io.*;

/* 
Реверс файла
C:\Users\Mike\OneDrive\Рабочий стол\first.txt
C:\Users\Mike\OneDrive\Рабочий стол\second.txt
*/

public class Solution {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            String file1 = reader.readLine();
            String file2 = reader.readLine();

            try (FileInputStream inputStream = new FileInputStream(file1);
                 FileOutputStream outputStream = new FileOutputStream(file2)) {
                byte [] buffer = new byte[inputStream.available()];
                inputStream.read(buffer);
                for (int i = buffer.length-1; i >= 0 ; i--) {
                    outputStream.write(buffer[i]);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
