package com.javarush.task.task18.task1825;

import java.io.*;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

/* 
Собираем файл
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        TreeSet<String> strings = new TreeSet<>();
        String fileName;
        while(!(fileName= reader.readLine()).equals("end")){
            strings.add(fileName);
        }
        if(!strings.isEmpty()){
            int lastDot = strings.first().lastIndexOf(".");
            String newFile = strings.first().substring(0, lastDot);
            strings.stream().forEach(ele -> {
                try(BufferedReader bufferedReader = new BufferedReader(new FileReader(ele));
                    BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(newFile, true))) {
                    while(bufferedReader.ready()){
                       bufferedWriter.write(bufferedReader.read());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }
}
