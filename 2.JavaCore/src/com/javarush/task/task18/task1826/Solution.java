package com.javarush.task.task18.task1826;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/* 
Шифровка
-e "C:\Users\Mike\OneDrive\Рабочий стол\first.txt" "C:\Users\Mike\OneDrive\Рабочий стол\second.txt"
-d "C:\Users\Mike\OneDrive\Рабочий стол\second.txt" "C:\Users\Mike\OneDrive\Рабочий стол\tree.txt"

*/

public class Solution {
    public static void main(String[] args) throws IOException {
        try (FileInputStream inputStream = new FileInputStream(args[1]);
             FileOutputStream outputStream = new FileOutputStream(args[2])) {

            switch (args[0]) {
                case "-e":
                    while (inputStream.available() > 0) {
                        outputStream.write(inputStream.read() + 22);
                    }
                case "-d":
                    while (inputStream.available() > 0) {
                        outputStream.write(inputStream.read() - 22);
                    }
            }
        }
    }
}
