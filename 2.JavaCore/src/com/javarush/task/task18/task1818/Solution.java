package com.javarush.task.task18.task1818;

import java.io.*;

/* 
Два в одном
C:\Users\Mike\OneDrive\Рабочий стол\first.txt
C:\Users\Mike\OneDrive\Рабочий стол\second.txt
C:\Users\Mike\OneDrive\Рабочий стол\tree.txt
*/

public class Solution {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            String file1 = reader.readLine();
            String file2 = reader.readLine();
            String file3 = reader.readLine();

            try (FileInputStream inputStream1 = new FileInputStream(file2);
                 FileInputStream inputStream2 = new FileInputStream(file3);
                 FileOutputStream outputStream = new FileOutputStream(file1, true)) {
                byte [] array1 = new byte[inputStream1.available()];
                byte [] array2 = new byte[inputStream2.available()];
                inputStream1.read(array1);
                inputStream2.read(array2);
                outputStream.write(array1);
                outputStream.write(array2);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
