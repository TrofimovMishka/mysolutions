package com.javarush.task.task18.task1817;

import java.io.FileInputStream;
import java.io.IOException;

/* 
Пробелы
"C:\Users\Mike\OneDrive\Рабочий стол\first.txt"
*/

public class Solution {
    public static void main(String[] args) {
        String nameFile = args[0];
        try (FileInputStream inputStream = new FileInputStream(nameFile)) {
            double spaces = 0;
            double allChar = 0;
            while (inputStream.available() > 0) {
                int ch = inputStream.read();
                allChar++;
                if (ch == 32) {
                    spaces++;
                }
            }

            double result = spaces / allChar * 100;
            System.out.printf("%.2f", result);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
