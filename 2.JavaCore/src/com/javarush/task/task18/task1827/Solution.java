package com.javarush.task.task18.task1827;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/* 
Прайсы

-c "Шорты пляжные червоные" 165.39 22
C:\Users\Mike\OneDrive\Рабочий стол\first.txt
C:\Users\Mike\OneDrive\Рабочий стол\first.txt
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = reader.readLine();

        List<String> arrayList = new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            while (bufferedReader.ready()) {
                arrayList.add(bufferedReader.readLine());
            }
        }
        int newID = 0;
        for (int i = 0; i < arrayList.size(); i++) {
            String idString = arrayList.get(i).substring(0, 8);
            int temp = Integer.parseInt(idString.trim());
            if(temp > newID){
                newID = temp;
            }
        }

        FileOutputStream outputStream = new FileOutputStream(fileName, true);
        if(args.length>0) {
            switch (args[0]) {
                case "-c":
                    newID++;
                    String nextID = Integer.toString(newID);
                    while (nextID.length() < 8) {
                        nextID += " ";
                    }
                    String productName = args[1];
                    while (productName.length() < 30) {
                        productName += " ";
                    }
                    if (productName.length() > 30){
                        productName = productName.substring(0, 30);
                    }
                    String price = args[2];
                    while (price.length() < 8) {
                        price += " ";
                    }
                    if(price.length() > 8){
                        price = price.substring(0, 8);
                    }
                    String quantity = args[3];
                    while (quantity.length() < 4) {
                        quantity += " ";
                    }
                    if(quantity.length() > 4){
                        quantity = quantity.substring(0, 4);
                    }
                    String newProduct = String.format("\n%s%s%s%s", nextID, productName, price, quantity);
                    outputStream.write(newProduct.getBytes());
            }
        }
        outputStream.close();
    }
}
