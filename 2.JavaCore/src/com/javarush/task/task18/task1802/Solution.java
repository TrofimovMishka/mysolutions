package com.javarush.task.task18.task1802;

import java.io.*;
import java.util.Arrays;

/* 
Минимальный байт
C:\Users\Mike\OneDrive\Рабочий стол\snapshot_blob.bin
C:\Users\Mike\OneDrive\Рабочий стол\some.txt
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String nameFile = reader.readLine();
        int min = 255;
        try (FileInputStream inputStream = new FileInputStream(nameFile)) {
            while (inputStream.available() > 0) {
                int res = inputStream.read();
                if (res < min) {
                    min = res;
                }
            }
        }
        System.out.println(min);
    }
}
