package com.javarush.task.task18.task1822;

import java.io.*;
import java.util.ArrayList;

/* 
Поиск данных внутри файла
195
C:\Users\Mike\OneDrive\Рабочий стол\first.txt
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = reader.readLine();
        try (BufferedReader fileReader = new BufferedReader(new FileReader(fileName))) {
            ArrayList<String> arrayList = new ArrayList<>();
            if (args.length > 0) {
                while (fileReader.ready()) {
                    arrayList.add(fileReader.readLine());
                }
                arrayList.stream().filter(ele -> {
                    String [] arr = ele.split(" ");
                    if(arr[0].equals(args[0])){
                        return true;
                    }
                    return false;
                }).forEach(System.out::println);
            }
        }
    }
}

// from JR:
/*
if (str.startsWith(args[0] + " ")) { Первый раз я делал так-же, только без пробела сравнивал args[0] + " ", и не прошло
   System.out.println(str);
    break;
    }
 */
