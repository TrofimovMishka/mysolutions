package com.javarush.task.task18.task1803;

import java.beans.IntrospectionException;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* 
Самые частые байтs
C:\Users\Mike\OneDrive\Рабочий стол\some.txt

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String nameFile = reader.readLine();

        ArrayList<Integer> arrayList;
        try (FileInputStream inputStream = new FileInputStream(nameFile)) {
            arrayList = new ArrayList<>();
            while (inputStream.available() > 0) {
                arrayList.add(inputStream.read());
            }
        }
        Set<Integer> hashSet = new HashSet<>();
        ArrayList<Integer> counts = new ArrayList<>();
        for (int i = 0; i < arrayList.size(); i++) {
            int b = arrayList.get(i);
            int count = 0;
            for (int j = 0; j < arrayList.size(); j++) {
                if(b == arrayList.get(j)){
                    count++;
                }
            }
            counts.add(count);
        }
        Collections.sort(counts);

        for (int i = 0; i < arrayList.size(); i++) {
            int b = arrayList.get(i);
            int count = 0;
            for (int j = 0; j < arrayList.size(); j++) {
                if(b == arrayList.get(j)){
                    count++;
                }
            }
            if (count == counts.get(counts.size()-1)){
                hashSet.add(b);
            }
        }
        for (Integer i :
                hashSet) {
            System.out.print(i+" ");
        }
    }
}
