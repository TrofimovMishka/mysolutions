package com.javarush.task.task18.task1828;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/* 
Прайсы 2
-u 198479 "Какая-то фигня по цене ширпотреба которая никому не нужна" 221.50 117
C:\Users\Mike\OneDrive\Рабочий стол\first.txt
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String nameFile = reader.readLine();
        List<String> arrayList = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(nameFile))) {
            while (bufferedReader.ready()) {
                arrayList.add(bufferedReader.readLine());
            }
        }
        if (args.length > 0) {
            //clear files
            FileOutputStream outputStream2 = new FileOutputStream(nameFile);
            outputStream2.write("".getBytes());
            outputStream2.close();
            switch (args[0]) {
                case "-d":
                    //open stream for rewrite
                    try (BufferedWriter outputStream = new BufferedWriter(new FileWriter(nameFile, true))) {
                        arrayList.stream().filter(str ->
                                !str.substring(0, 8).trim().equals(args[1])).forEach(str -> {  //what not delete
                            try { // write
                                outputStream.write(str);
                                outputStream.write("\n");
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                    }
                case "-u":
                    //open stream for rewrite
                    try (BufferedWriter outputStream3 = new BufferedWriter(new FileWriter(nameFile, true))) {
                        int placeForAdd = 0;
                        for (int i = 0; i < arrayList.size(); i++) {
                            if (arrayList.get(i).substring(0, 8).trim().equals(args[1])) {
                                placeForAdd = i;
                                arrayList.remove(i);
                            }
                        }
                        //create new string for add
                        String result = String.format("%s%s%s%s"
                                , lengthStr(args[1], 8)
                                , lengthStr(args[2], 30)
                                , lengthStr(args[3], 8)
                                , lengthStr(args[4], 4));

                        arrayList.add(placeForAdd, result);
                        arrayList.stream().forEach(str -> {
                            try {
                                outputStream3.write(str);
                                outputStream3.write("\n");
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                    }
            }
        }
    }

    static String lengthStr(String str, int len) {
        String result = str;
        while (result.length() < len) {
            result += " ";
        }
        if (result.length() > len) {
            result = result.substring(0, len);
        }
        return result;
    }
}
