package com.javarush.task.task18.task1820;

import java.io.*;
import java.util.Arrays;

/* 
Округление чисел

C:\Users\Mike\OneDrive\Рабочий стол\first.txt
C:\Users\Mike\OneDrive\Рабочий стол\second.txt

*/

public class Solution {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            String file1 = reader.readLine();
            String file2 = reader.readLine();

            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file1));
                 FileOutputStream outputStream = new FileOutputStream(file2)) {
                StringBuilder infoFromFile = new StringBuilder();
                while (bufferedReader.ready()){
                    infoFromFile.append(bufferedReader.readLine());
                }
                String [] arr = infoFromFile.toString().split(" ");
                Arrays.stream(arr).forEach(ele -> {
                    try {
                        double numb = Math.round(Double.parseDouble(ele));
                        outputStream.write((String.format("%.0f", numb).getBytes()));
                        outputStream.write(" ".getBytes());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
