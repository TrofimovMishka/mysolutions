package com.javarush.task.task18.task1816;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/* 
Английские буквы
"C:\Users\Mike\OneDrive\Рабочий стол\first.txt"

*/

public class Solution {
    public static void main(String[] args) {
        String fileName = args[0];
        int count = 0;
        try(FileInputStream inputStream = new FileInputStream(fileName)
        ) {
            while(inputStream.available()>0){
                int ch = inputStream.read();
                if((ch >=65 && ch <=90)||
                   (ch >=97 && ch <=122)){
                    count++;
                }
            }
            System.out.println(count);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
