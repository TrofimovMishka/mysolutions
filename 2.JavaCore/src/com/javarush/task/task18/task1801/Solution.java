package com.javarush.task.task18.task1801;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

/* 
Максимальный байт
C:\Users\Mike\OneDrive\Рабочий стол\second.txt
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String linkFile = reader.readLine();

        try (FileInputStream inputStream = new FileInputStream(linkFile)) {
//            System.out.printf("File size: %d bytes \n", inputStream.available());
            int max = 0;
            int info = -1;
            while((info = inputStream.read())!=-1){
                if(info > max){
                    max =info;
                }
            }
            System.out.println(max);
        }
    }
}
