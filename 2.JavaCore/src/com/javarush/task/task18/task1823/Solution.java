package com.javarush.task.task18.task1823;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/* 
Нити и байты
C:\Users\Mike\OneDrive\Рабочий стол\first.txt
C:\Users\Mike\OneDrive\Рабочий стол\second.txt
C:\Users\Mike\OneDrive\Рабочий стол\tree.txt
*/

public class Solution {
    public static volatile Map<String, Integer> resultMap = new HashMap<String, Integer>();

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename ;
        while(!((filename = reader.readLine()).equals("exit"))){
            new ReadThread(filename);
        }

        System.out.println(resultMap);
    }

    public static class ReadThread extends Thread {
        private String fileName;
        public ReadThread(String fileName) {
            //implement constructor body
            this.fileName = fileName;
            this.start();
        }
        // implement file reading here - реализуйте чтение из файла тут

        @Override
        public void run() {
            try(FileInputStream inputStream = new FileInputStream(fileName)
            ) {
                byte [] buffer = new byte[inputStream.available()];
                while (inputStream.available()>0){
                    inputStream.read(buffer);
                }
                int count = 0;
                int indexMaxByte = 0;
                for (int i = 0; i < buffer.length; i++) {
                    int preCount = 0;
                    for (byte b : buffer) {
                        if (buffer[i] == b) {
                            preCount++;
                        }
                    }
                    if(preCount>count){
                        count = preCount;
                        indexMaxByte = i;
                    }
                }
                synchronized (Solution.class){
                    resultMap.put(fileName, (int)buffer[indexMaxByte]);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
