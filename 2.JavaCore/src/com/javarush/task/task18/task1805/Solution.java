package com.javarush.task.task18.task1805;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;

/* 
Сортировка байт
C:\Users\Mike\OneDrive\Рабочий стол\some.txt
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String nameFile = reader.readLine();
        try (FileInputStream inputStream = new FileInputStream(nameFile)) {
            List<Integer> arrayList = new ArrayList<>();
            while (inputStream.available() > 0) {
                int fromFile = inputStream.read();
                if(!(arrayList.contains(fromFile))){
                    arrayList.add(fromFile);
                }
            }
            Collections.sort(arrayList);
            for (Integer i :
                    arrayList) {
                System.out.print(i+" ");
            }
        }
    }
}
