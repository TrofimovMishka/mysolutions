package com.javarush.task.task18.task1807;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Подсчет запятых
C:\Users\Mike\OneDrive\Рабочий стол\some.txt
*/

public class Solution {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String nameFile = null;
        try {
            nameFile = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try(FileInputStream inputStream = new FileInputStream(nameFile)) {
//            byte[] buffer = new byte[inputStream.available()];
            int count = 0;
            while(inputStream.available()>0){
                int i = -1;
                if((i=((char)inputStream.read())) == (',')){
                    count++;
                }
            }
            System.out.println(count);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
