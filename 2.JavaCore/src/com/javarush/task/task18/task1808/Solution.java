package com.javarush.task.task18.task1808;

import java.io.*;

/* 
Разделение файла
C:\Users\Mike\OneDrive\Рабочий стол\first.txt
C:\Users\Mike\OneDrive\Рабочий стол\second.txt
C:\Users\Mike\OneDrive\Рабочий стол\tree.txt
*/

public class Solution {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            String fileName1 = reader.readLine();
            String fileName2 = reader.readLine();
            String fileName3 = reader.readLine();

            try (FileInputStream inputStream = new FileInputStream(fileName1);
            FileOutputStream outputStream1 = new FileOutputStream(fileName2);
            FileOutputStream outputStream2 = new FileOutputStream(fileName3)) {
                byte [] infoFromFile1 = new byte[inputStream.available()];
               int countByte = inputStream.read(infoFromFile1);  // 11

                if (infoFromFile1.length % 2 == 0){
                    outputStream1.write(infoFromFile1, 0, countByte/2);
                    outputStream2.write(infoFromFile1, infoFromFile1.length/2, countByte/2);
                }else{
                    outputStream1.write(infoFromFile1, 0, countByte/2+1);// 5+1
                    outputStream2.write(infoFromFile1, countByte/2+1, countByte/2); //5+1, 5
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
