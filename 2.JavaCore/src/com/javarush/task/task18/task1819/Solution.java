package com.javarush.task.task18.task1819;

import java.io.*;
import java.nio.Buffer;
import java.util.Arrays;

/* 
Объединение файлов
C:\Users\Mike\OneDrive\Рабочий стол\first.txt
C:\Users\Mike\OneDrive\Рабочий стол\second.txt
*/

public class Solution {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            String file1 = reader.readLine();
            String file2 = reader.readLine();

            try (FileInputStream inputStream1 = new FileInputStream(file1);
                 FileInputStream inputStream2 = new FileInputStream(file2)) {
                byte[] bytes1 = new byte[inputStream1.available()];
                byte[] bytes2 = new byte[inputStream2.available()];
                inputStream1.read(bytes1);
                inputStream2.read(bytes2);

                byte [] newArray = Arrays.copyOf(bytes2, bytes1.length+bytes2.length);
                System.arraycopy(bytes1, 0, newArray, bytes2.length, bytes1.length);

                //write info to file
                FileOutputStream outputStream1 = new FileOutputStream(file1);
                outputStream1.write(newArray);
                outputStream1.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
