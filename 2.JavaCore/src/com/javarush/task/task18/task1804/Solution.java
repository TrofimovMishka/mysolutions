package com.javarush.task.task18.task1804;

import java.beans.IntrospectionException;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/* 
Самые редкие байт
C:\Users\Mike\OneDrive\Рабочий стол\some.txt
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String nameFile = reader.readLine();

        try (FileInputStream inputStream = new FileInputStream(nameFile)) {
            ArrayList<Integer> arrayList = new ArrayList<>();
            while (inputStream.available() > 0) {
                arrayList.add(inputStream.read());
            }

            ArrayList<Integer> counts = new ArrayList<>();

            for (int i = 0; i < arrayList.size(); i++) {
                int b = arrayList.get(i);
                int count = 0;
                for (int j = 0; j < arrayList.size(); j++) {
                    if(b == arrayList.get(j)){
                        count++;
                    }
                }
                counts.add(count);
            }
            Collections.sort(counts);
            for (int i = 0; i < arrayList.size(); i++) {
                int b = arrayList.get(i);
                int count = 0;
                for (int j = 0; j < arrayList.size(); j++) {
                    if(b == arrayList.get(j)){
                        count++;
                    }
                }
                if (count == counts.get(0)){
                    System.out.print(b+" ");
                }
            }
        }
    }
}
