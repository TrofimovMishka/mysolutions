package com.javarush.task.task17.task1711;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* 
CRUD 2
-c Миронов м 15/04/1990 Миронова ж 25/04/1997
-u 1 Миронов м 15/04/1990 0 Миронова ж 25/04/1997
-d 0 1
-i 0 1

*/

public class Solution {
    public static volatile List<Person> allPeople = new ArrayList<Person>();

    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) {
        //start here - начни тут
        switch (args[0]) {
            case "-c":
                synchronized (allPeople) {
                    for (int i = 3; i < args.length; i = i + 3) {
                        createNewPeople(args[i - 2], args[i - 1], args[i]);
                    }
                    break;
                }
            case "-u":
                synchronized (allPeople) {
                    for (int i = 4; i < args.length; i = i + 4) {
                        updatePeople(args[i - 3], args[i - 2], args[i - 1], args[i]);
                    }
                    break;
                }
            case "-d":
                synchronized (allPeople) {
                    for (int i = 1; i < args.length; i++) {
                        deletePeople(args[i]);
                    }
                    break;
                }
            case "-i":
                synchronized (allPeople) {
                    for (int i = 1; i < args.length; i++) {
                        infoPeople(args[i]);
                    }
                    break;
                }
        }

//        for (Person person: allPeople ){
//            System.out.println(person);
//        }
    }

    public static synchronized void createNewPeople(String name, String sex, String date) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        try {
            Date dateBirthday = format.parse(date);
            if (sex.equals("м")) {
                allPeople.add(Person.createMale(name, dateBirthday));
            } else {
                allPeople.add(Person.createFemale(name, dateBirthday));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(allPeople.size() - 1);
    }

    public static synchronized void updatePeople(String id, String name, String sex, String date) {
        Person person = allPeople.get(Integer.parseInt(id));
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date dateBirthday = format.parse(date);
            person.setBirthDate(dateBirthday);
            person.setName(name);
            if (sex.equals("м")) {
                person.setSex(Sex.MALE);
            } else {
                person.setSex(Sex.FEMALE);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public static synchronized void deletePeople(String id) {
        Person person = allPeople.get(Integer.parseInt(id));
        person.setSex(null);
        person.setBirthDate(null);
        person.setName(null);
    }

    public static synchronized void infoPeople(String id) {
        Person person = allPeople.get(Integer.parseInt(id));
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
        if (person.getSex().equals(Sex.MALE)) {
            System.out.println(person.getName() + " м " + dateFormat.format(person.getBirthDate()));
        } else {
            System.out.println((person.getName() + " ж " + dateFormat.format(person.getBirthDate())));
        }
    }


}
