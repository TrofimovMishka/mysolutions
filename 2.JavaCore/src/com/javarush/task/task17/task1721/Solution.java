package com.javarush.task.task17.task1721;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/* 
Транзакционност
C:\Users\Mike\OneDrive\Рабочий стол\first.txt
C:\Users\Mike\OneDrive\Рабочий стол\second.txt

*/

public class Solution {
    public static List<String> allLines = new ArrayList<String>();
    public static List<String> forRemoveLines = new ArrayList<String>();

    public static void main(String[] args) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String nameFirstFile = null;
        String nameSecondFile = null;
        try {
            nameFirstFile = reader.readLine();
            nameSecondFile = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (BufferedReader reader1 = new BufferedReader
                (new FileReader(nameFirstFile));
             BufferedReader reader2 = new BufferedReader
                     (new FileReader(nameSecondFile))) {

            // read and store first file
            String infoFirstFile;
            while ((infoFirstFile = reader1.readLine()) != null) {
                allLines.add(infoFirstFile);
            }
            // read and store second file
            String infoSecondFile;
            while ((infoSecondFile = reader2.readLine()) != null) {
                forRemoveLines.add(infoSecondFile);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

//        System.out.println(allLines);
//        System.out.println("-------------------------------------------------------------------------------");
//        System.out.println(forRemoveLines);
//        System.out.println();

        Solution solution = new Solution();
        try {
            solution.joinData();
        } catch (CorruptedDataException e) {
            e.printStackTrace();
        }

//        System.out.println(allLines);
//        System.out.println("-------------------------------------------------------------------------------");
//        System.out.println(forRemoveLines);
    }

    public void joinData() throws CorruptedDataException {
        if (allLines.containsAll(forRemoveLines)) {
            allLines.removeAll(forRemoveLines);
        } else {
            allLines.clear();
            throw new CorruptedDataException();
        }
    }
}
