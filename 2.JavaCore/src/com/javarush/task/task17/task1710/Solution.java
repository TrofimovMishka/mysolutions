package com.javarush.task.task17.task1710;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

/* 
CRUD
*/

public class Solution {
    public static List<Person> allPeople = new ArrayList<Person>();

    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createFemale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) {
        //напишите тут ваш код
//        for (int i = 0; i < args.length; i++) {
//            System.out.println(args[i]);
//        }
            if (args[0].equals("-c")){
                _cMethod(args[1], args[2], args[3]);
            }
            if (args[0].equals("-r")){
                _rMethod(Integer.parseInt(args[1]));
            }
            if (args[0].equals("-u")){
                _uMethod(Integer.parseInt(args[1]), args[2], args[3], args[4]);
            }
            if (args[0].equals("-d")){
                _dMethod(Integer.parseInt(args[1]));
            }
//        for(Person one: allPeople){
//            System.out.println(one.toString());
//        }

    }
    public static void _cMethod(String name, String sex, String data){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        Date newDate = null;
        try {
            newDate = simpleDateFormat.parse(data);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(sex.equals("м")){
            allPeople.add(Person.createMale(name, newDate));
        }else{
            allPeople.add(Person.createFemale(name, newDate));
        }
        System.out.println(allPeople.size()-1);

    }

    public static void _rMethod(int id){
        Person somePerson = allPeople.get(id);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
        if(somePerson.getSex().equals(Sex.FEMALE)){
            System.out.println(somePerson.getName()+" ж "+ dateFormat.format(somePerson.getBirthDate()));
        }else{
            System.out.println(somePerson.getName()+" м "+ dateFormat.format(somePerson.getBirthDate()));
        }
    }

    public static void _uMethod(int id, String name, String sex, String data){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        Date newDate = null;
        try {
            newDate = simpleDateFormat.parse(data);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Person somePerson = allPeople.get(id);
        somePerson.setName(name);
        somePerson.setBirthDate(newDate);

        if(sex.equals("м")){
            somePerson.setSex(Sex.MALE);
        }else{
            somePerson.setSex(Sex.FEMALE);
        }
    }

    public static void _dMethod(int id){
        Person somePerson = allPeople.get(id);
        somePerson.setName(null);
        somePerson.setBirthDate(null);
        somePerson.setSex(null);
    }
}
