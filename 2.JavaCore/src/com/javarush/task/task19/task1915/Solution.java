package com.javarush.task.task19.task1915;

import java.io.*;

/* 
Дублируем текст
C:\Users\Mike\OneDrive\Рабочий стол\first.txt
*/

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) {
        PrintStream consoleStream = System.out;

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(outputStream);

        System.setOut(printStream);

        testString.printSomething();
        String result = outputStream.toString();

        System.setOut(consoleStream);

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String file = reader.readLine();
            try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
                fileOutputStream.write(result.getBytes());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(result);
    }

    public static class TestString {
        public void printSomething() {
            System.out.println("it's a text for testing");
        }
    }
}

