package com.javarush.task.task19.task1904;

import java.io.IOException;
import java.util.*;

/* 
И еще один адаптер
*/

public class Solution {

    public static void main(String[] args) {

    }

    public static class PersonScannerAdapter implements PersonScanner {
        private final Scanner fileScanner;

        public PersonScannerAdapter(Scanner fileScanner) {
            this.fileScanner = fileScanner;
        }

        @Override
        public Person read() throws IOException {
            String[] onePerson = fileScanner.nextLine().split(" ");
            String firstName = onePerson[1];
            String middleName = onePerson[2];
            String lastName = onePerson[0];
            Calendar localDate = new GregorianCalendar (Integer.parseInt(onePerson[5])
                    , Integer.parseInt(onePerson[4])-1, Integer.parseInt(onePerson[3]));
            Date birthDate = localDate.getTime();
            return new Person(firstName, middleName, lastName, birthDate);
        }

        @Override
        public void close() throws IOException {
            fileScanner.close();
        }
    }
}
