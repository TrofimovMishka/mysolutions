package com.javarush.task.task19.task1923;

import java.io.*;
import java.nio.file.StandardOpenOption;

/* 
Слова с цифрами
"C:\Users\Mike\OneDrive\Рабочий стол\first.txt" "C:\Users\Mike\OneDrive\Рабочий стол\second.txt"

*/

public class Solution {
    public static void main(String[] args) {
        String file1 = "";
        String file2 = "";
        if (args.length > 1){
            file1 = args[0];
            file2 = args[1];
        }

        try (BufferedReader reader = new BufferedReader(new FileReader(file1));
             BufferedWriter writer = new BufferedWriter(new FileWriter(file2))) {
            while (reader.ready()){
                String fromFile = reader.readLine();
                String [] arr = fromFile.split(" ");
                for (String s :
                        arr) {
                    if (s.matches(".*\\d+.*")){
                        writer.write(s+" ");
                    }
                }
            }
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
