package com.javarush.task.task19.task1922;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/* 
Ищем нужные строки
C:\Users\Mike\OneDrive\Рабочий стол\first.txt
*/

public class Solution {
    public static List<String> words = new ArrayList<String>();

    static {
        words.add("файл");
        words.add("вид");
        words.add("В");
    }

    public static void main(String[] args) {
        String fileName = "";
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            fileName = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<String> arrayList = new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            bufferedReader.lines().forEach(arrayList::add);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < arrayList.size(); i++) {
            int count = 0;
            String check = arrayList.get(i);
            String [] arr = check.split(" ");

            for (String s :
                    words) {
                for (String s2 :
                        arr) {
                    if (s.equals(s2)) {
                        count++;
                    }
                }
            }
            if (count==2){
                System.out.println(check);
            }
        }
    }
}
