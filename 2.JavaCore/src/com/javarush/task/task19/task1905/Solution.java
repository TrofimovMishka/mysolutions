package com.javarush.task.task19.task1905;

import java.util.HashMap;
import java.util.Map;

/* 
Закрепляем адаптер
*/

public class Solution {
    public static Map<String, String> countries = new HashMap<String, String>();
    static {
        countries.put("UA", "Ukraine");
        countries.put("RU", "Russia");
        countries.put("CA", "Canada");
    }

    public static void main(String[] args) {

    }

    public static class DataAdapter implements RowItem {
        private Customer customer;
        private Contact contact;

        public DataAdapter(Customer customer, Contact contact) {
            this.customer = customer;
            this.contact = contact;
        }
        /*
        Метод getCountryCode() должен вернуть страну из countries, по значению getCountryName() объекта customer.
         */

        @Override
        public String getCountryCode() {
            for(Map.Entry<String, String> entry : countries.entrySet()){
                if(entry.getValue().equals(customer.getCountryName())){
                    return entry.getKey();
                }
            }
            return "";
        }

        @Override
        public String getCompany() {
            return customer.getCompanyName();
        }

        @Override
        public String getContactFirstName() {
            String[] person = contact.getName().split(", ");
            return person[1];
        }

        @Override
        public String getContactLastName() {
            String[] person = contact.getName().split(", ");
            return person[0];
        }

        @Override
        public String getDialString() {
            return reformatPhone(contact.getPhoneNumber());
        }
    }

    public static interface RowItem {
        String getCountryCode();        //For example: UA

        String getCompany();            //For example: JavaRush Ltd.

        String getContactFirstName();   //For example: Ivan

        String getContactLastName();    //For example: Ivanov

        String getDialString();         //For example: callto://+380501234567
    }

    public static interface Customer {
        String getCompanyName();        //For example: JavaRush Ltd.

        String getCountryName();        //For example: Ukraine
    }

    public static interface Contact {
        String getName();               //For example: Ivanov, Ivan

        String getPhoneNumber();        //For example: +38(050)123-45-67 or +3(805)0123-4567 or +380(50)123-4567 or ...
    }

    static String reformatPhone(String call){
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i < call.length(); i++) {
            try{
                sb.append(Integer.parseInt(call.substring(i-1, i)));
                if(i==call.length()-1){
                    sb.append(Integer.parseInt(call.substring(i)));
                }
            }catch (NumberFormatException e){
            }
        }
        return String.format("callto://+%s", sb);
    }
}