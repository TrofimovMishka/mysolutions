package com.javarush.task.task19.task1919;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/* 
Считаем зарплаты
C:\Users\Mike\OneDrive\Рабочий стол\first.txt
C:\Users\Mike\OneDrive\Рабочий стол\source.txt
*/

public class Solution {
    public static void main(String[] args) {
        String fileName = null;
        if (args.length > 0) {
            fileName = args[0];
        }
        ArrayList<String> arrayList = new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            bufferedReader.lines().forEach(arrayList::add);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Set<String> strings = new TreeSet<>();
        arrayList.stream().forEach(ele -> {
            String [] s = ele.trim().split(" ");
            strings.add(s[0]);
        });

        strings.forEach(ele -> {
            double salary = 0;
            for (String s :
                    arrayList) {
                String [] arr = s.trim().split(" ");
                if(arr.length>1){
                    if(ele.equals(arr[0])){
                        try{
                            salary+=Double.parseDouble(arr[1]);
                        }catch (NumberFormatException e){
                            System.out.println("UPS...");
                        }
                    }
                }
            }
            System.out.printf("%s %s", ele, salary);
            System.out.println();
        });
    }
}