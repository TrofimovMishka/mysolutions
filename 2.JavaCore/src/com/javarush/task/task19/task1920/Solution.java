package com.javarush.task.task19.task1920;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/* 
Самый богаты
C:\Users\Mike\OneDrive\Рабочий стол\source.txt
*/

public class Solution {
    public static void main(String[] args) {
        String fileName = null;
        if (args.length > 0) {
            fileName = args[0];
        }
        ArrayList<String> arrayList = new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            bufferedReader.lines().forEach(arrayList::add);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Map<String, Double> employee = new TreeMap<>();

        for (int i = 0; i < arrayList.size(); i++) {
            String [] arr = arrayList.get(i).split(" ");
            if(employee.containsKey(arr[0])){
                employee.put(arr[0], Double.parseDouble(arr[1]) + employee.get(arr[0]));
            }else{
                employee.put(arr[0], Double.parseDouble(arr[1]));
            }
        }
//        System.out.println(Collections.max(employee.entrySet()
//        , Comparator.comparingDouble(Map.Entry::getValue)).getKey());
        double maxValueInMap = (Collections.max(employee.values()));
        for (Map.Entry<String, Double> entry : employee.entrySet()){
            if(entry.getValue() == maxValueInMap){
                System.out.println(entry.getKey());
            }
        }
    }
}