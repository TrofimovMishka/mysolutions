package com.javarush.task.task19.task1921;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/* 
Хуан Хуанович
"C:\Users\Mike\OneDrive\Рабочий стол\first.txt"
*/

public class Solution {
    public static final List<Person> PEOPLE = new ArrayList<Person>();

    public static void main(String[] args) {
        String fileName = "";
        if (args.length > 0) {
            fileName = args[0];
        }
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            reader.lines().forEach(str -> {
                StringBuilder name = new StringBuilder();
                int day = 0;
                int month = 0;
                int year = 0;

                String[] arr = str.split(" ");
                for (String s :
                        arr) {
                    if (s.matches("\\D+|\\D*\\W\\D*")) {
                        name.append(s+" ");
                    }
                }
                try {
                    day = Integer.parseInt(arr[arr.length - 3]);
                    month = Integer.parseInt(arr[arr.length - 2]);
                    year = Integer.parseInt(arr[arr.length - 1]);
                } catch (NumberFormatException ignor) {
                }

                Calendar calendar = new GregorianCalendar(year, month - 1, day);
                PEOPLE.add(new Person(name.toString().trim(), calendar.getTime()));
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
