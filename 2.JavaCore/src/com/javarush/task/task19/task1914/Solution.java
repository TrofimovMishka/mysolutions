package com.javarush.task.task19.task1914;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/* 
Решаем пример
C:\Users\Mike\OneDrive\Рабочий стол\first.txt
C:\Users\Mike\OneDrive\Рабочий стол\second.txt
C:\Users\Mike\OneDrive\Рабочий стол\tree.txt
*/

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) {
        PrintStream consoleStream = System.out;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(outputStream);
        System.setOut(printStream);
        testString.printSomething();
        String result = outputStream.toString();
        System.setOut(consoleStream);
        String[] arr = result.split(" ");

        if (arr[1].equals("+")) {
            int sum = Integer.parseInt(arr[0]) + Integer.parseInt(arr[2]);
            System.out.printf("%s + %s = %d", arr[0], arr[2], sum);
        }
        if (arr[1].equals("-")) {
            int sum = Integer.parseInt(arr[0]) - Integer.parseInt(arr[2]);
            System.out.printf("%s - %s = %d", arr[0], arr[2], sum);
        }
        if (arr[1].equals("*")) {
            int sum = Integer.parseInt(arr[0]) * Integer.parseInt(arr[2]);
            System.out.printf("%s * %s = %d", arr[0], arr[2], sum);
        }

    }

    public static class TestString {
        public void printSomething() {
            System.out.println("3 + 6 = ");
        }
    }
}

