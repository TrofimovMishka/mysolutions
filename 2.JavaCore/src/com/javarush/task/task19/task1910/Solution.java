package com.javarush.task.task19.task1910;

import java.io.*;
import java.util.ArrayList;

/* 
Пунктуация
C:\Users\Mike\OneDrive\Рабочий стол\first.txt
C:\Users\Mike\OneDrive\Рабочий стол\second.txt
*/

public class Solution {
    public static void main(String[] args) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String file1 = reader.readLine();
            String file2 = reader.readLine();

            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file1));
                 BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file2))) {
                while (bufferedReader.ready()){
                    String [] arr = bufferedReader.readLine().split("\\p{P}");
                    for (String s :
                            arr) {
                        bufferedWriter.write(s);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
