package com.javarush.task.task19.task1907;

import java.io.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Scanner;

/* 
Считаем слов
C:\Users\Mike\OneDrive\Рабочий стол\first.txt
*/

public class Solution {
    public static void main(String[] args) {

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String fileName = reader.readLine();
            ArrayList<String> arrayList = new ArrayList<>();
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
                bufferedReader.lines().forEach(arrayList::add);
            }
            int count = 0;

//            arrayList.forEach(System.out::println);

            for (String value : arrayList) {
                String[] arr = value.split("\\W");
                for (String s :
                        arr) {
                    if (s.trim().equalsIgnoreCase("world")) {
                        count++;
                    }
                }
            }

            System.out.println(count);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
