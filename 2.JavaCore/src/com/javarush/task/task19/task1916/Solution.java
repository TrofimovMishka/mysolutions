package com.javarush.task.task19.task1916;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;

/* 
Отслеживаем изменения
C:\Users\Mike\OneDrive\Рабочий стол\first.txt
C:\Users\Mike\OneDrive\Рабочий стол\second.txt

*/

public class Solution {
    public static List<LineItem> lines = new ArrayList<LineItem>();

    public static void main(String[] args) {
        List<String> listForFile1 = new ArrayList<>();
        List<String> listForFile2 = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String file1 = reader.readLine();
            String file2 = reader.readLine();

            try (BufferedReader bufferedReader1 = new BufferedReader(new FileReader(file1));
                 BufferedReader bufferedReader2 = new BufferedReader(new FileReader(file2))) {
                while (bufferedReader1.ready()) {
                    listForFile1.add(bufferedReader1.readLine());
                }
                while (bufferedReader2.ready()) {
                    listForFile2.add(bufferedReader2.readLine());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        ArrayDeque arrayDeque1 = new ArrayDeque(listForFile1);
        ArrayDeque arrayDeque2 = new ArrayDeque(listForFile2);

        while (arrayDeque1.peek() != null && arrayDeque2.peek() != null) {
            String a = (String) arrayDeque1.pollFirst();
            String b = (String) arrayDeque2.pollFirst();
            if (a.equals(b)) {
                lines.add(new LineItem(Type.SAME, a));
//                System.out.println("SAME " + a);
            } else {
                String c = (String) arrayDeque1.peekFirst();
                String e = (String) arrayDeque2.peekFirst();
                if (b.equals(c)) {
                    lines.add(new LineItem(Type.REMOVED, a));
                    arrayDeque2.addFirst(b);
//                    System.out.println("REMOVED " + a);
                } else {
                    lines.add(new LineItem(Type.ADDED, b));
                    arrayDeque1.addFirst(a);
//                    System.out.println("ADDED " + b);
                }
            }
        }
        while (arrayDeque1.peek() != null) {
            String a = (String) arrayDeque1.pollFirst();
            lines.add(new LineItem(Type.REMOVED, a));
//            System.out.println("REMOVED " + a);
        }
        while (arrayDeque2.peek() != null) {
            String a = (String) arrayDeque2.pollFirst();
            lines.add(new LineItem(Type.ADDED, a));
//            System.out.println("ADDED " + a);
        }
    }

    public static enum Type {
        ADDED,        //добавлена новая строка
        REMOVED,      //удалена строка
        SAME          //без изменений
    }

    public static class LineItem {
        public Type type;
        public String line;

        public LineItem(Type type, String line) {
            this.type = type;
            this.line = line;
        }
    }
}
