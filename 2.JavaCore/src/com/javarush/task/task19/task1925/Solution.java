package com.javarush.task.task19.task1925;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/* 
Длинные слова
"C:\Users\Mike\OneDrive\Рабочий стол\first.txt" "C:\Users\Mike\OneDrive\Рабочий стол\second.txt"
*/

public class Solution {
    public static void main(String[] args) {
        String file1 = "";
        String file2 = "";
        if (args.length > 1) {
            file1 = args[0];
            file2 = args[1];
        }
        List<String> arrayList = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(file1));
             BufferedWriter writer = new BufferedWriter(new FileWriter(file2))) {
            reader.lines().forEach(ele -> {
                String[] arr = ele.split("\\p{Z}");
                for (String s :
                        arr) {
                    if (s.length() > 6) {
                        arrayList.add(s);
                    }
                }
            });
            writer.write(String.join(",", arrayList));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
