package com.javarush.task.task19.task1908;

import java.io.*;
import java.util.ArrayList;

/* 
Выделяем числа
C:\Users\Mike\OneDrive\Рабочий стол\first.txt
C:\Users\Mike\OneDrive\Рабочий стол\second.txt
*/

public class Solution {
    public static void main(String[] args) {
        String file1;
        String file2;
        ArrayList<String> arrayList = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            file1 = reader.readLine();
            file2 = reader.readLine();
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file1));
                 BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file2))) {
                while (bufferedReader.ready()) {
                    arrayList.add(bufferedReader.readLine());
                }
                for (String s :
                        arrayList) {
                    String[] arr = s.split("\\p{Z}");
                    //   String[] splitLine = line.trim().split(" "); - from javaRush
                    for (int i = 0; i < arr.length; i++) {
                        try {
                            int numb = Integer.parseInt(arr[i]);
                            bufferedWriter.write(arr[i]+" ");
                        } catch (NumberFormatException e) {
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
