package com.javarush.task.task19.task1926;

import java.io.*;

/* 
Перевертыши
C:\Users\Mike\OneDrive\Рабочий стол\first.txt
*/

public class Solution {
    public static void main(String[] args) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String fileName = reader.readLine();
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
                bufferedReader.lines().forEach(ele -> {
                    System.out.println(new StringBuilder(ele).reverse());
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
