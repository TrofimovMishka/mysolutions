package com.javarush.task.task15.task1529;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Осваивание статического блока
*/

public class Solution {
    public static void main(String[] args) {

    }

    static {
        //add your code here - добавьте код тут
        reset();
    }

    public static CanFly result;

    public static void reset() {
        //add your code here - добавьте код тут
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
            String fromConsole = bufferedReader.readLine();
            if (fromConsole.equals("helicopter")){
                result = new Helicopter();
            }else if(fromConsole.equals("plane")){
                int countPass = Integer.parseInt(bufferedReader.readLine());
                result = new Plane(countPass);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
