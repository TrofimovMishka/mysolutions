package com.javarush.task.task15.task1531;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.text.DecimalFormat;

/* 
Факториал
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int input = Integer.parseInt(reader.readLine());
        reader.close();

        System.out.println(factorial(input));
    }

    public static String factorial(int n) {
        //add your code here
        if (n < 0) {    // wtf
            return "0";
        }
        if (n == 0) {
            return "1";
        }

        BigDecimal result = new BigDecimal(1);  // from JR
//        result = (double) n * Double.parseDouble(factorial(n - 1)); // use rekursia. not work with getNumberInstance
        for (int i = 1; i <= n; i++) {
            result = result.multiply(new BigDecimal(i));        // from JR
        }

//        return String.valueOf(result);  // n =150   -> 5.7133839564458505E262             // Валидатор не принял
//        return DecimalFormat.getNumberInstance().format(result);
// return all digits in number // Валидатор не принял - очень не точное число

        return result.toString();   // from JR - очень точное число
    }
}
