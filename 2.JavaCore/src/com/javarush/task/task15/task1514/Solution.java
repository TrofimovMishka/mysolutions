package com.javarush.task.task15.task1514;

import java.util.HashMap;
import java.util.Map;

/* 
Статики-1
*/

public class Solution {
    public static Map<Double, String> labels = new HashMap<Double, String>();
    static {
        labels.put(101.5 , "DHello");
        labels.put(102.5 , "Hello");
        labels.put(103.5 , "FHello");
        labels.put(104.5 , "VHello");
        labels.put(100.5 , "THello");
    }

    public static void main(String[] args) {
        System.out.println(labels);
    }
}
