package com.javarush.task.task15.task1519;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Разные методы для разных типов
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        //напиште тут ваш код
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String info;
            while (!(info = reader.readLine()).equals("exit")) {
                try {
                    if (info.contains(".")) {
                        print(Double.parseDouble(info));
                    } else if (info.matches("[-]?\\d+")) {
                        if (Integer.parseInt(info) > 0 && Integer.parseInt(info) < 128) {
                            print(Short.parseShort(info));
                        } else if ((Integer.parseInt(info) <= 0 || Integer.parseInt(info) >= 128)) {
                            print(Integer.parseInt(info));
                        }
                    } else {
                        print(info);
                    }
                } catch (Exception e) {
                    print(info);
                }

            }
        }
    }

    public static void print(Double value) {
        System.out.println("Это тип Double, значение " + value);
    }

    public static void print(String value) {
        System.out.println("Это тип String, значение " + value);
    }

    public static void print(short value) {
        System.out.println("Это тип short, значение " + value);
    }

    public static void print(Integer value) {
        System.out.println("Это тип Integer, значение " + value);
    }
}
