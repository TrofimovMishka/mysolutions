package com.javarush.task.task15.task1527;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Парсер реквестов
http://javarush.ru/alpha/index.html?obj=tr3.14&name=Amigo&lvl=15&view&name=Amigo
http://javarush.ru/alpha/index.html?lvl=15&view&name=Amigo
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String url = reader.readLine();
        //напишите тут ваш код
        String subRes = url.substring(url.lastIndexOf("?") + 1);
        String[] array1 = subRes.split("&");
        double resulDouble = 0;
        String resultString = null;
        StringBuilder builder = new StringBuilder("");

        for (int i = 0; i < array1.length; i++) {
            if (array1[i].contains("obj")) {

                String [] tempArray = array1[i].split("=");
                try {
                    resulDouble = Double.parseDouble(tempArray[1]);
                } catch (Exception e) {
                    resultString = tempArray[1];
                }
            }

            String[] array2 = array1[i].split("=");
            builder.append(array2[0] + " ");

        }
        System.out.println(builder);

        if(resultString != null){
            alert(resultString);
        }
        if(resulDouble > 0){
            alert(resulDouble);
        }

    }

    public static void alert(double value) {
        System.out.println("double: " + value);
    }

    public static void alert(String value) {
        System.out.println("String: " + value);
    }
}
