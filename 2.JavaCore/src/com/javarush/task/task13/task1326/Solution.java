package com.javarush.task.task13.task1326;

import java.io.*;
import java.util.*;

/* 
Сортировка четных чисел из файла  C:\Users\Mike\OneDrive\Рабочий стол\some.txt
DataInputStream readerFromFile = new DataInputStream(new FileInputStream(nameFile))
*/

public class Solution {
    public static void main(String[] args) {
        // напишите тут ваш код
        ArrayList<Integer> arrayList = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String nameFile = reader.readLine();
            try (BufferedReader readerFromFile = new BufferedReader(new InputStreamReader(new DataInputStream(new FileInputStream(nameFile))))) {  //WTF?
                String info;
                while ((info = readerFromFile.readLine()) != null) {
                    arrayList.add(Integer.parseInt(info));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Collections.sort(arrayList);

        for (int i = 0; i < arrayList.size(); i++) {
            if (arrayList.get(i) % 2 == 0) {
                System.out.println(arrayList.get(i));
            }
        }
    }
}
