package com.javarush.task.task13.task1319;

import java.io.*;

/* 
Писатель в файл с консоли C:\Users\Mike\OneDrive\Рабочий стол\some.txt
*/

public class Solution {
    public static void main(String[] args) {
        // напишите тут ваш код
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String fileName = reader.readLine();
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
                String someText = " "; // read from commandLine
                do {
                    someText = reader.readLine();
                    writer.write(someText + "\n"); //and write to buffer
                    writer.flush();                     // clean buffer
                } while (!(someText.equals("exit")));

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
