package com.javarush.task.task13.task1318;

import java.io.*;
import java.util.Scanner;

/* 
Чтение файла
*/

public class Solution {
    public static void main(String[] args) {
        // напишите тут ваш код
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ) {
            String locate = reader.readLine();
            try (BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(locate));
            ) {
                while (inputStream.available() > 0) {
                    int s = inputStream.read();
                    System.out.print((char) s);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}