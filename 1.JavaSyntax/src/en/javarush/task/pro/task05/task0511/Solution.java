package en.javarush.task.pro.task05.task0511;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/* 
Creating a two-dimensional array
*/

public class Solution {
    public static int[][] multiArray;

    public static void main(String[] args) throws IOException {
        //write your code here
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int sizeFirst = Integer.parseInt(reader.readLine());

        multiArray = new int[sizeFirst][];

        for (int i = 0; i < multiArray.length; i++) {
            multiArray[i] = new int[Integer.parseInt(reader.readLine())];
        }

        for (int i = 0; i < multiArray.length; i++) {
            for (int j = 0; j < multiArray[i].length; j++) {
                System.out.println(multiArray[i][j]);
            }
        }

    }
}
