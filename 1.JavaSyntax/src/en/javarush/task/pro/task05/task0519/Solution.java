package en.javarush.task.pro.task05.task0519;

import java.util.Arrays;

/* 
Is anyone there?
*/

public class Solution {

    public static int[] array = {9, 8, 7, 6, 5, 4, 3, 2, 1};
    public static int element = 1;

    public static void main(String[] args) {
        //write your code here
        int [] array2 = Arrays.copyOfRange(array, 0, array.length);
        Arrays.sort(array2);
        int index = Arrays.binarySearch(array2, element);
        System.out.println(index >= 0);
    }
}
