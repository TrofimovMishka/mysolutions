package en.javarush.task.pro.task06.task0605;

/* 
Correct order
*/

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Solution {

    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
        printArray(array);
        reverseArray(array);
        printArray(array);
    }

    public static void reverseArray(int[] array) {
        //write your code here
        int [] array2 = Arrays.copyOf(array, array.length);
        int j = array.length-1;
        for (int i = 0; i < array.length; i++) {
            array[i] = array2[j];
            j--;
        }
    }

    public static void printArray(int[] array) {
        for (int i : array) {
            System.out.print(i + ", ");
        }
        System.out.println();
    }
}
