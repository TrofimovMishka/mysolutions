package en.javarush.task.pro.task06.task0608;

/* 
A cube calculator
*/

public class Solution {
    public static void main(String[] args) {

    }
    public static long cube(long number){
        return (long)Math.pow((double)number, 3);
    }

    //write your code here
}
