package en.javarush.task.pro.task06.task0606;

/* 
A method for all seasons
*/

public class Solution {

    public static void main(String[] args) {

    }

    public static void universalMethod() {

    }
    public static void universalMethod(int i) {

    }
    public static void universalMethod(double i) {

    }
    public static void universalMethod(String s) {

    }
    public static void universalMethod(int[] array) {

    }
    public static void universalMethod(int i, int j) {

    }
    public static void universalMethod(double i, String s) {

    }
    public static void universalMethod(int[] array, int i) {

    }
    public static void universalMethod(String [] array, String s) {

    }
    public static void universalMethod(double i, int k) {

    }



    //write your code here
}
