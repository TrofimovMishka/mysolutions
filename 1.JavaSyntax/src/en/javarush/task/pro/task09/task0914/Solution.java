package en.javarush.task.pro.task09.task0914;

/* 
Path update
*/

public class Solution {
    public static void main(String[] args) {
        String path = "/usr/java/jdk1/bin";

        String jdk13 = "jdk35-1355";
        System.out.println(changePath(path, jdk13));
    }

    public static String changePath(String path, String jdk) {
        //write your code here

        return path.replaceAll("jdk\\d*\\D*\\d+", jdk);
    }
}
