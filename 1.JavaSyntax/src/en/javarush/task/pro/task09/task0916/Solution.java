package en.javarush.task.pro.task09.task0916;

/* 
String.format()
*/
// My name is <name>. I will earn $<salary> a month.
public class Solution {
    public static void main(String[] args) {
        System.out.println(format("Amigo", 5000));
    }

    public static String format(String name, int salary) {
        String phrase = "My name is . I will earn $ a month.";
        //write your code here
        return String.format("My name is %s. I will earn $%d a month.", name, salary);
    }
}
