package en.javarush.task.pro.task09.task0915;

import java.util.Arrays;
import java.util.StringTokenizer;

/* 
StringTokenizer
*/

public class Solution {
    public static void main(String[] args) {
        String packagePath = "java.util.stream";
        String[] tokens = getTokens(packagePath, "\\.");
        System.out.println(Arrays.toString(tokens));
    }

    public static String[] getTokens(String query, String delimiter) {
        //write your code here

        // Solution javaRush
        StringTokenizer stringTokenizer = new StringTokenizer(query, delimiter);
        String[] result = new String[6];           // what a fuck is this????????????
        int i = 0;
        while (stringTokenizer.hasMoreTokens()) {
            result[i++] = stringTokenizer.nextToken();
        }
        return result;

        //My Solution
        //Find size for uor array:
//        StringTokenizer tokenizer = new StringTokenizer(query, delimiter);
//        int lengthArray = 0;
//        while (tokenizer.hasMoreTokens()) {
//            tokenizer.nextToken();
//            lengthArray++;
//        }
//      //Fill array:
//        StringTokenizer tokenizer2 = new StringTokenizer(query, delimiter);
//        String[] arrayToken = new String[lengthArray];
//        int i = 0;
//        while (tokenizer2.hasMoreTokens()) {
//            arrayToken[i] = tokenizer2.nextToken();
//            i++;
//        }
//
//        return arrayToken;
    }
}
