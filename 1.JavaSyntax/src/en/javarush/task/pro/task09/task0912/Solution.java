package en.javarush.task.pro.task09.task0912;

/* 
URL validation
*/
// <network protocol>://<resource name>.<domain> (com, net, org, or us)

public class Solution {
    public static void main(String[] args) {
        String[] urls = {"https://javarush.us", "https://google.com", "http://wikipedia.org", "facebook.com", "https://instagram", "codegym.cc"};
        for (String url : urls) {
            String protocol = checkProtocol(url);
            String domainExtension = checkDomainExtension(url);

            System.out.println("URL address: " + url + ", network protocol - " + protocol + ", domain extension - " + domainExtension);
        }
    }

    public static String checkProtocol(String url) {
        //write your code here
        int indexTo = url.indexOf("://");
        if (indexTo > 1) {
            if (((url.substring(0, indexTo)).equals("http")) || ((url.substring(0, indexTo)).equals("https"))) {
                return url.substring(0, indexTo);
            }
        }
        return "unknown";
    }

    public static String checkDomainExtension(String url) {
        //write your code here
        int indexTo = url.indexOf(".");
        if (indexTo > 1) {
            if (url.substring(indexTo + 1).equals("com") ||
                    url.substring(indexTo + 1).equals("net") ||
                    url.substring(indexTo + 1).equals("org") ||
                    url.substring(indexTo + 1).equals("us")) {
                return url.substring(indexTo + 1);
            }
        }
        return "unknown";
    }
}
