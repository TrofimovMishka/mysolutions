package en.javarush.task.jdk13.task08.task0813;

import java.util.HashSet;
import java.util.Set;

/* 
20 words that start with the letter "L"
*/

public class Solution {
    public static Set<String> createSet() {
        //write your code here
        Set<String> set = new HashSet<>();
        set.add("Lq");
        set.add("Lw");
        set.add("Le");
        set.add("Lr");
        set.add("Lt");
        set.add("Ly");
        set.add("Lu");
        set.add("Li");
        set.add("Lo");
        set.add("Lp");
        set.add("La");
        set.add("Ls");
        set.add("Ld");
        set.add("Lf");
        set.add("Lg");
        set.add("Lh");
        set.add("Lj");
        set.add("Lk");
        set.add("Ll");
        set.add("Lz");

        return set;
    }

    public static void main(String[] args) {

    }
}
