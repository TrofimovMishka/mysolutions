package en.javarush.task.jdk13.task08.task0811;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/* 
Method quartet
*/

public class Solution {
    public static List getListForGet() {//ArrayList
        //write your code here
        List<Object> list = new ArrayList<>();
        return list;

    }

    public static List getListForSet() {//ArrayList
        //write your code here
        List<Object> list = new ArrayList<>();
        return list;

    }

    public static List getListForAddOrInsert() { //linkedList
        //write your code here
        List<Object> list = new LinkedList<>();
        return list;

    }

    public static List getListForRemove() {//linkedList
        //write your code here
        List<Object> list = new LinkedList<>();
        return list;

    }

    public static void main(String[] args) {

    }
}
