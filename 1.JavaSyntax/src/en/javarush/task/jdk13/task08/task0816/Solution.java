package en.javarush.task.jdk13.task08.task0816;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/* 
Remove all people born in the summer
Map<String, String> copy = new HashMap<>(map);

        for (String name : copy.values()) {
            int count = 0;
            for (String nameTmp : map.values()) {
                if (nameTmp.equals(name)) {
                    count++;
                }
            }
            if (count > 1) {
                removeItemFromMapByValue(map, name);
            }
*/

public class Solution {
    public static Map<String, Date> createMap()  throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("MMMMM d yyyy", Locale.ENGLISH);
        Map<String, Date> map = new HashMap<>();
        map.put("Smith", dateFormat.parse("MAY 1 2014"));
        map.put("Kira", dateFormat.parse("APRIL 2 2012"));
        map.put("Monika", dateFormat.parse("MARCH 7 2012"));
        map.put("Elza", dateFormat.parse("JUNE 1 2012"));
        map.put("Jessi", dateFormat.parse("JULY 1 2016"));
        map.put("John", dateFormat.parse("JULY 3 2012"));
        map.put("Robert", dateFormat.parse("AUGUST 6 2012"));
        map.put("Liza", dateFormat.parse("AUGUST 9 2013"));
        map.put("Ola", dateFormat.parse("SEPTEMBER 2 2014"));
        map.put("Masha", dateFormat.parse("NOVEMBER 8 2015"));

        //write your code here
        return map;
    }

    public static void removeAllSummerPeople(Map<String, Date> map) throws ParseException {
        //write your code here

        Map<String, Date> copy = new HashMap<>(map);
        SimpleDateFormat format = new SimpleDateFormat("MMMMM", Locale.ENGLISH);

// summer month - JUNE, JULY, AUGUST
        for (String name : copy.keySet()) {
            if(new SimpleDateFormat("MMMMM", Locale.ENGLISH).format(copy.get(name)).equalsIgnoreCase("JUNE")  ||
              new SimpleDateFormat("MMMMM", Locale.ENGLISH).format(copy.get(name)).equalsIgnoreCase("JULY")   ||
              new SimpleDateFormat("MMMMM", Locale.ENGLISH).format(copy.get(name)).equalsIgnoreCase("AUGUST") ){
                map.remove(name);
            }
        }
    }

    public static void main(String[] args) throws ParseException {
//        Map<String, Date> map = createMap();
//        removeAllSummerPeople(map);
//        System.out.println(map);
    }
}

/*
Solution from JR
public static void removeAllSummerPeople(Map<String, Date> map) {
        Map<String, Date> copy = new HashMap<>(map);
        for (String key : copy.keySet()) {
            Date date = copy.get(key);
            int month = date.getMonth() + 1;
            if (month == 6 || month == 7 || month == 8) {
                map.remove(key);
            }
        }
    }
 */
