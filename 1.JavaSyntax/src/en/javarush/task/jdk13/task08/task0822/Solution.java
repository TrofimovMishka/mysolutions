package en.javarush.task.jdk13.task08.task0822;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* 
Minimum of N numbers
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        List<Integer> integerList = getIntegerList();
        System.out.println(getMinimum(integerList));
    }

    public static int getMinimum(List<Integer> list) {
        // Find the minimum here
        Collections.sort(list);
        if(list.size()<1){
            return -1;
        }else{
            return list.get(0);
        }
    }

    public static List<Integer> getIntegerList() throws IOException {
        // Create and initialize a list here
        List<Integer> integers = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int n =0;
        for (int i = 0; i < 1; i++) {
            n= Integer.parseInt(reader.readLine());
        }
        for (int i = 0; i < n; i++) {
            integers.add(Integer.parseInt(reader.readLine()));
        }

        return integers;
    }
}
