package en.javarush.task.jdk13.task08.task0808;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/* 
10 thousand deletions and insertions
*/

public class Solution {
    public static void main(String[] args) {
        // ArrayList
        ArrayList arrayList = new ArrayList();
        insert10000(arrayList);
        get10000(arrayList);
        set10000(arrayList);
        remove10000(arrayList);

        // LinkedList
        LinkedList linkedList = new LinkedList();
        insert10000(linkedList);
        get10000(linkedList);
        set10000(linkedList);
        remove10000(linkedList);

        System.out.println(arrayList.toString());
        System.out.println(linkedList.toString());
    }

    public static void insert10000(List list) {
        //write your code here
        for (int i = 0; i < 10_000; i++) {
            list.add(i, i+1);
        }

    }

    public static void get10000(List list) {
        //write your code here
        for (int i = 0; i < 10_000; i++) {
            list.get(i);
        }

    }

    public static void set10000(List list) {
        //write your code here
        for (int i = 0; i < 10_000; i++) {
            list.set(i, i+3);
        }

    }

    public static void remove10000(List list) {
        //write your code here
//
        list.removeAll(list);

    }
}
