package en.javarush.task.jdk13.task08.task0819;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/* 
Set of cats
*/

public class Solution {
    public static void main(String[] args) {
        Set<Cat> cats = createCats();

        //write your code here. step 3

        Iterator<Cat> catIterator = cats.iterator();
        Cat one = null;

        while (catIterator.hasNext()){
            for (int i = 0; i < cats.size(); i++) {
                if (i==0){
                    one = catIterator.next();
                }
            }
        }

        cats.remove(one);

        printCats(cats);
    }

    public static Set<Cat> createCats() {
        //write your code here. step 2
        Set<Cat> cats = new HashSet<>();
        Cat cat1 = new Cat();
        cats.add(cat1);
        Cat cat2 = new Cat();
        cats.add(cat2);
        Cat cat3 = new Cat();
        cats.add(cat3);

        return cats;
    }

    public static void printCats(Set<Cat> cats) {
        // step 4
        Iterator<Cat> catIterator = cats.iterator();
        while (catIterator.hasNext()){
            System.out.println(catIterator.next());
        }
    }

    // step 1
    public static class Cat{

    }
}
