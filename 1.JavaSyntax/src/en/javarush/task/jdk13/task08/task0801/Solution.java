package en.javarush.task.jdk13.task08.task0801;

import java.util.HashSet;
import java.util.Set;

/* 
Set of plants
watermelon

banana

cherry

pear

cantaloupe

blackberry

ginseng

strawberry

iris

potato
*/

public class Solution {
    public static void main(String[] args) {
        //write your code here
        Set<String> strings = new HashSet<>();
        strings.add("watermelon");
        strings.add("banana");
        strings.add("cherry");
        strings.add("pear");
        strings.add("cantaloupe");
        strings.add("blackberry");
        strings.add("ginseng");
        strings.add("strawberry");
        strings.add("iris");
        strings.add("potato");
        for (String s :
                strings) {
            System.out.println(s);
        }

    }
}
