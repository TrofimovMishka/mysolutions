package en.javarush.task.jdk13.task08.task0817;

import java.util.*;

/* 
We don't need repeats
в любом случае не удаляйте во время итерации hashMap. сохраните значения в списке и удалите их во внешнем цикле
*/

public class Solution {
    public static Map<String, String> createMap() {
        //write your code here
        Map<String, String> map = new HashMap<>();
        map.put("Borsukova", "Inna");
        map.put("Troukova", "Anna");
        map.put("Gorrsukova", "Janna");
        map.put("lopsukova", "Ola");
        map.put("sirsukova", "Kira");
        map.put("Drirsukova", "Minika");
        map.put("Astrsukova", "Inna");
        map.put("Wenirsukova", "Jessi");
        map.put("Korsukova", "Inna");
        map.put("Sorsukova", "Jull");

        return map;

    }

    public static void removeFirstNameDuplicates(Map<String, String> map) {
        //write your code here
        Map<String, String> copy = new HashMap<>(map);
        Map<String, String> map2 = new HashMap<>();
        Set<String> keys = map.keySet();
        Iterator<String> keyIter = keys.iterator();
        while (keyIter.hasNext()) {
            String key = keyIter.next();
            String value = map.get(key);
            map2.put(value, key);
        }
        for (Map.Entry<String, String> entry : map2.entrySet()) {
            copy.remove(entry.getValue());
        }
        for (Map.Entry<String, String> entry : copy.entrySet()) {
            removeItemFromMapByValue(map, entry.getValue());
        }
    }

    public static void removeItemFromMapByValue(Map<String, String> map, String value) {
        Map<String, String> copy = new HashMap<>(map);
        for (Map.Entry<String, String> pair : copy.entrySet()) {
            if (pair.getValue().equals(value)) {
                map.remove(pair.getKey());
            }
        }
    }

    public static void main(String[] args) {

    }
}
