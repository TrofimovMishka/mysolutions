package en.javarush.task.jdk13.task08.task0818;

import java.util.HashMap;
import java.util.Map;

/* 
Only for the rich
*/

public class Solution {
    public static Map<String, Integer> createMap() {
        //write your code here
        Map<String, Integer> map = new HashMap<>();
        map.put("Volkova", 1200);
        map.put("Igolkova", 400);
        map.put("Zubolkova", 300);
        map.put("Kolkova", 500);
        map.put("Sholkova", 600);
        map.put("Polkova", 400);
        map.put("Zolkova", 100);
        map.put("Dolkova", 200);
        map.put("Qolkova", 1700);
        map.put("Lolkova", 3000);

        return map;
    }

    public static void removeItemFromMap(Map<String, Integer> map) {
        //write your code here
        Map<String, Integer> copy = new HashMap<>(map);

        for(String name: copy.keySet()){
            if(copy.get(name)<500){
                map.remove(name);
            }
        }
    }

    public static void main(String[] args) {
//        Map<String, Integer> copys = createMap();
//        removeItemFromMap(copys);
//        System.out.println(copys);
    }
}