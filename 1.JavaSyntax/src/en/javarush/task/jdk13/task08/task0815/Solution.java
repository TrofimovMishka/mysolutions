package en.javarush.task.jdk13.task08.task0815;

import java.util.HashMap;
import java.util.Map;

/* 
Census
*/

public class Solution {
    public static Map<String, String> createMap() {
        //write your code here
        Map<String, String> map = new HashMap<>();
        map.put("Borsukova", "Inna");
        map.put("Troukova", "Anna");
        map.put("Gorrsukova", "Janna");
        map.put("lopsukova", "Ola");
        map.put("sirsukova", "Kira");
        map.put("Drirsukova", "Minika");
        map.put("Astrsukova", "Inna");
        map.put("Wenirsukova", "Jessi");
        map.put("Korsukova", "Inna");
        map.put("Sorsukova", "Jull");

        return map;
    }

    public static int getSameFirstNameCount(Map<String, String> map, String name) {
        //write your code here
        int result =0;
        for(Map.Entry<String, String> entry : map.entrySet()){
            if (entry.getValue()==name){
                result++;
            }
        }
        return result;
    }

    public static int getSameLastNameCount(Map<String, String> map, String lastName) {
        //write your code here
        int result =0;
        for(Map.Entry<String, String> entry : map.entrySet()){
            if (entry.getKey()==lastName){
                result++;
            }
        }
        return result;

    }

    public static void main(String[] args) {

    }
}
