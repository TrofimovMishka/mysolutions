package en.javarush.task.jdk13.task08.task0807;

import java.util.ArrayList;
import java.util.LinkedList;

/* 
LinkedList and ArrayList
*/

public class Solution {
    public static Object createArrayList() {
        //write your code here
        ArrayList<Object> arrayList = new ArrayList<>();
        return arrayList;
    }

    public static Object createLinkedList() {
        //write your code here
        LinkedList<Object> linkedList = new LinkedList<>();
        return linkedList;

    }

    public static void main(String[] args) {

    }
}
