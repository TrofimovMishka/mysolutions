package en.javarush.task.jdk13.task08.task0831;

import java.util.Arrays;

/* 
Favorite board games
*/

public class Solution {

    public static BoardGame[] collection = new BoardGame[5];

    public static void main(String[] args) {
        BoardGame chess = new BoardGame();
        BoardGame checkers = new BoardGame();
        BoardGame backgammon = new BoardGame();
        BoardGame dominoes = new BoardGame();
        BoardGame cards = new BoardGame();

        chess.name = "Battleship";
        checkers.name = "Frootti";
        backgammon.name = "Krekersy";
        dominoes.name = "Snikersy";
        cards.name = "Nagibalovo";


        collection[0] = chess;
        collection[1] = checkers;
        collection[2] = backgammon;
        collection[3] = dominoes;
        collection[4] = cards;

        //write your code here
        System.out.println(Arrays.toString(collection));
    }
}