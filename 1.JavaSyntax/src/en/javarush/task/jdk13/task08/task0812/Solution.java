package en.javarush.task.jdk13.task08.task0812;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* 
Longest sequence
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        //write your code here
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        List<Integer> integers = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            integers.add(Integer.parseInt(reader.readLine()));
        }
        int result = 0;
        for (int i = 0; i < 10; i++) {
            int numbers = 0;
            for (int j = 0; j < 10; j++) {
                if (integers.get(i).equals(integers.get(j))) {
                    numbers++;
                } else {
                    if (numbers > result) {
                        result = numbers;
                    }
                    numbers = 0;
                }
                if (numbers > result) {
                    result = numbers;
                }
            }
        }
        System.out.println(result);
    }


    // Solution from javarush
    /*
     int count = 1;
        int max = 1;
        for (int i = 0; i < 9; i++) {
            if (list.get(i).equals(list.get(i + 1))) {
                max++;
            } else if (max > count) {
                count = max;
                max = 1;
            } else {
                max = 1;
            }
        }

        if (max > count) {
            count = max;
        }

        System.out.println(count);
     */
}
