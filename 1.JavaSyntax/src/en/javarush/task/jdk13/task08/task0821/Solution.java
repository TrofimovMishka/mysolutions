package en.javarush.task.jdk13.task08.task0821;

import java.util.HashMap;
import java.util.Map;

/* 
Shared last names and first names
*/

public class Solution {
    public static void main(String[] args) {
        Map<String, String> map = createPeopleMap();
        printPeopleMap(map);
    }

    public static Map<String, String> createPeopleMap() {
        //write your code here
        Map<String, String> map = new HashMap<>();
        map.put("Volkova", "Inna");
        map.put("Bobolkova", "Alena");
        map.put("Zolkova", "Masha");
        map.put("Volkova", "Liza");
        map.put("Litolkova", "Inna");
        map.put("Kolkova", "Liza");
        map.put("Nolkova", "Elza");
        map.put("Volkova", "Lola");
        map.put("Polkova", "Lila");
        map.put("Jagolkova", "Inna");



        return map;
    }

    public static void printPeopleMap(Map<String, String> map) {
        for (Map.Entry<String, String> s : map.entrySet()) {
            System.out.println(s.getKey() + " " + s.getValue());
        }
    }
}
