package en.javarush.task.jdk13.task08.task0829;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

/* 
Software update
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        List<String> list = new ArrayList<>();
        while (true) {
            String family = reader.readLine();
            if (family.isEmpty()) {
                break;
            }

            list.add(family);
        }

        // Read the house number
//        int houseNumber = Integer.parseInt(reader.readLine());
        String city = reader.readLine();
        int indexForFamily = 0;

        for (int i = 0; i < list.size(); i++) {
            if (city.equalsIgnoreCase(list.get(i))) {
                indexForFamily = i+1;
            }
        }
        System.out.println(list.get(indexForFamily));

//        if (houseNumber >= 0 && houseNumber < list.size()) {
//            String familyName = list.get(houseNumber);
//            System.out.println(familyName);
//        }
    }
}
