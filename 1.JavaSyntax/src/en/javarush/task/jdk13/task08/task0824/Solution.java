package en.javarush.task.jdk13.task08.task0824;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* 
The whole family together
*/

public class Solution {
    public static void main(String[] args) {
        //write your code here
        Human children1 = new Human("heoh", true, 2);
        Human children2 = new Human("hwqweoh", true, 3);
        Human children3 = new Human("qqqqqweoh", true, 3);

        Human father = new Human("Zopa", true, 34, children1, children2, children3);
        Human mother = new Human("Dopa", false, 34, children1, children2, children3);

        Human grandfather1 = new Human("Ded1", true, 55, father );
        Human grandfather2 = new Human("Ded2", true, 55,  mother);

        Human grandmother1 = new Human("Baba1", false, 55, father);
        Human grandmother2 = new Human("Baba2", false, 55,  mother);

        System.out.println(grandfather1.toString());
        System.out.println(grandfather2.toString());
        System.out.println(grandmother1.toString());
        System.out.println(grandmother2.toString());
        System.out.println(father.toString());
        System.out.println(mother.toString());
        System.out.println(children1.toString());
        System.out.println(children2.toString());
        System.out.println(children3.toString());


    }

    public static class Human {
        //write your code here
        String name;
        boolean sex;
        int age;
        ArrayList<Human> children = new ArrayList<>();

        public Human(String name, boolean sex, int age, Human... children) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            Collections.addAll(this.children, children);
        }

        public Human(String name, boolean sex, int age) {
            this.name = name;
            this.sex = sex;
            this.age = age;
        }

        public String toString() {
            String text = "";
            text += "Name: " + this.name;
            text += ", sex: " + (this.sex ? "male" : "female");
            text += ", age: " + this.age;

            int childCount = this.children.size();
            if (childCount > 0) {
                text += ", children: " + this.children.get(0).name;

                for (int i = 1; i < childCount; i++) {
                    Human child = this.children.get(i);
                    text += ", " + child.name;
                }
            }
            return text;
        }
    }
}
