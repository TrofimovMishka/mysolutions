package en.javarush.task.jdk13.task08.task0814;

import java.util.*;
import java.util.stream.Collectors;

/* 
Greater than 10? You're not a good fit for us
*/

public class Solution {
    public static Set<Integer> createSet() {
        // write your code here
        Set<Integer> set = new HashSet<>();
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
        set.add(5);
        set.add(6);
        set.add(7);
        set.add(8);
        set.add(9);
        set.add(10);
        set.add(12);
        set.add(13);
        set.add(14);
        set.add(15);
        set.add(16);
        set.add(17);
        set.add(18);
        set.add(19);
        set.add(21);
        set.add(22);

        return set;
    }

    public static void removeAllNumbersGreaterThan10(Set<Integer> set) {
        // write your code here
        Set <Integer> set2 = set.stream().filter(ele -> ele>10).collect(Collectors.toSet());
        set.removeAll(set2);
    }

    public static void main(String[] args) {

    }
}
