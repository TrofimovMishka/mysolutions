package en.javarush.task.jdk13.task08.task0820;

import java.util.HashSet;
import java.util.Set;

/* 
Animal set
*/

public class Solution {
    public static void main(String[] args) {
        Set<Cat> cats = createCats();
        Set<Dog> dogs = createDogs();

        Set<Object> pets = join(cats, dogs);
        printPets(pets);
        removeCats(pets, cats);
        printPets(pets);
    }

    public static Set<Cat> createCats() {
        Set<Cat> result = new HashSet<Cat>();

        //write your code here
        Cat cat1 = new Cat();
        Cat cat2 = new Cat();
        Cat cat3 = new Cat();
        Cat cat4 = new Cat();
        result.add(cat1);
        result.add(cat2);
        result.add(cat3);
        result.add(cat4);


        return result;
    }

    public static Set<Dog> createDogs() {
        //write your code here
        Set<Dog> dogs = new HashSet<>();
        Dog dog = new Dog();
        Dog dog1 = new Dog();
        Dog dog2 = new Dog();
        dogs.add(dog);
        dogs.add(dog1);
        dogs.add(dog2);

        return dogs;
    }

    public static Set<Object> join(Set<Cat> cats, Set<Dog> dogs) {
        //write your code here
        Set<Object> catsAndDods = new HashSet<>();
        catsAndDods.addAll(cats);
        catsAndDods.addAll(dogs);
        return catsAndDods;
    }

    public static void removeCats(Set<Object> pets, Set<Cat> cats) {
        //write your code here
        HashSet<Object> copy = new HashSet<>(pets);
        for(Object o: copy){
            for(Object c: cats){
                if(o.equals(c)){
                    pets.remove(c);
                }
            }
        }
    }

    public static void printPets(Set<Object> pets) {
        //write your code here
        for(Object o: pets){
            System.out.println(o.toString());
        }
    }

    //write your code here
    public static class Cat{

    }
    public static class Dog{

    }
}
