package en.javarush.task.jdk13.task04.task0401;

/* 
This age doesn't work for me…
*/

public class Solution {
    public static void main(String[] args) {
        Person person = new Person();
        System.out.println("Age: " + person.age);
        person.adjustAge(person.age);                                 //5
//        System.out.println("Adjusted age: " + person.age);
    }

    public static class Person {
        public int age = 20;                                          //2

        public void adjustAge(int age) {                              //4
            this.age = age + 20;                                           //6
            System.out.println("The age in adjustAge() is: " + age);  // 1, 3
        }
    }
}
