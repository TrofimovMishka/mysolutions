package en.javarush.task.jdk13.task04.task0432;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
You can't have too much of a good thing
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //write your code here
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = reader.readLine();
        int a = Integer.parseInt(reader.readLine());

        while (a > 0) {
            System.out.println(s);
            a--;
        }

    }
}
