package en.javarush.task.jdk13.task04.task0431;

/* 
From 10 to 1
*/

public class Solution {
    public static void main(String[] args) {
        //write your code here
        int i = 10;
        while (i >= 1) {
            System.out.println(i);
            i--;
        }

    }
}
