package en.javarush.task.jdk13.task04.task0418;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Minimum of two numbers
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //write your code here
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int firstNumber = Integer.parseInt(reader.readLine());
        int secondNumber = Integer.parseInt(reader.readLine());

        if(firstNumber != secondNumber){
            if(firstNumber>secondNumber){
                System.out.println(secondNumber);
            }
            if(firstNumber<secondNumber){
                System.out.println(firstNumber);
            }
        }
        else{
            System.out.println(firstNumber);
        }

    }
}