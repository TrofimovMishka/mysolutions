package en.javarush.task.jdk13.task04.task0412;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Positive and negative numbers
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //write your code here
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int a = Integer.parseInt(reader.readLine());

        if (a > 0) {
            a = a + a;
            System.out.println(a);
        } else if (a < 0) {
            a = a + 1;
            System.out.println(a);
        }
        else if (a==0){
            System.out.println(0);
        }

    }

}