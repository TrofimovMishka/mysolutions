package en.javarush.task.jdk13.task04.task0429;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Positive and negative numbers
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //write your code here
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int a = Integer.parseInt(reader.readLine());
        int b = Integer.parseInt(reader.readLine());
        int c = Integer.parseInt(reader.readLine());
        int positiv = 0;
        int negativ = 0;

        if (a > 0) {
            positiv += 1;
        } else if (a < 0) {
            negativ += 1;
        }
        if (b > 0) {
            positiv += 1;
        } else if (b < 0) {
            negativ += 1;
        }
        if (c > 0) {
            positiv += 1;
        } else if (c < 0) {
            negativ += 1;
        }

        System.out.println("Number of negative numbers: " + negativ);
        System.out.println("Number of positive numbers: " + positiv);

    }
}
