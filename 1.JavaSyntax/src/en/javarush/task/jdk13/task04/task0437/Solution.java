package en.javarush.task.jdk13.task04.task0437;

/* 
Triangle of eights
*/

public class Solution {
    public static void main(String[] args) {
        //write your code here
        int a = 1;
        for (int i =0; i < 10; i++) {
            for (int j = 0; j < a; j++) {
                System.out.print(8);
            }
            ++a;
            System.out.println();
        }
    }
}
