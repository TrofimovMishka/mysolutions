package en.javarush.task.jdk13.task12.task1205;

/* 
But that's how I want it

if the argument is a Byte object, then divide it by 2 and convert it to a string, adding the letter "b" at the end;
if the argument is an Integer object, then divide it by 3 and convert it to a string;
if the argument is a Double object, then multiply it by 20 and convert it to a string;
if the argument is not one of the above types, then return the string "I didn't expect this type of number!"
*/

public class Solution {

    private static String UNEXPECTED_TYPE = "I didn't expect this type of number!";

    public static void main(String[] args) {
        System.out.println(toCustomString((byte) 12));
        System.out.println(toCustomString(12));
        System.out.println(toCustomString(12.));
        System.out.println(toCustomString(12L));
    }

    public static String toCustomString(Number number) {
        //write your code here
        if (number instanceof Byte){
            byte i = (byte)number;
            return i/2+"b";
        }
        if (number instanceof Integer){
            int i = (int)number;
            return Integer.toString(i/3);
        }
        if (number instanceof Double){
            double i = (double)number;
            return Double.toString(i*20);
        }

        return UNEXPECTED_TYPE;
    }
}
