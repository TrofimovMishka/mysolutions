package en.javarush.task.jdk13.task12.task1203;

/* 
Conscious choice

byte	1	-128 .. 127
short	2	-32,768 .. 32,767
int  	4	-2*10 9 .. 2*10 9

*/

public class Solution {

    public static void main(String[] args) {
        System.out.println(isByte(12)); // true
        System.out.println(isShort(130999)); // false
        System.out.println(isInt(1999939990L)); // true
        System.out.println(isInt(19999999939990L)); // false

    }

    public static boolean isByte(long l) {
        if(l<=Byte.MAX_VALUE && l>=Byte.MIN_VALUE){
            return true;
        }
        return false;
    }

    public static boolean isShort(long l) {
        if(l<=Short.MAX_VALUE && l>=Short.MIN_VALUE){
            return true;
        }
        return false;
    }

    public static boolean isInt(long l) {
        if(l<=Integer.MAX_VALUE && l>=Integer.MIN_VALUE){
            return true;
        }
        return false;
    }
}
