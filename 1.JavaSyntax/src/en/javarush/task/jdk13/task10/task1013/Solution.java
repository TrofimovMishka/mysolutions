package en.javarush.task.jdk13.task10.task1013;

/* 
Human class constructors
*/

public class Solution {
    public static void main(String[] args) {
    }

    public static class Human {
        // Write your variables and constructors here

        private String name;
        private char sex;
        private int age;
        private double weight;
        private String colorHair;
        private double height;

        public Human(String name, char sex, int age, double weight, String colorHair, double height) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.weight = weight;
            this.colorHair = colorHair;
            this.height = height;
        }

        public Human(String name, char sex, int age, double weight, String colorHair) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.weight = weight;
            this.colorHair = colorHair;
        }

        public Human(String name, char sex, int age, double weight) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.weight = weight;
        }

        public Human(String name, char sex, int age) {
            this.name = name;
            this.sex = sex;
            this.age = age;
        }

        public Human(String name, char sex) {
            this.name = name;
            this.sex = sex;
        }

        public Human(String name) {
            this.name = name;
        }

        public Human(char sex, int age, String colorHair) {
            this.sex = sex;
            this.age = age;
            this.colorHair = colorHair;
        }

        public Human(String name, char sex, String colorHair) {
            this.name = name;
            this.sex = sex;
            this.colorHair = colorHair;
        }

        public Human(String name, double height) {
            this.name = name;
            this.height = height;
        }

        public Human(String name, String colorHair) {
            this.name = name;
            this.colorHair = colorHair;
        }
    }
}
