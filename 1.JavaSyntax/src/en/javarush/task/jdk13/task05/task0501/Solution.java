package en.javarush.task.jdk13.task05.task0501;

/* 
Cat carnage (1)
*/
//A cat (Cat class) must have a name (String name), age (int age), weight (int weight), and strength (int strength).
public class Solution {
    public static void main(String[] args) {
        Cat cat1 = new Cat("Kiti", 2, 23, 23);
        Cat murza = new Cat("Murza", 3, 45, 56);
        Cat goga = new Cat("Goga", 4, 47, 59);

        System.out.println(cat1.fight(murza));
        System.out.println(cat1.fight(goga));
        System.out.println(goga.fight(murza));

    }

    public static class Cat {
        //write your code here
        private String name ;
        private int age;
        private int weight;
        private int strength;

        public Cat(String name, int age, int weight, int strength) {
            this.name = name;
            this.age = age;
            this.weight = weight;
            this.strength = strength;
        }

        public String getName() {
            return name;
        }

        public int getAge() {
            return age;
        }

        public int getWeight() {
            return weight;
        }

        public int getStrength() {
            return strength;
        }

        //fight(Cat anotherCat)

       public boolean fight(Cat cat2){

            int resultForCat1 =0;
            int resultForCat2 =0;

            if (this.getAge() > cat2.getAge()){
                resultForCat1++;
            }
            else if (this.getAge() < cat2.getAge()){
                resultForCat2++;
            }

            if (this.getStrength() > cat2.getStrength()){
                resultForCat1++;
            }
            else if (this.getStrength() < cat2.getStrength()){
                resultForCat2++;
            }

            if (this.getWeight() > cat2.getWeight()){
                resultForCat1++;
            }
            else if (this.getWeight() < cat2.getWeight()){
                resultForCat2++;
            }

            if(resultForCat1 == resultForCat2){
                return false;
            }
            return resultForCat1 > resultForCat2;

        }
    }
}
