package en.javarush.task.jdk13.task05.task0513;

/* 
Let's put together a rectangle
*/

public class Rectangle {
    //write your code here   left, top, width, height
    int left = 0;
    int top = 0;
    int width = 0;
    int height = 0;

    public void initialize(int left) {
        this.left = left;
    }
    public void initialize(int left, int top) {
        this.left = left;
        this.top = top;
    }
    public void initialize(int left, int top, int width) {
        this.left = left;
        this.top = top;
        this.width = width;
    }
    public void initialize(int left, int top, int width, int height) {
        this.left = left;
        this.top = top;
        this.width = width;
        this.height = height;
    }

    public static void main(String[] args) {

    }
}
