package en.javarush.task.jdk13.task05.task0516;

/* 
You can't buy friends
A friend (Friend class) must have three constructors:

Name
Name, age
Name, age, sex

*/

public class Friend {
    //write your code here
    String name;
    int age;
    char sex;

    public Friend(String name, int age, char sex) {
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    public Friend(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Friend(String name) {
        this.name = name;
    }

    public static void main(String[] args) {

    }
}
