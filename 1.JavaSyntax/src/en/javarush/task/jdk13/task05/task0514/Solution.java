package en.javarush.task.jdk13.task05.task0514;

/* 
Initializing objects  should have a String name and int age.
*/

public class Solution {
    public static void main(String[] args) {
        //write your code here
        Person person = new Person();
        person.initialize("Monika", 24);
    }

    static class Person {
        //write your code here

        String name;
        int age;

        public void initialize (String name, int age){
            this.name =name;
            this.age = age;
        }
    }
}
