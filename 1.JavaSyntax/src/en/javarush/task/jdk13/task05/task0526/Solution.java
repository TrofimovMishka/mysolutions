package en.javarush.task.jdk13.task05.task0526;

/* 
Man and woman
*/

public class Solution {
    public static void main(String[] args) {
        //write your code here

        Man man = new Man("Vova2", 23, "Gdynia");
        Man man2 = new Man("Vova", 20, "Moscow");
        Woman woman = new Woman("Monika", 24, "NewYork");
        Woman woman2 = new Woman("Monika2", 27, "Nikopol");

        System.out.println(man);
        System.out.println(man2);
        System.out.println(woman);
        System.out.println(woman2);

    }

    //write your code here
    public static  class  Man{
        String name;
        int age;
        String address;

        @Override
        public String toString() {
            return name + " " + age + " " + address;
        }


        public  Man(String name, int age, String address) {
            this.name = name;
            this.age = age;
            this.address = address;
        }
    }

    public static class Woman{
        String name;
        int age;
        String address;

        @Override
        public String toString() {
            return name + " " + age + " " + address;
        }

        public Woman(String name, int age, String address) {
            this.name = name;
            this.age = age;
            this.address = address;
        }
    }
}
