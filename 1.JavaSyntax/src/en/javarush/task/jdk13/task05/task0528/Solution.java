package en.javarush.task.jdk13.task05.task0528;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/* 
Display today's date
*/

public class Solution {
    public static void main(String[] args) {
        //write your code here

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM dd yyyy");
        Date date = new Date();
        System.out.println(dateFormat.format(date));

    }
}
