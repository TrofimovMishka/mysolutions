package en.javarush.task.jdk13.task05.task0510;

/* 
Initializing cats
Name
Name, weight, age
Name, age (standard weight)
Weight, color (unknown name, address and age, i.e. a homeless cat)
Weight, color, address (someone else's pet)
*/

public class Cat {
    //write your code here
    String name = null;
    int weight = 100;
    int age = 5;
    String color = "Black";
    String address = null;

    public void initialize(String name) {
        this.name = name;
        this.age = 5;
        this.weight = 100;
        this.color = "Black";
    }

    public void initialize(String name, int weight, int age) {
        this.name = name;
        this.weight = weight;
        this.age = age;
        this.color = "Black";
    }

    public void initialize(String name, int age) {
        this.name = name;
        this.age = age;
        this.weight = 100;
        this.color = "Black";
    }

    public void initialize(int weight, String color) {
        this.weight = weight;
        this.color = color;
        this.age = 5;

    }

    public void initialize(int weight, String color, String address) {
        this.weight = weight;
        this.color = color;
        this.address = address;
        this.age = 5;
    }


    public static void main(String[] args) {

    }
}
