package en.javarush.task.jdk13.task05.task0517;

/* 
Creating cats
A cat (Cat class) must have five constructors:

Name
Name, weight, age
Name, age (standard weight)
Weight, color (name, address, and age are unknown; the cat is homeless)
Weight, color, address (someone else's pet)
The constructor's job is to make the object valid.

*/

public class Cat {
    //write your code here
    String name;
    int age;
    int weight;
    String color;
    String address = null;

    public Cat(String name) {
        this.name = name;
        this.age = 5;
        this.weight = 100;
        this.color="Black";
    }

    public Cat(String name,  int weight, int age) {
        this.name = name;
        this.weight = weight;
        this.age = age;
        this.color="Black";
    }
    public Cat(String name, int age) {
        this.name = name;
        this.age = age;
        this.weight = 100;
        this.color="Black";
    }

    public Cat(int weight, String color) {
        this.weight = weight;
        this.color = color;
        this.age = 5;
    }

    public Cat(int weight, String color, String address) {
        this.weight = weight;
        this.color = color;
        this.address = address;
        this.age = 5;
    }

    public static void main(String[] args) {

    }
}
