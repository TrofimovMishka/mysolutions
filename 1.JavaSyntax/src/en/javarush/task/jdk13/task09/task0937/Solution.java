package en.javarush.task.jdk13.task09.task0937;

/* 
Literals
*/

public class Solution {
    //write your code here
    String s = " Hello";
    char c = 'a';
    byte b = 126;
    short sh = 122;
    int i = 10;
    long lo = 1222L;
    float f = 122.5F;
    double d = 344.2D;
    boolean bol = true;

}
