package en.javarush.task.jdk13.task09.task0927;

import java.util.*;

/* 
Ten cats
*/

public class Solution {

    public static void main(String[] args) {
        Map<String, Cat> map = createMap();
        Set<Cat> set = convertMapToSet(map);
        printCatSet(set);
    }

    public static Map<String, Cat> createMap() {
        //write your code here
        Map<String, Cat> map = new HashMap<>();
        map.put("qwe", new Cat("qwe"));
        map.put("asd", new Cat("asd"));
        map.put("zxc", new Cat("zxc"));
        map.put("rty", new Cat("rty"));
        map.put("fgh", new Cat("fgh"));
        map.put("vbn", new Cat("vbn"));
        map.put("uio", new Cat("uio"));
        map.put("jkl", new Cat("jkl"));
        map.put("bnm", new Cat("bnm"));
        map.put("ghj", new Cat("ghj"));
        return map;
    }

    public static Set<Cat> convertMapToSet(Map<String, Cat> map) {
        //write your code here
        Set<Cat> cats = new HashSet<>(map.values());
        return cats;
    }

    public static void printCatSet(Set<Cat> set) {
        for (Cat cat : set) {
            System.out.println(cat);
        }
    }

    public static class Cat {
        public String name;

        public Cat(String name) {
            this.name = name;
        }

        public String toString() {
            return "Cat " + this.name;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof Cat)) {
                return false;
            }
            Cat cat = (Cat) o;
            return Objects.equals(name, cat.name);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name);
        }
    }
}
