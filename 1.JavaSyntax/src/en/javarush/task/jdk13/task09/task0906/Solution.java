package en.javarush.task.jdk13.task09.task0906;

/* 
Logging stack traces
Example output:
Solution.java: main: In method
*/

public class Solution {

    public static void main(String[] args) {
        log("In method");
    }

    public static void log(String text) {
        //write your code here
        String result2 = Thread.currentThread().getStackTrace()[2].getMethodName();
        String result1 = Thread.currentThread().getStackTrace()[2].getFileName();
        System.out.println(result1 +": "+result2+": "+ text);

    }
}
