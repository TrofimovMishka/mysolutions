package en.javarush.task.jdk13.task09.task0930;
/*Solution JR
 for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (isNumber(array[i]) && isNumber(array[j])) {
                    if (Integer.parseInt(array[i]) < Integer.parseInt(array[j])) {
                        String temp = array[i];
                        array[i] = array[j];
                        array[j] = temp;
                    }
                } else if (!isNumber(array[i]) && !isNumber(array[j])) {
                    if (isGreaterThan(array[i], array[j])) {
                        String temp = array[i];
                        array[i] = array[j];
                        array[j] = temp;
                    }
                }
            }
        }
    }
 */

import java.util.Arrays;
import java.util.Collections;

public class ArraySorter {

    public void sort(String[] array2) {
        //write your code here

        //create copy for given array
        String[] copyArray = Arrays.copyOf(array2, array2.length);

        // How length need arrays
        int lengthForString = 0;
        int lengthForInt = 0;
        for (int i = 0; i < array2.length; i++) {
            if (!isNumber(array2[i])) {
                lengthForString++;
            } else {
                lengthForInt++;
            }
        }

        // Create new array only for strings
        String[] array = new String[lengthForString];
        //Fill this array
        for (int i = 0, j = array.length - 1; i < array2.length; i++) {
            if (!isNumber(array2[i])) {
                array[j] = array2[i];
                j--;
            }
        }

        //Sort this array
        String temp;
        for (int out = array.length-1; out >0; out--) {
            for (int in = 0; in < out; in++) {
                if (isGreaterThan(array[in + 1], array[in])) {
                    temp = array[in];
                    array[in] = array[in + 1];
                    array[in + 1] = temp;
                }
            }
        }

        //create array only for Integer
        int[] arrayInt = new int[lengthForInt];
        //Fill this array
        for (
                int i = 0, j = arrayInt.length - 1;
                i < array2.length; i++) {
            if (isNumber(array2[i])) {
                arrayInt[j] = Integer.parseInt(array2[i]);
                j--;
            }
        }
        //Sort this array
        Arrays.sort(arrayInt);

        // Now we have three arrays.
        // Chenge given array
        int j = 0; //for string array (array)
        int g = arrayInt.length-1; //for int array    (arrayInt)
        for (
                int i = 0;
                i < copyArray.length; i++) {
            if (!isNumber(copyArray[i])) {      // this is String
                array2[i] = array[j];             //add string
                j++;
            } else {                           // this is number
                array2[i] = Integer.toString(arrayInt[g]); //add number
                g--;
            }
        }
//        System.out.println(Arrays.toString(array));
//        System.out.println(Arrays.toString(arrayInt));
    }

    // String comparison method: 'a' is greater than 'b'
    public boolean isGreaterThan(String a, String b) {
        return a.compareTo(b) > 0;
    }


    // Is the passed string a number?
    public boolean isNumber(String text) {
        if (text.length() == 0) {
            return false;
        }

        char[] chars = text.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            char character = chars[i];
            if ((i != 0 && character == '-') // The string contains a hyphen
                    || (!Character.isDigit(character) && character != '-') // or is not a number and doesn't start with a hyphen
                    || (chars.length == 1 && character == '-')) // or is a single hyphen
            {
                return false;
            }
        }
        return true;
    }
}
