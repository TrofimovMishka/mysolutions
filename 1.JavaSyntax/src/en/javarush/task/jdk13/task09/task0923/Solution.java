package en.javarush.task.jdk13.task09.task0923;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Vowels and consonants
*/

public class Solution {
    public static char[] vowels = new char[]{'a', 'e', 'i', 'o', 'u'};

    public static void main(String[] args) throws Exception {
        //write your code here
        String oneLine = "";
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            oneLine = reader.readLine();
        }
        String first = "";
        String second ="";
        String s2 = "";

        String[] s = oneLine.split(" ");
        for (int i = 0; i < s.length; i++) {
            s2 +=s[i];
        }
        for (int i = 0; i < s2.length(); i++) {
            if(isVowel(s2.charAt(i))){
                first += s2.charAt(i)+" ";
            }else{
                second += s2.charAt(i)+" ";
            }
        }
        System.out.println(first);
        System.out.println(second);
    }

    // The method checks whether a letter is a vowel
    public static boolean isVowel(char character) {
        character = Character.toLowerCase(character);  // Convert to lowercase
        for (char vowel : vowels) {  // Look for vowels in the array
            if (character == vowel) {
                return true;
            }
        }
        return false;
    }
}