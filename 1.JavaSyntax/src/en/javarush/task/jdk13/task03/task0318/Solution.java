package en.javarush.task.jdk13.task03.task0318;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
How to take over the world
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //write your code here...Kevin will take over the world in 8 years. Mwa-ha-ha!

       BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

       String name = reader.readLine();
       int number = Integer.parseInt(reader.readLine());

        System.out.println(name+" "+"will take over the world in"+" "+number+" "+"years. Mwa-ha-ha!");

    }
}
