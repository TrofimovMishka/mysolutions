package en.javarush.task.jdk13.task03.task0319;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Predictions
name" will receive "number1" in "number2" years.
Here's an example:
Nick will receive 10000 in 5 years.
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //write your code here
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String name = reader.readLine();
        int number1 = Integer.parseInt(reader.readLine());
        int number2 = Integer.parseInt(reader.readLine());
        System.out.println(name + " will receive " + number1 + " in " + number2 + " years.");

    }
}
