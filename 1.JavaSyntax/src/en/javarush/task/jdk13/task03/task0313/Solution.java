package en.javarush.task.jdk13.task03.task0313;

/* 
Sam I Am
*/
// 123/ 213/ 321/ 132/ 312/ 231/
public class Solution {
    public static void main(String[] args) {
        //write your code here
        System.out.printf("%s%s%s", "Sam", "I", "Am");
        System.out.println();
        System.out.printf("%2$s%1$s%3$s", "Sam", "I", "Am");
        System.out.println();
        System.out.printf("%3$s%2$s%1$s", "Sam", "I", "Am");
        System.out.println();
        System.out.printf("%1$s%3$s%2$s", "Sam", "I", "Am");
        System.out.println();
        System.out.printf("%3$s%1$s%2$s", "Sam", "I", "Am");
        System.out.println();
        System.out.printf("%2$s%3$s%1$s", "Sam", "I", "Am");



    }
}
