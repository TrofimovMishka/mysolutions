package en.javarush.task.jdk13.task16.task1605;

import java.io.FileNotFoundException;
import java.io.IOException;

/* 
Throwing exceptions
*/

public class Solution {

    public void method1() throws IOException {

    }

    public void method2() throws IOException {

    }

    public void method3() throws RuntimeException {

    }

    public void method4() throws RuntimeException {

    }
}
