package en.javarush.task.jdk13.task06.task0611;

/* 
StringHelper class
*/

public class StringHelper {
    public static String multiply(String text) {
        String result = String.format("%1$s%1$s%1$s%1$s%1$s",text);
        //write your code here
        return result;
    }

    public static String multiply(String text, int count) {
        String result = "";
        //write your code here
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < count; i++) {
            builder.append(text);
            result = builder.toString();
        }
        return result;
    }

    public static void main(String[] args) {
    }
}
