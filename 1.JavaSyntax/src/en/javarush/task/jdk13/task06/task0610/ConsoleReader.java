package en.javarush.task.jdk13.task06.task0610;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
ConsoleReader class

In the ConsoleReader class, implement four static methods:

String readString() - reads a string from the keyboard
int readInt() - reads a number from the keyboard
double readDouble() - reads a fractional number from the keyboard
boolean readBoolean() - reads the string "true" or "false" from the keyboard and returns the corresponding boolean value (true or false).
Please note: in each method, create a variable to read data from the console (BufferedReader or Scanner).
*/

public class ConsoleReader {
    public static String readString() throws Exception {
        //write your code here
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return reader.readLine();

    }

    public static int readInt() throws Exception {
        //write your code here
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return Integer.parseInt(reader.readLine());

    }

    public static double readDouble() throws Exception {
        //write your code here
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return Double.parseDouble(reader.readLine());

    }

    public static boolean readBoolean() throws Exception {
        //write your code here
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return Boolean.parseBoolean(reader.readLine());
    }

    public static void main(String[] args) throws Exception {

    }
}
