package en.javarush.task.jdk13.task07.task0712;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Minimum or maximum
*/

public class Solution {

    public static ArrayList<String> strings;

    public static void main(String[] args) throws IOException {
        //write your code here
        strings = new ArrayList<>();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        for (int i = 0; i < 10; i++) {
            strings.add(bufferedReader.readLine());
        }

        int indexMin = 0;
        int minimum = strings.get(0).length();

        for (int j = 1; j < strings.size(); j++) {
            if (strings.get(j).length() < minimum) {
                minimum = strings.get(j).length();
                indexMin = j;
            }
        }

        int indexMax = 0;
        int maximum = strings.get(0).length();

        for (int j = 1; j < strings.size(); j++) {
            if (strings.get(j).length() > maximum) {
                maximum = strings.get(j).length();
                indexMax = j;
            }
        }

        if(indexMin < indexMax){
            System.out.println(strings.get(indexMin));
        }else{
            System.out.println(strings.get(indexMax));
        }

    }
}
