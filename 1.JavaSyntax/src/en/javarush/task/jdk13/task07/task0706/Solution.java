package en.javarush.task.jdk13.task07.task0706;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Street and houses
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        //write your code here
        int[] houses = new int[15];
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        for (int i = 0; i < houses.length; i++) {
            houses[i] = Integer.parseInt(reader.readLine());
        }
        int sumOdd = 0;
        int sumEven = houses[0];

        for (int i = 1; i < houses.length; i++) {
            if (i % 2 == 0) {
                sumEven += houses[i];
            } else {
                sumOdd += houses[i];
            }
        }
        if (sumEven == sumOdd){

        }
        else if (sumEven > sumOdd){
            System.out.println("Even-numbered houses have more residents.");
        }
        else if (sumEven < sumOdd){
            System.out.println("Odd-numbered houses have more residents.");
        }

    }
}
