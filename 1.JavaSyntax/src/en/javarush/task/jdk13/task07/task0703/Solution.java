package en.javarush.task.jdk13.task07.task0703;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
2 arrays
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //write your code here
        String[] stringArray = new String[10];
        int[] intArray = new int[10];

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        for (int i = 0; i < stringArray.length; i++) {
            stringArray[i] = reader.readLine();
        }
        for (int i = 0; i < intArray.length; i++) {
            intArray[i] = stringArray[i].length();
            System.out.println(intArray[i]);
        }
    }
}
