package en.javarush.task.jdk13.task07.task0708;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

/* 
The largest number
*/

public class Solution {

    private static ArrayList<Integer> integers = new ArrayList<>();

    public static void main(String[] args) throws Exception {
        //write your code here
        int temp =0;

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        for (int i = 0  ; i < 5; i++) {
            int num = Integer.parseInt(reader.readLine());
            if (num>temp){
                temp = num;
            }
            integers.add(num);

        }
        Integer[] array = integers.toArray(new Integer[0]);
        Arrays.sort(array);

        System.out.println(array[array.length - 1]);

    }
}
