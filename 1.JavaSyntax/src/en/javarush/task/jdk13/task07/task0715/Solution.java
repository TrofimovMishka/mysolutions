package en.javarush.task.jdk13.task07.task0715;

import java.util.ArrayList;

/* 
More Sam-I-Am
*/

public class Solution {
    public static void main(String[] args) {
        //write your code here
        ArrayList<String> list = new ArrayList<>();
        list.add("Sam");
        list.add("Ham");

        list.add("I");
        list.add("Ham");

        list.add("Am");
        list.add("Ham");

        for (String s :
                list) {
            System.out.println(s);
        }
    }
}
