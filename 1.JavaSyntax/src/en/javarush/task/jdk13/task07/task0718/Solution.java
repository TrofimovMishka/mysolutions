package en.javarush.task.jdk13.task07.task0718;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

/* 
Checking the order
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        //write your code here
        ArrayList<String> list = new ArrayList<>();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        for (int i = 0; i < 10; i++) {
            list.add(reader.readLine());
        }

        String s = list.get(0);

        for (int i = 1; i < list.size(); i++) {
            if(list.get(i).length() >= s.length()){
                s = list.get(i);
            }else{
                System.out.println(i);
                break;
            }
        }


    }

}

