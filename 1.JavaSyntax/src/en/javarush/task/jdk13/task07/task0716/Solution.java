package en.javarush.task.jdk13.task07.task0716;

import java.util.ArrayList;

/* 
R or L
*/

public class Solution {
    public static void main(String[] args) {
        ArrayList<String> strings = new ArrayList<String>();
        strings.add("rose");
        strings.add("measure");
        strings.add("love");
        strings.add("lyra");
        strings.add("wade");
        strings.add("bark");
        strings = fix(strings);

        for (String string : strings) {
            System.out.println(string);
        }
    }

    public static ArrayList<String> fix(ArrayList<String> strings) {
        //write your code here
        ArrayList<String> strings1 = new ArrayList<>();

        for (int i = 0; i < strings.size(); i++) {
            if (strings.get(i).contains("r") && !strings.get(i).contains("l")){
                strings.remove(i);
                i--;
            }
        }
        for (int i = 0; i < strings.size(); i++) {
            if(!strings.get(i).contains("r") && strings.get(i).contains("l")){
                strings1.add(strings.get(i));
                strings1.add(strings.get(i));
            }else{
                strings1.add(strings.get(i));
            }
        }

        return strings1;
    }
}