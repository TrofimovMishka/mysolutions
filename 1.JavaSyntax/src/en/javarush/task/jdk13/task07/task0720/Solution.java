package en.javarush.task.jdk13.task07.task0720;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Shuffled just in time
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> list = new ArrayList<>();
        //write your code here
        int n=0;
        int m=0;
        for (int i = 0; i < 1; i++) {
            n = Integer.parseInt(reader.readLine());
            m = Integer.parseInt(reader.readLine());
        }

        for (int i = 0; i < n; i++) {
            list.add(reader.readLine());
        }

        for (int i = 0; i < m; i++) {
            String s = list.get(0);
            list.remove(0);
            list.add(list.size(), s);
        }

        for (String s:
             list) {
            System.out.println(s);
        }
    }
}
