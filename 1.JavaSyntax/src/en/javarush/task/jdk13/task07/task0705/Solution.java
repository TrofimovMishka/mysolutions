package en.javarush.task.jdk13.task07.task0705;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
One large array and two small ones
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //write your code here
        int [] array1 = new int[20];
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        for (int i = 0; i < array1.length; i++) {
            array1[i] = Integer.parseInt(reader.readLine());
        }

        int [] array2 = new int[10];
        int [] array3 = new int[10];

        for (int i = 0; i < array2.length; i++) {
            array2[i] = array1[i];
        }
        for (int i = 0; i < array3.length; i++) {
            array3[i] = array1[i+10];
            System.out.println(array3[i]);
        }
    }
}
