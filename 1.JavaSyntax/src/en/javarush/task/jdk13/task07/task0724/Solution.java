package en.javarush.task.jdk13.task07.task0724;

/* 
Family census
Create the Human class with String name, boolean sex, int age, Human father, and Human mother fields.
Create and populate objects so that we end up with: Two grandfathers, two grandmothers, one father, one mother,
and three children. Display the objects on the screen.
*/

public class Solution {
    public static void main(String[] args) {
        // write your code here
        Human maria = new Human("Maria", false, 26);
        Human monika = new Human("Monika", false, 24);
        Human oleg = new Human("Oleg", true, 34);
        Human igor = new Human("Igor", true, 32);

        Human kira = new Human("Kira", false, 21, oleg, monika);
        Human hum1 = new Human("Grzeg", true, 22, oleg, monika);
        Human hum2 = new Human("Lora", false, 23, oleg, monika);
        Human hum3 = new Human("Jessi", false, 24, oleg, monika);
        Human hum4 = new Human("Polana", false, 25, oleg, monika);

        System.out.println(maria);
        System.out.println(monika);
        System.out.println(oleg);
        System.out.println(igor);
        System.out.println(kira);
        System.out.println(hum1);
        System.out.println(hum2);
        System.out.println(hum3);
        System.out.println(hum4);


    }


    public static class Human {
        // write your code here
        String name;
        boolean sex;
        int age;
        Human father;
        Human mother;

        public Human(String name, boolean sex, int age) {
            this.name = name;
            this.sex = sex;
            this.age = age;
        }

        public Human(String name, boolean sex, int age, Human father, Human mother) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.father = father;
            this.mother = mother;
        }

        public String toString() {
            String text = "";
            text += "Name: " + this.name;
            text += ", sex: " + (this.sex ? "male" : "female");
            text += ", age: " + this.age;

            if (this.father != null) {
                text += ", father: " + this.father.name;
            }

            if (this.mother != null) {
                text += ", mother: " + this.mother.name;
            }

            return text;
        }
    }
}