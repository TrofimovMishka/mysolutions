package en.javarush.task.jdk13.task07.task0707;

import java.util.ArrayList;

/* 
5 different strings in a list
*/

public class Solution {

    public static ArrayList<String> list;

    public static void main(String[] args) {
        //write your code here
        list = new ArrayList<>();
        list.add("Kira");
        list.add("Monika");
        list.add("Nikol");
        list.add("Sara");
        list.add("Ola");

        System.out.println(list.size());

        for (String s: list
             ) {
            System.out.println(s);
        }

    }
}
