package en.javarush.task.jdk13.task07.task0709;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

/* 
Expressing ourselves more concisely
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //write your code here
        ArrayList<String> list = new ArrayList<>();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        for (int i = 0; i < 5; i++) {
            list.add(reader.readLine());
        }

        int lengthString = list.get(0).length();

        for (int j = 1; j < list.size(); j++) {
            if (list.get(j).length() < lengthString) {
                lengthString = list.get(j).length();
            }
        }

        for (String s : list
        ) {
            if (s.length() == lengthString) {
                System.out.println(s);
            }
        }

    }
}
