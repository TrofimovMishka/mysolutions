package example;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

// Если Мы не сможем провести десериализацию этих данных обратно в Java-объекты, т.к. они фактически неразличимы, то:
/*
Во-первых, выделяют некоторое поле, которое используется для того, чтобы отличать один тип от другого. Если его нет – его заводят.
 */
class TestForJSON3 {
    public static void main(String[] args) throws IOException {
        Cat3 cat = new Cat3();
        cat.name = "Murka";
        cat.age = 5;

        Dog dog = new Dog();
        dog.name = "Killer";
        dog.age = 8;
        dog.owner = "Bill Jeferson";

        House house = new House();
        house.pets.add(dog);
        house.pets.add(cat);

        StringWriter writer = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(writer, house);
        System.out.println(writer.toString());
    }
}

//Во-вторых, есть специальные аннотации, которые позволяют управлять процессом «полиморфной десериализации». Вот что можно сделать:
/*
С помощью аннотаций мы указываем, что JSON-представление будет содержать специальное поле type,
 которое будет хранить значение cat, для класса Cat и значение dog, для класса Dog. Этой информации достаточно,
 чтобы выполнить корректную десериализацию объекта: при десериализации по значению поля type будет
  определяться тип объекта, который надо создать.
 */

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Cat3.class, name = "cat"),
        @JsonSubTypes.Type(value = Dog.class, name = "dog")
})
class Pet {
    public String name;
}

class Cat3 extends Pet {
    public int age;
}

class Dog extends Pet {
    public int age;
    public String owner;
}

class House {
    public List<Pet> pets = new ArrayList<>();
}
