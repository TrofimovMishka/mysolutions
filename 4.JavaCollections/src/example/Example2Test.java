package example;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.*;

public class Example2Test{
    private Example2 example2;

    @Before
    public void init(){
        example2 = new Example2();
    }

    @Test
    public void nullStringShouldReturnFalse(){
        assertFalse(example2.isPalindrome(null));
    }

    @Test
    public void oneLetterStringShouldReturnTrue() {
        assertTrue(example2.isPalindrome("a"));
    }

    @Test
    public void twoSameLettersStringShouldReturnTrue(){
        assertTrue(example2.isPalindrome("aa"));
    }

    @Test
    public void twoNotSameLetterStringShouldReturnFalse(){
        assertFalse(example2.isPalindrome("ab"));
    }
}
