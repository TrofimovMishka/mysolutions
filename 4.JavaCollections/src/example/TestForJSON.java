package example;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.StringWriter;


class TestForJSON {
    public static void main(String[] args) throws IOException {
        //Сериализация объекта:
        //создание объекта для сериализации в JSON
        Cat2 cat = new Cat2();
        cat.name = "Murka";
        cat.age = 5;
        cat.weight = 4;

        //писать результат сериализации будем во Writer(StringWriter)
        StringWriter writer = new StringWriter();

        //это объект Jackson, который выполняет сериализацию
        ObjectMapper mapper = new ObjectMapper();

        // сама сериализация: 1-куда, 2-что
        mapper.writeValue(writer, cat);

        //преобразовываем все записанное во StringWriter в строку
        String result = writer.toString();
        System.out.println(result);

        //===========================================================================================
        //Конвертация объекта из JSON
        /*
        String jsonString = "{ \"name\":\"Murka\", \"age\":5, \"weight\":4}";
        StringReader reader = new StringReader(jsonString);

        ObjectMapper mapper = new ObjectMapper();
                                      //откуда,  что
        Cat cat = mapper.readValue(reader, Cat.class);
         */
    }
}
//Класс Cat, объект которого конвертирует в JSON.
/*
К объектам, которые сериализуются/десериализуются в JSON есть несколько требований:
1) поля должны быть видимые: или public или иметь getter’ы и setter’ы;
2) должен быть конструктор по умолчанию (без параметров).
 */

@JsonAutoDetect
class Cat2{

    @JsonProperty("alias")// Изменили имя поля name -> "alias"
    public String name;
    public int age;

    @JsonIgnore
    public int weight;

    Cat2(){}
}
/*
@JsonAutoDetect	 - Ставится перед классом. Помечает класс как готовый к сериализациив JSON.
@JsonIgnore	 - Ставится перед свойством. Свойство игнорируется при сериализации.
@JsonProperty - Ставится перед свойством или getter’ом или setter’ом. Позволяет задать другое имя поля при сериализации.
@JsonPropertyOrder -  Ставится перед классом. Позволяет задать порядок полей для сериализации.


 */
