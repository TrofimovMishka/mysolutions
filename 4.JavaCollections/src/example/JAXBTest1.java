package example;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.StringReader;
import java.io.StringWriter;

public class JAXBTest1 {
    public static void main(String[] args) throws JAXBException {
        Cat4 murka = new Cat4("Murka", 2);

        StringWriter writer = new StringWriter();

        JAXBContext context = JAXBContext.newInstance(Cat4.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        marshaller.marshal(murka, writer);
        String res = writer.toString();
        System.out.println(writer.toString());

        StringReader reader = new StringReader(res);
        JAXBContext context1 = JAXBContext.newInstance(Cat4.class);
        Unmarshaller umMarshaller1 = context1.createUnmarshaller();
       Cat4 newCat = (Cat4) umMarshaller1.unmarshal(reader);
        System.out.println(newCat.name);


    }
}

@XmlType(name = "cat")
@XmlRootElement
class Cat4 {
    public String name;
    public int age;

    public Cat4() {

    }

    public Cat4(String name, int age) {
        this.name = name;
        this.age = age;
    }
}
