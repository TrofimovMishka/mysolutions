package example;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

class Example1 {
    public static void main(String[] args) {
        int [] arr = {7, 2, 3, 5, 1, 6, 4};
        System.out.println(howPairWeHave(arr, 6));
    }

    public static int howPairWeHave(int[] arr , int number){
        int result = 0;
        for (int i : arr) {
            for (int j = i+1; j < arr.length; j++) {
                if(number == arr[i]+arr[j]){
                    result++;
                }
            }
        }

        return result;
    }

    public void fileRead(String fileName, String findMe){
//        try (BufferedReader reader = Files.newBufferedReader(Path.of(fileName))) {
//            reader.lines().filter(line -> line.contains(findMe)).forEach(System.out::println);
//        }catch (IOException ignore){}
    }
}
