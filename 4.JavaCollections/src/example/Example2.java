package example;


class Example2 {
    public static void main(String[] args) {
    }

    public  boolean isPalindrome(String str){
        if(str == null){
            return false;
        }
        if(str.length() == 1){
            return true;
        }

        for (int i = 0, j = str.length()-1; i < j ; i++, j--) {
            if(str.charAt(i) != str.charAt(j)){
                return false;
            }
        }
       return true;
    }
}
