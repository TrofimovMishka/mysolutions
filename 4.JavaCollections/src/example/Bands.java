package example;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Bands {

    Map<Band, List<Musician>> bandsWithMusicians;
    List<Band> bandList;

    public Bands() {
//        bandsWithMusicians = new HashMap<>();
//        bandList = BandsDataProvider.getBands();
//        initialize();
    }

//    private void initialize() {
//        List<Musician> musicians = List.of(
//                new Musician(3, "Hati", Instrument.DRUMS),
//                new Musician(1, "James Alan Hetfield", Instrument.VOCALIST),
//                new Musician(2, "Florin Tibu", Instrument.VOCALIST),
//                new Musician(1, "Lars Ulrich", Instrument.DRUMS),
//                new Musician(3, "Robse", Instrument.VOCALIST),
//                new Musician(1, "Kirk Hammett", Instrument.GUITAR),
//                new Musician(3, "René Berthiaume", Instrument.GUITAR),
//                new Musician(1, "Robert Trujillo", Instrument.BASS),
//                new Musician(2, "Bogdan Mihu", Instrument.DRUMS),
//                new Musician(3, "Dom", Instrument.GUITAR),
//                new Musician(2, "Bogdan Luparu", Instrument.GUITAR),
//                new Musician(2, "Jorge Augusto Coan", Instrument.BASS),
//                new Musician(3, "Marcus Riewaldt", Instrument.BASS)
//        );
//
//        //TODO#1: assign musicians to bands - store this relation in map bandsWithMusicians
//        for (Band band : bandList) {
//            List<Musician> musicianList = musicians
//                    .stream()
//                    .filter(musician -> musician.getBandId() == band.getId())
//                    .collect(Collectors.toList());
//            band.setSize(musicianList.size());
//            bandsWithMusicians.put(band, musicianList);
//        }
//
//        //TODO#3: set size of the band based on amount of musicians
//
//    }

    //TODO#2: implement the method which will return ALL musicians playing on specific instrument
    public List<Musician> getMusiciansPlayingOnInstrument(Instrument instrument) {

       return bandsWithMusicians
                .values()
                .stream()
                .flatMap(List::stream)
                .filter(musician -> musician.getInstrument().equals(instrument))
                .collect(Collectors.toList());
    }

    // assume this implementation is correct
//    public List<Musician> getMusiciansByBandId(int id) {
//        return bandsWithMusicians.get(
//                bandList.stream()
//                        .filter(band -> band.id == id)
//                        .findFirst()
//                        .orElseThrow()
//        );
//    }
//
//    // assume this implementation is correct
//    public int getBandSize(int bandId) {
//        return bandsWithMusicians.keySet().stream()
//                .filter(band -> band.getId() == bandId)
//                .findFirst()
//                .orElseThrow()
//                .getSize();
//    }

//    public static void main(String[] args) {
//        Bands bandsInstance = new Bands();
//
//        System.out.println("Musicians of Metallica (id: 1): ");
//        System.out.println(bandsInstance.getMusiciansByBandId(1));
//        System.out.println("Musicians of Bucovina (id: 2): ");
//        System.out.println(bandsInstance.getMusiciansByBandId(2));
//        System.out.println("Musicians of Equilibrium (id: 3): ");
//        System.out.println(bandsInstance.getMusiciansByBandId(3));
//        System.out.println("=======================================");
//        System.out.println("All guitar players:");
//        System.out.println(bandsInstance.getMusiciansPlayingOnInstrument(Instrument.GUITAR));
//        System.out.println("All drums players:");
//        System.out.println(bandsInstance.getMusiciansPlayingOnInstrument(Instrument.DRUMS));
//        System.out.println("All bass players:");
//        System.out.println(bandsInstance.getMusiciansPlayingOnInstrument(Instrument.BASS));
//        System.out.println("All vocalists:");
//        System.out.println(bandsInstance.getMusiciansPlayingOnInstrument(Instrument.VOCALIST));
//        System.out.println("=======================================");
//                System.out.println("Size of Metallica (should be 4)");
//        System.out.println(bandsInstance.getBandSize(1));
//        System.out.println("Size of Bucovina (should be 4)");
//        System.out.println(bandsInstance.getBandSize(2));
//        System.out.println("Size of Equilibrium (should be 5)");
//        System.out.println(bandsInstance.getBandSize(3));
//    }
}

class Band {
    int id;
    String name;
    int size;

    public Band(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}

class Musician {
    int bandId;
    String name;
    Instrument playedInstrument;

    public Musician(int bandId, String name, Instrument playedInstrument) {
        this.bandId = bandId;
        this.name = name;
        this.playedInstrument = playedInstrument;
    }

    public int getBandId() {
        return bandId;
    }

    public String getName() {
        return name;
    }

    public Instrument getInstrument() {
        return playedInstrument;
    }

    @Override
    public String toString() {
        return "Musician{" +
                "bandId=" + bandId +
                ", name='" + name + '\'' +
                ", playedInstrument=" + playedInstrument +
                '}';
    }
}

enum Instrument {
    GUITAR,
    BASS,
    DRUMS,
    VOCALIST
}

class BandsDataProvider {
//    public static List<Band> getBands() {
//        return List.of(new Band(1, "Metallica"),
//                new Band(2, "Bucovina"),
//                new Band(3, "Equilibrium"));
//    }
}