package example;

import java.util.ArrayList;
import java.util.List;

class Example3 {
    private static List<Integer> fibosNumber = new ArrayList<>();

    public static void main(String[] args) {

        printFib(6);
        fibosNumber.forEach(System.out::print);

    }

    private static int fib(int number) {
        if (number == 0) {
            return 0;
        }
        if (number == 1) {
            return 1;
        }
        return (fib(number - 1) + fib(number - 2));
    }

    private static void printFib(int numbers) {
        for (; numbers > 0; numbers--) {
            fibosNumber.add(0, fib(numbers));
        }
    }

    private static int fac(int number) {
        if (number == 0) {
            return 0;
        }
        if (number == 1) {
            return 1;
        }
        return number * fac(number - 1);
    }

    public static boolean isPrime(int n) {
        if (n <= 1) {
            return false;
        }
        for (int i = 2; i < Math.sqrt(n); i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static void printPrime() {
        int i = 0;
        int num = 0;
        String primeNumbers = "";
        for (i = 1; i <= 100; i++) {
            int counter = 0;
            for (num = i; num >= 1; num--) {
                if (i % num == 0) {
                    counter++;
                }
            }
            if (counter == 2) {
                primeNumbers = primeNumbers + i + " ";
            }
        }
        System.out.println("Prime numbers between 1 and 100 are : \n");
        System.out.println(primeNumbers);
    }
}
