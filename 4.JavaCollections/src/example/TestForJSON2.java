package example;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

class TestForJSON2 {
    public static void main(String[] args) throws IOException {
        //Конвертация объекта из JSON
        String jsonString = "{\"name\":\"Murka\",\"cats\":[{\"name\":\"Timka\"},{\"name\":\"Killer\"}]}";
        ObjectMapper mapper = new ObjectMapper();
        Cat cat = mapper.readValue(jsonString, Cat.class);
        System.out.println(cat);
        System.out.println(cat.cats.getClass());
    }
}
//Класс, объект которого десериализуется из JSON-формата
//А вот при десериализации неясно, какой объект создать — ArrayList или LinkedList?
// (1)Т.е. мы не вмешиваемся и jackson сам определяет классы, которые будут использоваться при десериализации.
@JsonAutoDetect
class Cat {
    public String name;
    // (2) @JsonDeserialize(as = LinkedList.class), где указали, какую реализацию интерфейса List использовать.
    @JsonDeserialize(as = LinkedList.class) // без этого поля это (1)
    public List<Cat> cats;
    Cat(){}
}
/*
Тип коллекции	Как задать тип данных
List	        @JsonDeserialize(contentAs=ValueTypeImpl.class)
Map	            @JsonDeserialize(keyAs=KeyTypeImpl.class)
 */
