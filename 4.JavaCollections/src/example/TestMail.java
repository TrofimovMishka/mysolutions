package example;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.*;
import java.util.Date;
import java.util.Properties;

class TestMail {
    public static void main(String[] args) throws MessagingException {

        String userLogin = "arnold@gmail.com";
        String userPassword = "strong";

        Properties props = new Properties();

        props.put("mail.transport.protocol", "smtps");
        props.put("mail.smtps.ssl.trust", "smtp.gmail.com");
        props.put("mail.smtps.auth", "true");
        props.put("mail.smtps.host", "smtp.gmail.com");
        props.put("mail.smtps.user", userLogin);
        props.put("mail.smtps.starttls.enable", "true");
        props.put("mail.smtp.ssl.enable", "true");

        Session session = Session.getDefaultInstance(props);

        //создаем сообщение
        MimeMessage message = new MimeMessage(session);

        //устанавливаем тему письма
        message.setSubject("тестовое письмо!");

        //добавляем текст письма
        message.setText("Asta la vista, baby!");

        //указываем получателя
        message.addRecipient(Message.RecipientType.TO, new InternetAddress("stalone@gmail.com"));

        //указываем дату отправления
        message.setSentDate(new Date());

        Transport transport = session.getTransport();
        transport.connect("smtp.gmail.com", 465, userLogin, userPassword);
        transport.sendMessage(message, message.getAllRecipients());
    }
}
