package example;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class UserTest {

    @Test
    public void getAllUsers() {
        //создаем тестовые данные
        User user = new User("Евгений", 35, Sex.MALE);
        User user1 = new User("Марина", 34, Sex.FEMALE);
        User user2 = new User("Алина", 7, Sex.FEMALE);

        //создаем список expected и заполняем его данными нашего метода
        List<User> expected = User.getAllUsers();

        //создаем список actual в него помещаем данные для сравнения
        //то что мы предпологиаем метод должен вернуть
        List<User> actual = new ArrayList<>();
        actual.add(user);
        actual.add(user1);
        actual.add(user2);

        //запускаем тест, в случае если список expected и actual не будут равны
        //тест будет провален, о результатах теста читаем в консоли
        Assert.assertEquals(expected, actual);
    }
    //проверить, а не будет ли нам метод getAllUsers() возвращать null
    @Test
    public void getAllUsers_NO_NULL() {
        //добавим проверку на null
        List<User> expected = User.getAllUsers();
        Assert.assertNotNull(expected);
        /*
        Как вы помните, поле allUsers мы инициализировали в конструкторе, и значит при вызове метода getAllUsers(),
         мы обратимся к объекту, который еще не был инициализирован. Будем править, уберем инициализацию из конструктора,
         и сделаем ее при объявлении поля.
         */
    }
    @Test
    public void getAllUsers_Male() {
        //создаем тестовые данные
        User user = new User("Евгений", 35, Sex.MALE);
        User user1 = new User("Марина", 34, Sex.FEMALE);
        User user2 = new User("Алина", 7, Sex.FEMALE);

        //создаем список expected и заполняем его данными нашего метода
        List<User> expected = User.getAllUsers(Sex.MALE);

        //создаем список actual в него помещаем данные для сравнения
        //то что мы предпологиаем метод должен вернуть
        List<User> actual = new ArrayList<>();
        actual.add(user);

        //запускаем тест, в случае если список expected и actual не будут равны
        //тест будет провален, о результатах теста читаем в консоли
        Assert.assertEquals(expected, actual);
    }
    @Test
    public void getAllUsers_Male_NO_Null() {
        List<User> expected = User.getAllUsers(Sex.MALE);
        Assert.assertNotNull(expected);
    }
    @Test
    public void getAllUsers_Female() {
        //создаем тестовые данные
        User user = new User("Евгений", 35, Sex.MALE);
        User user1 = new User("Марина", 34, Sex.FEMALE);
        User user2 = new User("Алина", 7, Sex.FEMALE);

        //создаем список expected и заполняем его данными нашего метода
        List<User> expected = User.getAllUsers(Sex.FEMALE);

        //создаем список actual в него помещаем данные для сравнения
        //то что мы предпологиаем метод должен вернуть
        List<User> actual = new ArrayList<>();
        actual.add(user1);
        actual.add(user2);

        //запускаем тест, в случае если список expected и actual не будут равны
        //тест будет провален, о результатах теста читаем в консоли
        Assert.assertEquals(expected, actual);
    }
    @Test
    public void getAllUsers_Female_NO_Null() {
        List<User> expected = User.getAllUsers(Sex.FEMALE);
        Assert.assertNotNull(expected);
    }

    @Test
    public void getHowManyUsers() {
        User user = new User("Евгений", 35, Sex.MALE);
        User user1 = new User("Марина", 34, Sex.FEMALE);
        User user2 = new User("Алина", 7, Sex.FEMALE);
        User user3 = new User("Nika", 7, Sex.FEMALE);

        int expected = User.getHowManyUsers();
        int actual = 4;
        Assert.assertEquals(expected, actual);
    }
    @Test
    public void getHowManyUsers_Female() {
        User user = new User("Евгений", 35, Sex.MALE);
        User user1 = new User("Марина", 34, Sex.FEMALE);
        User user2 = new User("Алина", 7, Sex.FEMALE);
        User user3 = new User("Nika", 7, Sex.FEMALE);
        int expected = User.getHowManyUsers(Sex.FEMALE);
        int actual = 3;
        Assert.assertEquals(expected, actual);
    }
    @Test
    public void getHowManyUsers_Male() {
        User user = new User("Евгений", 35, Sex.MALE);
        User user1 = new User("Марина", 34, Sex.FEMALE);
        User user2 = new User("Алина", 7, Sex.FEMALE);
        User user3 = new User("Nika", 7, Sex.FEMALE);
        int expected = User.getHowManyUsers(Sex.MALE);
        int actual = 1;
        Assert.assertEquals(expected, actual);
    }


    @Test
    public void getAllAgeUsers() {
        User user = new User("Евгений", 2, Sex.MALE);
        User user1 = new User("Марина", 3, Sex.FEMALE);
        User user2 = new User("Алина", 2, Sex.FEMALE);
        User user3 = new User("Nika", 3, Sex.FEMALE);
        int expected = User.getAllAgeUsers();
        int actual = 10;
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getAllAgeUsers_Female() {
        User user = new User("Евгений", 2, Sex.MALE);
        User user1 = new User("Марина", 3, Sex.FEMALE);
        User user2 = new User("Алина", 2, Sex.FEMALE);
        User user3 = new User("Nika", 3, Sex.FEMALE);
        int expected = User.getAllAgeUsers(Sex.FEMALE);
        int actual = 8;
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getAllAgeUsers_Male() {
        User user = new User("Евгений", 2, Sex.MALE);
        User user1 = new User("Марина", 3, Sex.FEMALE);
        User user2 = new User("Алина", 2, Sex.FEMALE);
        User user3 = new User("Nika", 3, Sex.FEMALE);
        int expected = User.getAllAgeUsers(Sex.MALE);
        int actual = 2;
        Assert.assertEquals(expected, actual);
    }


    @Test
    public void getAverageAgeOfAllUsers() {
        User user = new User("Евгений", 4, Sex.MALE);
        User user1 = new User("Марина", 4, Sex.FEMALE);
        User user2 = new User("Алина", 2, Sex.FEMALE);
        User user3 = new User("Nika", 2, Sex.FEMALE);
        int expected = User.getAverageAgeOfAllUsers();
        int actual = 3;
        Assert.assertEquals(expected, actual);
    }

}