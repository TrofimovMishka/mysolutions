package com.javarush.task.task39.task3905;

public class PhotoPaint {
    public boolean paintFill(Color[][] image, int x, int y, Color desiredColor) {
        if (image == null || image.length == 0 || image[0].length == 0) return false;
        if (y >= image.length || x >= image[0].length) return false;
        if(desiredColor == null) return false;
        if(desiredColor.equals(image[y][x])) return false;

        image[y][x] = desiredColor;
        return true;
    }
}
