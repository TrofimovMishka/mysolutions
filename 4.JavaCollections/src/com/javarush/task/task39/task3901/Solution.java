package com.javarush.task.task39.task3901;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Locale;

/* 
Уникальные подстроки
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please enter your string: ");
        String s = bufferedReader.readLine();

        System.out.println("The longest unique substring length is: \n" + lengthOfLongestUniqueSubstring(s));
    }

    public static int lengthOfLongestUniqueSubstring(String s) {
        if (s == null || s.equals("")) {
            return 0;
        }
        HashSet<String> strings = new HashSet<>();
        String copyString = s.toLowerCase(Locale.ROOT);
        char[] letters = copyString.toCharArray();
        for (int i = 0; i < letters.length; i++) {
            StringBuilder sb = new StringBuilder();
            for (int j = i; j < letters.length; j++) {
                String str = String.valueOf(letters[j]);
                if(sb.indexOf(str)<0){
                    sb.append(str);
                }else{
                    break;
                }
            }
            strings.add(sb.toString());
        }
        int max = 0;
        for (String str :
                strings) {
            if (str.length() > max){
                max = str.length();
            }
        }
        return max;
    }
}
