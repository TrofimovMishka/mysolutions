package com.javarush.task.task39.task3912;

/* 
Максимальная площадь
*/

public class Solution {
    public static void main(String[] args) {
//        int[][] matrix = new int[][]{
//                { 1, 1, 1, 1, 0, 1, 0, 0, 1, 1 },
//                { 1, 0, 1, 1, 0, 1, 0, 0, 1, 1 },
//                { 1, 1, 1, 1, 1, 1, 0, 0, 0, 0 },
//                { 1, 1, 1, 1, 1, 0, 1, 1, 1, 1 },
//                { 0, 0, 1, 1, 1, 1, 1, 1, 1, 1 },
//                { 1, 0, 0, 0, 1, 1, 1, 1, 1, 1 },
//                { 0, 1, 1, 0, 1, 1, 1, 1, 1, 1 },
//                { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
//                { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
//                { 1, 0, 0, 0, 0, 1, 0, 1, 1, 1 }
//        };
//        int[][] matrix1 = new int[][]{
//                {0, 0, 1, 1, 1},
//                {1, 0, 1, 1, 1},
//                {0, 0, 1, 1, 1},
//                {1, 0, 0, 1, 1},
//                {0, 0, 1, 0, 1},
//        };
//        System.out.println(maxSquare(matrix));
    }

    public static int maxSquare(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return 0;
        }

        int[][] array = new int[matrix.length][matrix[0].length];

        for (int i = 0; i < matrix.length; i++) {
//            matrix[i] = new int[matrix[i].length];
            if (i == 0) {
                for (int j = 0; j < matrix[i].length; j++) {
                    array[i][j] = matrix[i][j];
                }
            } else {
                array[i][0] = matrix[i][0];
            }
        }
//        for (int i = 0; i < array.length; i++) {
//            System.out.println(Arrays.toString(array[i]));
//        }
//        System.out.println("-----------------------------------------------");
        int max = 0;
        for (int i = 1; i < matrix.length; i++) {
            for (int j = 1; j < matrix[i].length; j++) {
                if (matrix[i][j] == 0) {
                    array[i][j] = 0;
                } else {
                    int in = Math.min(Math.min(array[i][j - 1], array[i - 1][j - 1]), array[i - 1][j]) + 1;
                    array[i][j] = in;
                    if (in > max) max = in;
                }
            }
        }
        if(max == 0) return 0;
//        for (int i = 0; i < array.length; i++) {
//            System.out.println(Arrays.toString(array[i]));
//        }
        return max * max;
    }
}
