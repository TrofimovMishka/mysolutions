package com.javarush.task.task39.task3909;

/* 
Одно изменение
*/

public class Solution {
    public static void main(String[] args) {

    }

    public static boolean isOneEditAway(String first, String second) {
        if(first == null || second == null || Math.abs(first.length() - second.length()) > 1) return false;
        if(first.equals("") && second.equals("")) return false;
        if(first.equals(second)) return true;

        int whoLonger = first.length() - second.length();

        if(whoLonger > 0){//second is less
            return isOneEditForDiffLength(second, first);
        }else if(whoLonger < 0) {// first is less
            return isOneEditForDiffLength(first, second);
        }else{ // lengths are the same
            return isOneForSameLength(first, second);
        }
    }

    //chek if first less than second
    private static boolean isOneEditForDiffLength(String less, String greater){
        return false;
    }

    private static boolean isOneForSameLength(String one, String two){
        return false;
    }
}
