package com.javarush.task.task39.task3908;

/* 
Возможен ли палиндром?
*/


import java.util.*;

public class Solution {
    public static void main(String[] args) {
//        System.out.println(isPalindromePermutation("aab"));
    }

    public static boolean isPalindromePermutation(String s) {
        boolean result = false;
        Set<String> list = permutationFinder(s.toLowerCase(Locale.ROOT));

        for (String str :
                list) {
//            System.out.print(str+"  ");
            if (isPalindrome(str)){
                result = true;
                break;
            }
        }
        return result;
    }

    private static boolean isPalindrome(String s){
        StringBuilder sb = new StringBuilder();
        for (char c :
                s.toCharArray()) {
            sb.append(c);
        }
        return s.equals(sb.reverse().toString());
    }

        // find all worlds
    public static Set<String> permutationFinder(String str) {
        Set<String> perm = new HashSet<String>();
        if (str == null) {
            return null;
        } else if (str.length() == 0) {
            perm.add("");
            return perm;
        }
        char initial = str.charAt(0);
        String rem = str.substring(1);
        Set<String> words = permutationFinder(rem);
        for (String strNew : words) {
            for (int i = 0;i<=strNew.length();i++){
                perm.add(insertChar(strNew, initial, i));
            }
        }
        return perm;
    }

    public static String insertChar(String str, char c, int j) {
        String begin = str.substring(0, j);
        String end = str.substring(j);
        return begin + c + end;
    }
}
/*
from jr
public static boolean isPalindromePermutation(String s) {
        boolean foundOdd = false;
        int[] tableCount = new int[256];

        for (char c : s.toLowerCase().toCharArray()) {
            tableCount[c] += 1;
        }

        for (int count : tableCount) {
            if (count % 2 != 0) {
                if (foundOdd) {
                    return false;
                }
                foundOdd = true;
            }
        }

        return true;
    }
 */

