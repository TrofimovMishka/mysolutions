package com.javarush.task.task39.task3913;

import com.javarush.task.task39.task3913.query.*;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class LogParser implements IPQuery, UserQuery, DateQuery, EventQuery, QLQuery {
    private final Path path;
    private final List<Path> filesFromPath = new ArrayList<>(); // store all files names
    private List<Log> allData;

    public LogParser(Path path) {
        this.path = path;
        readFileName();
    }

    class MyFileVisitor extends SimpleFileVisitor<Path> {
        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            Path path = file.toAbsolutePath();
            if (path.toString().endsWith(".log")) {
                filesFromPath.add(path);
            }
            return FileVisitResult.CONTINUE;
        }
    }

    private void readFileName() {
        try {
            Files.walkFileTree(path, new MyFileVisitor());
        } catch (IOException e) {
            e.printStackTrace();
        }
        allData = readData();
    }

    private List<Log> readData() {
        List<Log> logs = new ArrayList<>();

        for (Path value : filesFromPath) {
            File fileName = value.toFile();
            try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
                while (reader.ready()) {
                    logs.add(new Log(reader.readLine()));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return logs;
    }

    private boolean isDateInRange(Date check, Date after, Date before) {
        if (after == null && before != null) {
            if (check.getTime() < before.getTime()) {
                return true;
            }
        }
        if (before == null && after != null) {
            if (check.getTime() > after.getTime()) {
                return true;
            }
        }
        if (before != null && after != null) {
            if (check.getTime() < before.getTime() && check.getTime() > after.getTime()) {
                return true;
            }
        }
        if (after == null && before == null) {
            return true;
        }
        return false;
    }

    @Override
    public int getNumberOfUniqueIPs(Date after, Date before) {
        return getUniqueIPs(after, before).size();
    }

    @Override
    public Set<String> getUniqueIPs(Date after, Date before) {
        Set<String> ips = new HashSet<>();
        for (Log log :
                allData) {
            if (isDateInRange(log.getDate(), after, before)) {
                ips.add(log.getIp());
            }
        }
        return ips;
    }

    @Override
    public Set<String> getIPsForUser(String user, Date after, Date before) {
        Set<String> ips = new HashSet<>();
        for (Log log :
                allData) {
            if (isDateInRange(log.getDate(), after, before) && user.equals(log.getUserName())) {
                ips.add(log.getIp());
            }
        }
        return ips;
    }

    @Override
    public Set<String> getIPsForEvent(Event event, Date after, Date before) {
        Set<String> ips = new HashSet<>();
        for (Log log :
                allData) {
            if (isDateInRange(log.getDate(), after, before) && log.getEvent().equals(event)) {
                ips.add(log.getIp());
            }
        }
        return ips;
    }

    @Override
    public Set<String> getIPsForStatus(Status status, Date after, Date before) {
        Set<String> ips = new HashSet<>();
        for (Log log :
                allData) {
            if (isDateInRange(log.getDate(), after, before) && log.getStatus().equals(status)) {
                ips.add(log.getIp());
            }
        }
        return ips;
    }

    @Override
    public Set<String> getAllUsers() {
        Set<String> allUsers = allData.stream()
                .map(Log::getUserName).collect(Collectors.toSet());
        return allUsers;
    }

    @Override
    public int getNumberOfUsers(Date after, Date before) {
        Set<String> allUsers = allData.stream()
                .filter(ele -> isDateInRange(ele.getDate(), after, before))
                .map(Log::getUserName).collect(Collectors.toSet());
        return allUsers.size();
    }

    @Override
    public int getNumberOfUserEvents(String user, Date after, Date before) {
        Set<Event> allUsers = allData.stream()
                .filter(ele -> ele.getUserName().equals(user) &&
                        isDateInRange(ele.getDate(), after, before))
                .map(Log::getEvent).collect(Collectors.toSet());
        return allUsers.size();
    }

    @Override
    public Set<String> getUsersForIP(String ip, Date after, Date before) {
        Set<String> allUsers = allData.stream()
                .filter(log -> isDateInRange(log.getDate(), after, before) && log.getIp().equals(ip))
                .map(Log::getUserName).collect(Collectors.toSet());
        return allUsers;
    }

    @Override
    public Set<String> getLoggedUsers(Date after, Date before) {
        Set<String> allUsers = allData.stream()
                .filter(log -> isDateInRange(log.getDate(), after, before) && log.getEvent().equals(Event.LOGIN))
                .map(Log::getUserName).collect(Collectors.toSet());
        return allUsers;
    }

    @Override
    public Set<String> getDownloadedPluginUsers(Date after, Date before) {
        Set<String> allUsers = allData.stream()
                .filter(log -> isDateInRange(log.getDate(), after, before) && log.getEvent().equals(Event.DOWNLOAD_PLUGIN))
                .map(Log::getUserName).collect(Collectors.toSet());
        return allUsers;
    }

    @Override
    public Set<String> getWroteMessageUsers(Date after, Date before) {
        Set<String> allUsers = allData.stream()
                .filter(log -> isDateInRange(log.getDate(), after, before) && log.getEvent().equals(Event.WRITE_MESSAGE))
                .map(Log::getUserName).collect(Collectors.toSet());
        return allUsers;
    }

    @Override
    public Set<String> getSolvedTaskUsers(Date after, Date before) {
        Set<String> allUsers = allData.stream()
                .filter(log -> isDateInRange(log.getDate(), after, before) && log.getEvent().equals(Event.SOLVE_TASK))
                .map(Log::getUserName).collect(Collectors.toSet());
        return allUsers;
    }

    @Override
    public Set<String> getSolvedTaskUsers(Date after, Date before, int task) {
        Set<String> allUsers = allData.stream()
                .filter(log -> isDateInRange(log.getDate(), after, before)
                        && log.getEvent().equals(Event.SOLVE_TASK)
                        && log.getTaskNumber() == task)
                .map(Log::getUserName).collect(Collectors.toSet());
        return allUsers;
    }

    @Override
    public Set<String> getDoneTaskUsers(Date after, Date before) {
        Set<String> allUsers = allData.stream()
                .filter(log -> isDateInRange(log.getDate(), after, before)
                        && log.getEvent().equals(Event.DONE_TASK))
                .map(Log::getUserName).collect(Collectors.toSet());
        return allUsers;
    }

    @Override
    public Set<String> getDoneTaskUsers(Date after, Date before, int task) {
        Set<String> allUsers = allData.stream()
                .filter(log -> isDateInRange(log.getDate(), after, before)
                        && log.getEvent().equals(Event.DONE_TASK)
                        && log.getTaskNumber() == task)
                .map(Log::getUserName).collect(Collectors.toSet());
        return allUsers;
    }

    @Override
    public Set<Date> getDatesForUserAndEvent(String user, Event event, Date after, Date before) {
        Set<Date> allUsers = allData.stream()
                .filter(log -> isDateInRange(log.getDate(), after, before)
                        && log.getEvent().equals(event)
                        && log.getUserName().equals(user))
                .map(Log::getDate).collect(Collectors.toSet());
        return allUsers;
    }

    @Override
    public Set<Date> getDatesWhenSomethingFailed(Date after, Date before) {
        Set<Date> allUsers = allData.stream()
                .filter(log -> isDateInRange(log.getDate(), after, before)
                        && log.getStatus().equals(Status.FAILED))
                .map(Log::getDate).collect(Collectors.toSet());
        return allUsers;
    }

    @Override
    public Set<Date> getDatesWhenErrorHappened(Date after, Date before) {
        Set<Date> allUsers = allData.stream()
                .filter(log -> isDateInRange(log.getDate(), after, before)
                        && log.getStatus().equals(Status.ERROR))
                .map(Log::getDate).collect(Collectors.toSet());
        return allUsers;
    }

    @Override
    public Date getDateWhenUserLoggedFirstTime(String user, Date after, Date before) {
        Date firstLogin = null;
        Set<Date> allUsers = allData.stream()
                .filter(log -> isDateInRange(log.getDate(), after, before)
                        && log.getUserName().equals(user)
                        && log.getEvent().equals(Event.LOGIN))
                .map(Log::getDate).collect(Collectors.toSet());
        if (!allUsers.isEmpty()) {
            List<Date> dates = new ArrayList<>();
            dates.addAll(allUsers);
            Collections.sort(dates);
            firstLogin = dates.get(0);
        }
        return firstLogin;
    }

    @Override
    public Date getDateWhenUserSolvedTask(String user, int task, Date after, Date before) {
        Date firstTask = null;
        Set<Date> allUsers = allData.stream()
                .filter(log -> isDateInRange(log.getDate(), after, before)
                        && log.getUserName().equals(user)
                        && log.getEvent().equals(Event.SOLVE_TASK)
                        && log.getTaskNumber() == task)
                .map(Log::getDate).collect(Collectors.toSet());
        if (!allUsers.isEmpty()) {
            List<Date> dates = new ArrayList<>();
            dates.addAll(allUsers);
            Collections.sort(dates);
            firstTask = dates.get(0);
        }
        return firstTask;
    }

    @Override
    public Date getDateWhenUserDoneTask(String user, int task, Date after, Date before) {
        Date firstTask = null;
        Set<Date> allUsers = allData.stream()
                .filter(log -> isDateInRange(log.getDate(), after, before)
                        && log.getUserName().equals(user)
                        && log.getEvent().equals(Event.DONE_TASK)
                        && log.getTaskNumber() == task)
                .map(Log::getDate).collect(Collectors.toSet());
        if (!allUsers.isEmpty()) {
            List<Date> dates = new ArrayList<>();
            dates.addAll(allUsers);
            Collections.sort(dates);
            firstTask = dates.get(0);
        }
        return firstTask;
    }

    @Override
    public Set<Date> getDatesWhenUserWroteMessage(String user, Date after, Date before) {
        Set<Date> allUsers = allData.stream()
                .filter(log -> isDateInRange(log.getDate(), after, before)
                        && log.getUserName().equals(user)
                        && log.getEvent().equals(Event.WRITE_MESSAGE))
                .map(Log::getDate).collect(Collectors.toSet());
        return allUsers;
    }

    @Override
    public Set<Date> getDatesWhenUserDownloadedPlugin(String user, Date after, Date before) {
        Set<Date> allUsers = allData.stream()
                .filter(log -> isDateInRange(log.getDate(), after, before)
                        && log.getUserName().equals(user)
                        && log.getEvent().equals(Event.DOWNLOAD_PLUGIN))
                .map(Log::getDate).collect(Collectors.toSet());
        return allUsers;
    }

    @Override
    public int getNumberOfAllEvents(Date after, Date before) {
        return getAllEvents(after, before).size();
    }

    @Override
    public Set<Event> getAllEvents(Date after, Date before) {
        Set<Event> allEvent = allData.stream()
                .filter(log -> isDateInRange(log.getDate(), after, before))
                .map(Log::getEvent).collect(Collectors.toSet());
        return allEvent;
    }

    @Override
    public Set<Event> getEventsForIP(String ip, Date after, Date before) {
        Set<Event> allEvent = allData.stream()
                .filter(log -> isDateInRange(log.getDate(), after, before) && log.getIp().equals(ip))
                .map(Log::getEvent).collect(Collectors.toSet());
        return allEvent;
    }

    @Override
    public Set<Event> getEventsForUser(String user, Date after, Date before) {
        Set<Event> allEvent = allData.stream()
                .filter(log -> isDateInRange(log.getDate(), after, before) && log.getUserName().equals(user))
                .map(Log::getEvent).collect(Collectors.toSet());
        return allEvent;
    }

    @Override
    public Set<Event> getFailedEvents(Date after, Date before) {
        Set<Event> allEvent = allData.stream()
                .filter(log -> isDateInRange(log.getDate(), after, before)
                        && log.getStatus().equals(Status.FAILED))
                .map(Log::getEvent).collect(Collectors.toSet());
        return allEvent;
    }

    @Override
    public Set<Event> getErrorEvents(Date after, Date before) {
        Set<Event> allEvent = allData.stream()
                .filter(log -> isDateInRange(log.getDate(), after, before)
                        && log.getStatus().equals(Status.ERROR))
                .map(Log::getEvent).collect(Collectors.toSet());
        return allEvent;
    }

    @Override
    public int getNumberOfAttemptToSolveTask(int task, Date after, Date before) {
        List<Event> allEvent = allData.stream()
                .filter(log -> isDateInRange(log.getDate(), after, before)
                        && log.getEvent().equals(Event.SOLVE_TASK)
                        && log.getTaskNumber() == task)
                .map(Log::getEvent).collect(Collectors.toList());
        return allEvent.size();
    }

    @Override
    public int getNumberOfSuccessfulAttemptToSolveTask(int task, Date after, Date before) {
        List<Event> allEvent = allData.stream()
                .filter(log -> isDateInRange(log.getDate(), after, before)
                        && log.getEvent().equals(Event.DONE_TASK)
                        && log.getTaskNumber() == task)
                .map(Log::getEvent).collect(Collectors.toList());
        return allEvent.size();
    }

    @Override
    public Map<Integer, Integer> getAllSolvedTasksAndTheirNumber(Date after, Date before) {
        Map<Integer, Integer> solvedTask = allData.stream()
                .filter(log -> isDateInRange(log.getDate(), after, before)
                        && log.getEvent().equals(Event.SOLVE_TASK))
                .collect(Collectors.toMap(Log::getTaskNumber,
                        log -> getNumberOfAttemptToSolveTask(log.getTaskNumber(), after, before),
                        (a, b) -> a));
        return solvedTask;
    }

    @Override
    public Map<Integer, Integer> getAllDoneTasksAndTheirNumber(Date after, Date before) {
        Map<Integer, Integer> solvedTask = allData.stream()
                .filter(log -> isDateInRange(log.getDate(), after, before)
                        && log.getEvent().equals(Event.DONE_TASK))
                .collect(Collectors.toMap(Log::getTaskNumber,
                        log -> getNumberOfSuccessfulAttemptToSolveTask(log.getTaskNumber(), after, before),
                        (a, b) -> a));
        return solvedTask;
    }

    @Override
    public Set<Object> execute(String query) {
        Set<Object> logs = new HashSet<>();
        switch (query) {
            case "get ip":
                logs = allData.stream().map(Log::getIp).collect(Collectors.toSet());
                break;
            case "get user":
                logs = allData.stream().map(Log::getUserName).collect(Collectors.toSet());
                break;
            case "get date":
                logs = allData.stream().map(Log::getDate).collect(Collectors.toSet());
                break;
            case "get event":
                logs = allData.stream().map(Log::getEvent).collect(Collectors.toSet());
                break;
            case "get status":
                logs = allData.stream().map(Log::getStatus).collect(Collectors.toSet());
                break;
        }
        if (logs.isEmpty()) {
            if (query.contains("and")) {
                logs = executeMoreParametrWithDate(query);
            } else {
                logs = executeMoreParametr(query);
            }
        }
        return logs;
    }

    private Set<Object> executeMoreParametr(String query) {
        Set<Object> logs = new HashSet<>();

        String[] strArr = query.split("=");
        String[] strArr2 = strArr[0].split("\\s");

        String field1 = strArr2[1];
        String field2 = strArr2[3];
        String value1 = strArr[1].substring(2, strArr[1].length() - 1);

        switch (field1) {
            case "ip": // поле которое добавим в множество
                switch (field2) {
                    case "user": // если это поле в log
                        String user = value1; // равняется этому полю
                        logs = allData.stream().filter(log -> log.getUserName().equals(user))
                                .map(Log::getIp).collect(Collectors.toSet());
                        break;
                    case "date":
                        Date date = parseToDate(value1);
                        logs = allData.stream().filter(log -> isDateInRange(log.getDate(), date, date))
                                .map(Log::getIp).collect(Collectors.toSet());
                        break;
                    case "event":
                        Event event = parseToEvent(value1);
                        logs = allData.stream().filter(log -> log.getEvent().equals(event))
                                .map(Log::getIp).collect(Collectors.toSet());
                        break;
                    case "status":
                        Status status = parseToStatus(value1);
                        logs = allData.stream().filter(log -> log.getStatus().equals(status))
                                .map(Log::getIp).collect(Collectors.toSet());
                        break;
                }
                break;
            case "user": // поле которое добавим в множество
                switch (field2) {
                    case "ip":// если это поле в log
                        String ip = value1; // равняется этому полю
                        logs = allData.stream().filter(log -> log.getIp().equals(ip))
                                .map(Log::getUserName).collect(Collectors.toSet());
                        break;
                    case "date":
                        Date date = parseToDate(value1);
                        logs = allData.stream().filter(log -> isDateInRange(log.getDate(), date, date))
                                .map(Log::getUserName).collect(Collectors.toSet());
                        break;
                    case "event":
                        Event event = parseToEvent(value1);
                        logs = allData.stream().filter(log -> log.getEvent().equals(event))
                                .map(Log::getUserName).collect(Collectors.toSet());
                        break;
                    case "status":
                        Status status = parseToStatus(value1);
                        logs = allData.stream().filter(log -> log.getStatus().equals(status))
                                .map(Log::getUserName).collect(Collectors.toSet());
                        break;
                }
                break;
            case "date": // поле которое добавим в множество
                switch (field2) {
                    case "ip": // если это поле в log
                        String ip = value1; // равняется этому полю
                        logs = allData.stream().filter(log -> log.getIp().equals(ip))
                                .map(Log::getDate).collect(Collectors.toSet());
                        break;
                    case "user":
                        String user = value1;
                        logs = allData.stream().filter(log -> log.getUserName().equals(user))
                                .map(Log::getDate).collect(Collectors.toSet());
                        break;
                    case "event":
                        Event event = parseToEvent(value1);
                        logs = allData.stream().filter(log -> log.getEvent().equals(event))
                                .map(Log::getDate).collect(Collectors.toSet());
                        break;
                    case "status":
                        Status status = parseToStatus(value1);
                        logs = allData.stream().filter(log -> log.getStatus().equals(status))
                                .map(Log::getDate).collect(Collectors.toSet());
                        break;
                }
                break;
            case "event": // поле которое добавим в множество
                switch (field2) {
                    case "ip": // если это поле в log
                        String ip = value1; // равняется этому полю
                        logs = allData.stream().filter(log -> log.getIp().equals(ip))
                                .map(Log::getEvent).collect(Collectors.toSet());
                        break;
                    case "user":
                        String user = value1;
                        logs = allData.stream().filter(log -> log.getUserName().equals(user))
                                .map(Log::getEvent).collect(Collectors.toSet());
                        break;
                    case "date":
                        Date date = parseToDate(value1);
                        logs = allData.stream().filter(log -> isDateInRange(log.getDate(), date, date))
                                .map(Log::getEvent).collect(Collectors.toSet());
                        break;
                    case "status":
                        Status status = parseToStatus(value1);
                        logs = allData.stream().filter(log -> log.getStatus().equals(status))
                                .map(Log::getEvent).collect(Collectors.toSet());
                        break;
                }
                break;
            case "status": // поле которое добавим в множество
                switch (field2) {
                    case "ip":// если это поле в log
                        String ip = value1; // равняется этому полю
                        logs = allData.stream().filter(log -> log.getIp().equals(ip))
                                .map(Log::getStatus).collect(Collectors.toSet());
                        break;
                    case "user":
                        String user = value1;
                        logs = allData.stream().filter(log -> log.getUserName().equals(user))
                                .map(Log::getStatus).collect(Collectors.toSet());
                        break;
                    case "date":
                        Date date = parseToDate(value1);
                        logs = allData.stream().filter(log -> isDateInRange(log.getDate(), date, date))
                                .map(Log::getStatus).collect(Collectors.toSet());
                        break;
                    case "event":
                        Event event = parseToEvent(value1);
                        logs = allData.stream().filter(log -> log.getEvent().equals(event))
                                .map(Log::getStatus).collect(Collectors.toSet());
                        break;
                }
                break;
        }
        return logs;
    }

    private Set<Object> executeMoreParametrWithDate(String query) {
        Set<Object> logs = new HashSet<>();
        String[] strArr = query.split("=");
        String[] strArr2 = strArr[0].split("\\s");
        String[] strArr3 = strArr[1].split("\"");

        String field1 = strArr2[1];
        String field2 = strArr2[3];
        String value1 = strArr3[1];
        Date after = parseToDate(strArr3[3]);
        Date before = parseToDate(strArr3[5]);

        switch (field1) {
            case "ip": // поле которое добавим в множество
                switch (field2) {
                    case "user": // если это поле в log
                        String user = value1; // равняется этому полю
                        logs = allData.stream().filter(log -> log.getUserName().equals(user)
                                        && isDateInRange(log.getDate(), after, before))
                                .map(Log::getIp).collect(Collectors.toSet());
                        break;
                    case "date":
                        Date date = parseToDate(value1);
                        logs = allData.stream().filter(log -> isDateInRangeIncluding(log.getDate(), date, date)
                                        && isDateInRange(log.getDate(), after, before))
                                .map(Log::getIp).collect(Collectors.toSet());
                        break;
                    case "event":
                        Event event = parseToEvent(value1);
                        logs = allData.stream().filter(log -> log.getEvent().equals(event)
                                        && isDateInRange(log.getDate(), after, before))
                                .map(Log::getIp).collect(Collectors.toSet());
                        break;
                    case "status":
                        Status status = parseToStatus(value1);
                        logs = allData.stream().filter(log -> log.getStatus().equals(status)
                                        && isDateInRange(log.getDate(), after, before))
                                .map(Log::getIp).collect(Collectors.toSet());
                        break;
                }
                break;
            case "user": // поле которое добавим в множество
                switch (field2) {
                    case "ip":// если это поле в log
                        String ip = value1; // равняется этому полю
                        logs = allData.stream().filter(log -> log.getIp().equals(ip)
                                        && isDateInRange(log.getDate(), after, before))
                                .map(Log::getUserName).collect(Collectors.toSet());
                        break;
                    case "date":
                        Date date = parseToDate(value1);
                        logs = allData.stream().filter(log -> isDateInRangeIncluding(log.getDate(), date, date)
                                        && isDateInRange(log.getDate(), after, before))
                                .map(Log::getUserName).collect(Collectors.toSet());
                        break;
                    case "event":
                        Event event = parseToEvent(value1);
                        logs = allData.stream().filter(log -> log.getEvent().equals(event)
                                        && isDateInRange(log.getDate(), after, before))
                                .map(Log::getUserName).collect(Collectors.toSet());
                        break;
                    case "status":
                        Status status = parseToStatus(value1);
                        logs = allData.stream().filter(log -> log.getStatus().equals(status)
                                        && isDateInRange(log.getDate(), after, before))
                                .map(Log::getUserName).collect(Collectors.toSet());
                        break;
                }
                break;
            case "date": // поле которое добавим в множество
                switch (field2) {
                    case "ip": // если это поле в log
                        String ip = value1; // равняется этому полю
                        logs = allData.stream().filter(log -> log.getIp().equals(ip)
                                        && isDateInRange(log.getDate(), after, before))
                                .map(Log::getDate).collect(Collectors.toSet());
                        break;
                    case "user":
                        String user = value1;
                        logs = allData.stream().filter(log -> log.getUserName().equals(user)
                                        && isDateInRange(log.getDate(), after, before))
                                .map(Log::getDate).collect(Collectors.toSet());
                        break;
                    case "event":
                        Event event = parseToEvent(value1);
                        logs = allData.stream().filter(log -> log.getEvent().equals(event)
                                        && isDateInRange(log.getDate(), after, before))
                                .map(Log::getDate).collect(Collectors.toSet());
                        break;
                    case "status":
                        Status status = parseToStatus(value1);
                        logs = allData.stream().filter(log -> log.getStatus().equals(status)
                                        && isDateInRange(log.getDate(), after, before))
                                .map(Log::getDate).collect(Collectors.toSet());
                        break;
                }
                break;
            case "event": // поле которое добавим в множество
                switch (field2) {
                    case "ip": // если это поле в log
                        String ip = value1; // равняется этому полю
                        logs = allData.stream().filter(log -> log.getIp().equals(ip)
                                        && isDateInRange(log.getDate(), after, before))
                                .map(Log::getEvent).collect(Collectors.toSet());
                        break;
                    case "user":
                        String user = value1;
                        logs = allData.stream().filter(log -> log.getUserName().equals(user)
                                        && isDateInRange(log.getDate(), after, before))
                                .map(Log::getEvent).collect(Collectors.toSet());
                        break;
                    case "date":
                        Date date = parseToDate(value1);
                        logs = allData.stream().filter(log -> isDateInRangeIncluding(log.getDate(), date, date)
                                        && isDateInRange(log.getDate(), after, before))
                                .map(Log::getEvent).collect(Collectors.toSet());
                        break;
                    case "status":
                        Status status = parseToStatus(value1);
                        logs = allData.stream().filter(log -> log.getStatus().equals(status)
                                        && isDateInRange(log.getDate(), after, before))
                                .map(Log::getEvent).collect(Collectors.toSet());
                        break;
                }
                break;
            case "status": // поле которое добавим в множество
                switch (field2) {
                    case "ip":// если это поле в log
                        String ip = value1; // равняется этому полю
                        logs = allData.stream().filter(log -> log.getIp().equals(ip)
                                        && isDateInRange(log.getDate(), after, before))
                                .map(Log::getStatus).collect(Collectors.toSet());
                        break;
                    case "user":
                        String user = value1;
                        logs = allData.stream().filter(log -> log.getUserName().equals(user)
                                        && isDateInRange(log.getDate(), after, before))
                                .map(Log::getStatus).collect(Collectors.toSet());
                        break;
                    case "date":
                        Date date = parseToDate(value1);
                        logs = allData.stream().filter(log -> isDateInRangeIncluding(log.getDate(), date, date)
                                        && isDateInRange(log.getDate(), after, before))
                                .map(Log::getStatus).collect(Collectors.toSet());
                        break;
                    case "event":
                        Event event = parseToEvent(value1);
                        logs = allData.stream().filter(log -> log.getEvent().equals(event)
                                        && isDateInRange(log.getDate(), after, before))
                                .map(Log::getStatus).collect(Collectors.toSet());
                        break;
                }
                break;
        }

        return logs;
    }

    private boolean isDateInRangeIncluding(Date check, Date after, Date before) {
        if (after == null && before != null) {
            if (check.getTime() <= before.getTime()) {
                return true;
            }
        }
        if (before == null && after != null) {
            if (check.getTime() >= after.getTime()) {
                return true;
            }
        }
        if (before != null && after != null) {
            if (check.getTime() <= before.getTime() && check.getTime() >= after.getTime()) {
                return true;
            }
        }
        if (after == null && before == null) {
            return true;
        }
        return false;
    }

    private Date parseToDate(String str) {
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        try {
            date = format.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    private Event parseToEvent(String str) {
        Event event = null;
        switch (str) {
            case "DONE_TASK":
                event = Event.DONE_TASK;
                break;
            case "LOGIN":
                event = Event.LOGIN;
                break;
            case "SOLVE_TASK":
                event = Event.SOLVE_TASK;
                break;
            case "DOWNLOAD_PLUGIN":
                event = Event.DOWNLOAD_PLUGIN;
                break;
            case "WRITE_MESSAGE":
                event = Event.WRITE_MESSAGE;
                break;
        }
        return event;
    }

    private Status parseToStatus(String str) {
        Status status = null;
        switch (str) {
            case "OK":
                status = Status.OK;
                break;
            case "FAILED":
                status = Status.FAILED;
                break;
            case "ERROR":
                status = Status.ERROR;
                break;
        }
        return status;
    }

}