package com.javarush.task.task39.task3913;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class Log {
    private String userName;
    private String ip;
    private Date date;
    private Event event;
    private Status status;
    private final String allInfo;
    private final String[] allInfoArray;

    public Log(String allInfo) {
        this.allInfo = Objects.requireNonNull(allInfo);
        allInfoArray = allInfo.split("\t");
        parseToDate();
        parseToEvent();
        parseToStatus();
        parseNameAndIp();
    }

    private void parseToDate() {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        try {
            if (allInfoArray.length < 4) {
                throw new IllegalArgumentException("Check information from file. Not enough data");
            }
            date = format.parse(allInfoArray[2]);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void parseToEvent() {
        if (allInfoArray.length < 4) {
            throw new IllegalArgumentException("Check information from file. Not enough data");
        }
        String eventString = allInfoArray[3].split("\\s")[0];
        switch (eventString) {
            case "DONE_TASK":
                event = Event.DONE_TASK;
                break;
            case "LOGIN":
                event = Event.LOGIN;
                break;
            case "SOLVE_TASK":
                event = Event.SOLVE_TASK;
                break;
            case "DOWNLOAD_PLUGIN":
                event = Event.DOWNLOAD_PLUGIN;
                break;
            case "WRITE_MESSAGE":
                event = Event.WRITE_MESSAGE;
                break;
            default:
                if (event == null) {
                    throw new IllegalArgumentException("Check information from file. Event not find");
                }
        }
    }

    public int getTaskNumber(){
        int number = -1;
        String[] array = allInfoArray[3].split("\\s");
        if( array.length == 2){
            String numberTask = array[1];
            try{
                number = Integer.parseInt(numberTask);
            }catch (Exception e){
                System.out.println("Some problem with number task for user");
            }
        }
        return number;
    }

    private void parseNameAndIp(){
        if (allInfoArray.length < 4) {
            throw new IllegalArgumentException("Check information from file. Not enough data");
        }
        userName = allInfoArray[1];
        ip = allInfoArray[0];
    }

    private void parseToStatus() {
        if (allInfoArray.length < 4) {
            throw new IllegalArgumentException("Check information from file. Not enough data");
        }
        String statusString = allInfoArray[4].trim();
        switch (statusString) {
            case "OK":
                status = Status.OK;
                break;
            case "FAILED":
                status = Status.FAILED;
                break;
            case "ERROR":
                status = Status.ERROR;
                break;
            default:
                if (status == null) {
                    throw new IllegalArgumentException("Check information from file. Status not find");
                }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Log log = (Log) o;
        return Objects.equals(userName, log.userName) && Objects.equals(ip, log.ip) && Objects.equals(date, log.date) && event == log.event && status == log.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userName, ip, date, event, status);
    }

    public String getUserName() {
        return userName;
    }

    public String getIp() {
        return ip;
    }

    public Date getDate() {
        return date;
    }

    public Event getEvent() {
        return event;
    }

    public Status getStatus() {
        return status;
    }

}
