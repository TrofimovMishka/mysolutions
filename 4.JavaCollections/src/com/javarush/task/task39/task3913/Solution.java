package com.javarush.task.task39.task3913;

import java.nio.file.Paths;
import java.time.Month;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

public class Solution {
    public static void main(String[] args) {
        //C:\Users\Mike\Documents\JavaRushTasks\4.JavaCollections\src\com\javarush\task\task39\task3913\logs\
        LogParser logParser = new LogParser(Paths.get("C:\\Users\\Mike\\Documents\\JavaRushTasks\\4.JavaCollections\\src\\com\\javarush\\task\\task39\\task3913\\logs\\"));
        Set<String> ips =logParser.getDoneTaskUsers( null, null);
        ips.forEach(System.out::println);
    }
}