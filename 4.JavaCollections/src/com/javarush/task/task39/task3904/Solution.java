package com.javarush.task.task39.task3904;

import java.util.Arrays;

/* 
Лестница
*/

public class Solution {
    private static int n = 6;

    public static void main(String[] args) {
        System.out.println("The number of possible ascents for " + n + " steps is: " + numberOfPossibleAscents(n));
    }

    public static long numberOfPossibleAscents(int n) {
        if(n<0)return 0L;
        if(n<=1)return 1L;
        if(n==2)return 2L;

        long[] l = new long[n];
        l[0]=1L;
        l[1]=2L;
        l[2]=4L;

        for (int i = 3; i < n; i++) {
            l[i] = l[i-3]+l[i-2]+l[i-1];
        }

        return l[n-1];
    }
}

