package com.javarush.task.task39.task3903;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Неравноценный обмен
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Please enter a number: ");

        long number = Long.parseLong(reader.readLine());
        System.out.println("Please enter the first index: ");
        int i = Integer.parseInt(reader.readLine());
        System.out.println("Please enter the second index: ");
        int j = Integer.parseInt(reader.readLine());

        System.out.println("The result of swapping bits is " + swapBits(number, i, j));
    }

    public static long swapBits(long number, int i, int j) {
        if((number &(1l<<i))==(1l<<i)){// i == 1
            if(!((number &(1l<<j))==(1l<<j))){ //j == 0
                // set i in 0
                number = number & ~(1l<<i);
                //set j in 1
                number = number | (1l<<j);
            }
        }else{//i==0
            if((number &(1l<<j))==(1l<<j)){ //j == 1
                // set j in 0
                number = number & ~(1l<<j);
                //set i in 1
                number = number | (1l<<i);
            }
        }
        return number;
    }
}
/*
from jr
if (((number >>> i) & 1) != ((number >>> j) & 1)) {
            long bitMask = (1L << i) | (1L << j);
            number ^= bitMask;
        }
 */