package com.javarush.task.task26.task2613;

import com.javarush.task.task26.task2613.exception.InterruptOperationException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ResourceBundle;

public class ConsoleHelper {
    private static final BufferedReader bis = new BufferedReader(new InputStreamReader(System.in));
    private static ResourceBundle res = ResourceBundle.getBundle(CashMachine.RESOURCE_PATH + "exit");

    public static void writeMessage(String message) {
        System.out.println(message);
    }

    public static String readString() throws InterruptOperationException{
        String result = "";
        try {
            result = bis.readLine();
            if(result.equalsIgnoreCase("exit")){
                throw new InterruptOperationException();
            }
        } catch (IOException ignore) {
        }
        return result;
    }

    public static String askCurrencyCode() throws InterruptOperationException{
        writeMessage("Enter currency code:");
        String result = "";
        while (!(result = readString()).matches("[a-zA-Z]{3}")) {
            writeMessage("Wrong currency code");
            writeMessage("Enter correct currency code:");
        }
        return result.toUpperCase();
    }

    public static String[] getValidTwoDigits(String currencyCode) throws InterruptOperationException{ //  где первый элемент - номинал, второй - количество банкнот.
        writeMessage(currencyCode + ": enter denomination and amount ");
        String str = "";
        while (!(str = readString()).matches("\\d+ \\d+")) {
            throw new IllegalArgumentException();
//            writeMessage("Wrong denomination or amount, enter correct digits!");
        }
        return str.split(" ");
    }

    public static Operation askOperation() throws InterruptOperationException{
        writeMessage("Chose operation: "
                + "\n 0 - Login; "
                + "\n 1 - Info; "
                + "\n 2 - Deposit; "
                + "\n 3 - Withdraw; "
                + "\n 4 - Exit.");

        String chose = "";
        while (!(chose = readString()).matches("\\d{1}")) {
            writeMessage("Warning! Non-existent operation!");
            writeMessage("Chose correct operation: "
                    + "\n 0 - Login; "
                    + "\n 1 - Info; "
                    + "\n 2 - Deposit; "
                    + "\n 3 - Withdraw; "
                    + "\n 4 - Exit.");
        }
        if(chose.equals("0")){
            throw new IllegalArgumentException();
        }
        return Operation.getAllowableOperationByOrdinal(Integer.parseInt(chose));
    }

    public static void printExitMessage(){
        System.out.println(res.getString("thank.message"));

    }

}