package com.javarush.task.task26.task2613;

import com.javarush.task.task26.task2613.exception.NotEnoughMoneyException;

import java.util.*;
import java.util.stream.Collectors;

public class CurrencyManipulator {
    private String currencyCode;
    private Map<Integer, Integer> denominations = new TreeMap<>();

    public CurrencyManipulator(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void addAmount(int denomination, int count) {
        if (denominations.containsKey(denomination)) {
            int countInMap = denominations.get(denomination);
            denominations.put(denomination, count + countInMap);
        } else {
            denominations.put(denomination, count);
        }
    }

    public int getTotalAmount() {
        return denominations.entrySet()
                .stream()
                .mapToInt(entry -> (entry.getKey() * entry.getValue()))
                .sum();
    }

    public boolean hasMoney() {
        boolean result = false;
        if (denominations == null || denominations.isEmpty()) {
            return false;
        }
        for (Integer currentMoney :
                denominations.keySet()) {
            if (denominations.get(currentMoney) > 0) {
                result = true;
                break;
            }
        }
        return result;
    }

    public boolean isAmountAvailable(int expectedAmount) {
        return getTotalAmount() >= expectedAmount;
    }

    public Map<Integer, Integer> withdrawAmount(int expectedAmount) throws NotEnoughMoneyException{
        Map<Integer, Integer> map = new HashMap<>();
        List<Integer> bills = sortBills(new ArrayList<>(denominations.keySet()));
        for (Integer bill :
                bills) {
            if (bill > expectedAmount) {continue;}

            int countInCashMachine = denominations.get(bill);
            int takenBanknotes = 0;

            for (int i = countInCashMachine; i > 0 && expectedAmount >= bill; i--) {
                expectedAmount -= bill;
                takenBanknotes++;
                countInCashMachine--;
            }
            map.put(bill, takenBanknotes);

            if (expectedAmount == 0) {
                break;
            }
        }

        if(expectedAmount > 0){
            throw new NotEnoughMoneyException();
        }

        //Withdraw money
        Set<Integer> allBillsWithdraw = map.keySet();
        for (Integer oneBill : allBillsWithdraw){
            int newCount = denominations.get(oneBill) - map.get(oneBill);
            denominations.put(oneBill, newCount);
        }

        return map;
    }

    // sorting from largest to smallest
    private List<Integer> sortBills(List<Integer> list) {
        list.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2 - o1;
            }
        });
        return list;
    }

}