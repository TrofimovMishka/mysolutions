package com.javarush.task.task26.task2613.command;

import com.javarush.task.task26.task2613.CashMachine;
import com.javarush.task.task26.task2613.ConsoleHelper;
import com.javarush.task.task26.task2613.CurrencyManipulator;
import com.javarush.task.task26.task2613.CurrencyManipulatorFactory;

import java.util.Collection;
import java.util.ResourceBundle;

class InfoCommand implements Command {
    private ResourceBundle res = ResourceBundle.getBundle(CashMachine.RESOURCE_PATH + "info");

    @Override
    public void execute() {
        ConsoleHelper.writeMessage(res.getString("before"));
        Collection<CurrencyManipulator> collection = CurrencyManipulatorFactory.getAllCurrencyManipulators();
        if (collection == null || collection.isEmpty()) {
            ConsoleHelper.writeMessage(res.getString("no.money"));
        }
        collection
                .stream()
                .forEach(cm -> {
                    if (cm.hasMoney()) {
                        ConsoleHelper.writeMessage(cm.getCurrencyCode() + " - " + cm.getTotalAmount());
                    } else {
                        ConsoleHelper.writeMessage(res.getString("no.money"));
                    }
                });
    }
}
