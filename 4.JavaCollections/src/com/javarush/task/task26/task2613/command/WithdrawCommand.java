package com.javarush.task.task26.task2613.command;

import com.javarush.task.task26.task2613.CashMachine;
import com.javarush.task.task26.task2613.ConsoleHelper;
import com.javarush.task.task26.task2613.CurrencyManipulator;
import com.javarush.task.task26.task2613.CurrencyManipulatorFactory;
import com.javarush.task.task26.task2613.exception.InterruptOperationException;
import com.javarush.task.task26.task2613.exception.NotEnoughMoneyException;

import java.util.*;

class WithdrawCommand implements Command {
    private ResourceBundle res = ResourceBundle.getBundle(CashMachine.RESOURCE_PATH + "withdraw");

    @Override
    public void execute() throws InterruptOperationException {
        CurrencyManipulator manipulator = CurrencyManipulatorFactory
                .getManipulatorByCurrencyCode(ConsoleHelper.askCurrencyCode());

        boolean notStopOperation = true;
        while(notStopOperation) {
            requestOfMoney();
            String amountOfMoney = "";
            while (!(amountOfMoney = ConsoleHelper.readString()).matches("\\d+")) {
                ConsoleHelper.writeMessage(res.getString("specify.not.empty.amount"));
                requestOfMoney();
            }
            if (!(manipulator.isAmountAvailable(Integer.parseInt(amountOfMoney)))) {
                ConsoleHelper.writeMessage(res.getString("not.enough.money"));
                continue;
            }

            try {
                Map<Integer, Integer> money = manipulator.withdrawAmount(Integer.parseInt(amountOfMoney));
                List<Integer> bills = sortBills(new ArrayList<>(money.keySet()));

                bills.forEach(key -> ConsoleHelper.writeMessage(String.format(res.getString("success.format"), key, money.get(key).toString())));
//                ConsoleHelper.writeMessage("Transaction completed successfully");
                notStopOperation = false;
            } catch (NotEnoughMoneyException e) {
                ConsoleHelper.writeMessage(res.getString("exact.amount.not.available"));
            }
        }
    }

    private void requestOfMoney(){
        ConsoleHelper.writeMessage(res.getString("before"));
        ConsoleHelper.writeMessage(res.getString("specify.amount"));
    }


    private List<Integer> sortBills(List<Integer> list) {
        list.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2 - o1;
            }
        });
        return list;
    }
}
