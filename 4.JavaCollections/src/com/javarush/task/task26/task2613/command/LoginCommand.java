package com.javarush.task.task26.task2613.command;

import com.javarush.task.task26.task2613.CashMachine;
import com.javarush.task.task26.task2613.ConsoleHelper;
import com.javarush.task.task26.task2613.exception.InterruptOperationException;

import java.util.ResourceBundle;

class LoginCommand implements Command {

    private ResourceBundle validCreditCards = ResourceBundle.getBundle(CashMachine.RESOURCE_PATH + "verifiedCards");
    private ResourceBundle res = ResourceBundle.getBundle(CashMachine.RESOURCE_PATH+ "login");

    @Override
    public void execute() throws InterruptOperationException {
        ConsoleHelper.writeMessage(res.getString("before"));
        while (true) {
            String cardNumberFromUser = getCardNumber();
            String pinFromUser = getPinNumber();

            if (validCreditCards.containsKey(cardNumberFromUser) &&
                    (validCreditCards.getString(cardNumberFromUser)).equals(pinFromUser)) {
                ConsoleHelper.writeMessage(String.format(res.getString("success.format"), cardNumberFromUser));
                break;
            } else {
                ConsoleHelper.writeMessage(res.getString("try.again.with.details"));
            }
        }
    }

    private String getCardNumber() throws InterruptOperationException {
        ConsoleHelper.writeMessage(res.getString("specify.data"));
        String cardNumberFromUser = ConsoleHelper.readString();
        while (!isInformationFromUserValid(cardNumberFromUser, 12)) {
            ConsoleHelper.writeMessage(String.format(res.getString("not.verified.format"), cardNumberFromUser));
            ConsoleHelper.writeMessage(res.getString("try.again.or.exit"));
            cardNumberFromUser = ConsoleHelper.readString();
        }
        return cardNumberFromUser;
    }

    private String getPinNumber() throws InterruptOperationException {
        ConsoleHelper.writeMessage("Enter pin number:");
        String pinFromUser = ConsoleHelper.readString();
        while (!isInformationFromUserValid(pinFromUser, 4)) {
            ConsoleHelper.writeMessage("Wrong pin number! Enter valid pin number");
            pinFromUser = ConsoleHelper.readString();
        }
        return pinFromUser;
    }

    private boolean isInformationFromUserValid(String fromUser, int needLength) {
        if (fromUser == null || !fromUser.matches(String.format("\\d{%d}", needLength))) {
            return false;
        }
        return true;
    }

}
