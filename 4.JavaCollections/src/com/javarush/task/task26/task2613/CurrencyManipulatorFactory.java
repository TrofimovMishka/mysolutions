package com.javarush.task.task26.task2613;

import java.util.*;
import java.util.stream.Collectors;

public class CurrencyManipulatorFactory {
    private static Map<String, CurrencyManipulator> map = new TreeMap<>();
    
    private CurrencyManipulatorFactory(){}
    
    public static CurrencyManipulator getManipulatorByCurrencyCode(String currencyCode){
       Set<String> keys = map.keySet();
        for(String str : keys){
            if(str.equalsIgnoreCase(currencyCode)){
                return map.get(str);
            }
        }
        
        CurrencyManipulator newCurrency = new CurrencyManipulator(currencyCode);
        map.put(currencyCode, newCurrency);
        return newCurrency;
    }

    public static Collection<CurrencyManipulator> getAllCurrencyManipulators(){
        return map.entrySet()
                .stream()
                .map(entry -> entry.getValue())
                .collect(Collectors.toCollection(ArrayList::new));
    }
}