package com.javarush.task.task31.task3102;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;

/* 
Находим все файлы
*/

public class Solution {
    public static List<String> getFileTree(String root) throws IOException {
        Path path = Paths.get(root);
        Files.walkFileTree(path, new MyFileVisitor());
        List<String> arrayList = new ArrayList<>();
        MyFileVisitor.strings.forEach(arrayList::add);

        return arrayList;
    }


    static class MyFileVisitor implements FileVisitor<Path>{
        public static Deque<String> strings = new ArrayDeque<>();

        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            strings.offer(file.toFile().getAbsolutePath());
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
            return FileVisitResult.TERMINATE;
        }

        @Override
        public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
            return FileVisitResult.CONTINUE;
        }

    }

    public static void main(String[] args) {
//        Path path = Paths.get("C:\\Users\\Mike\\OneDrive\\Рабочий стол\\Files_for_tasks");
//        String s = path.toString();
//        System.out.println(s);
    }
}
