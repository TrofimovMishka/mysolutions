package com.javarush.task.task31.task3111;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

public class SearchFileVisitor extends SimpleFileVisitor<Path> {
    private List<Path> foundFiles = new ArrayList<>();
    private String partOfName = null;
    private String partOfContent = null;
    private int minSize = -1;
    private int maxSize = -1;

    public void setPartOfName(String partOfName) {
        this.partOfName = partOfName;
    }

    public void setPartOfContent(String partOfContent) {
        this.partOfContent = partOfContent;
    }

    public void setMinSize(int minSize) {
        this.minSize = minSize;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    public List<Path> getFoundFiles() {
        return foundFiles;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        byte[] content = Files.readAllBytes(file); // размер файла: content.length

        if (partOfName != null) {
            if (!file.getFileName().toString().contains(partOfName)) {
                return FileVisitResult.CONTINUE;
            }
        }

        if (partOfContent != null) {
           String infoFromFile = new String (content);
           if(!infoFromFile.contains(partOfContent)){
               return FileVisitResult.CONTINUE;
           }
        }

        if(maxSize >= 0){
            if (!(content.length < maxSize)){
                return FileVisitResult.CONTINUE;
            }
        }

        if(minSize >= 0){
            if (!(content.length > minSize)){
                return FileVisitResult.CONTINUE;
            }
        }

        foundFiles.add(file);
        return super.visitFile(file, attrs);
    }


}
