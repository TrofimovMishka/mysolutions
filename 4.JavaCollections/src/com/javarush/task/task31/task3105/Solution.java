package com.javarush.task.task31.task3105;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/* 
Добавление файла в архив
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        if (args.length < 2) {
            return;
        }
        File pathFile = new File(args[0]);
        List<Content> entries = getContent(args[1]); // write all info from zip and save to new obj.Content

        ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(args[1]));

        zos.putNextEntry(new ZipEntry("new/"+pathFile.getName())); // add new directory to zip

        Files.copy(pathFile.toPath(), zos); // copy file

        for (Content content : entries) {
            if (!content.getFileName().equals("new/" + pathFile.getName())){ content.saveToZip(zos);}
        }
        zos.close();
    }

    private static List<Content> getContent(String zip){
        List<Content> entries = new ArrayList<>();

        try(ZipInputStream zis = new ZipInputStream(new FileInputStream(zip))) {
            byte [] buffer = new byte[1024];
            ZipEntry currentEntry;
            while((currentEntry = zis.getNextEntry()) != null){
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                int length = 0;
                while ((length = zis.read(buffer)) > 0) {
                    outputStream.write(buffer, 0, length);
                }
                entries.add(new Content(currentEntry.getName(), outputStream.toByteArray()));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return entries;
    }


    static class Content {
        String fileName;
        byte[] body;

        public Content(String fileName, byte[] body) {
            this.fileName = fileName;
            this.body = body;
        }

        public String getFileName() {
            return fileName;
        }

        void saveToZip (ZipOutputStream zos) throws IOException {
            ZipEntry zipEntry = new ZipEntry(fileName);
            zos.putNextEntry(zipEntry);
            zos.write(body);
            zos.closeEntry();
        }
    }
}


