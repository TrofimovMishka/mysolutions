package com.javarush.task.task31.task3113;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/* 
Что внутри папки?
*/

public class Solution {

    public static void main(String[] args) throws IOException {
        String directoryName;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            directoryName = reader.readLine();
        }
        Path path = Paths.get(directoryName);
        if (!Files.isDirectory(path)) {
            System.out.println(path.toString() + " - не папка");
            return;
        }

        getInfo(path);
    }

    public static void getInfo(Path direcctoryName) throws IOException {
        MyVisitor myVisitor = new MyVisitor();
        Files.walkFileTree(direcctoryName, myVisitor);
        System.out.println("Всего папок - " + myVisitor.getAllDirectories());
        System.out.println("Всего файлов  - " + myVisitor.getAllFiles());
        System.out.println("Общий размер - " + myVisitor.getSumSize());
    }
}

class MyVisitor implements FileVisitor<Path> {
    private int allDirectories = -1;
    private int allFiles = 0;
    private long sumSize = 0;

    public int getAllDirectories() {
        return allDirectories;
    }

    public int getAllFiles() {
        return allFiles;
    }

    public long getSumSize() {
        return sumSize;
    }


    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        if (attrs.isDirectory()) {
            allDirectories++;
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        if(attrs.isRegularFile()){
            allFiles++;
            sumSize += Files.size(file);
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
        if (Files.isDirectory(file)) {
            allDirectories++;
        }else if(Files.isRegularFile(file)){
            allFiles++;
            sumSize += Files.size(file);
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        return FileVisitResult.CONTINUE;
    }

}
