package com.javarush.task.task33.task3310;

import com.javarush.task.task33.task3310.strategy.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Solution {
    public static void main(String[] args) {
        testStrategy(new HashMapStorageStrategy(), 10000);
        testStrategy(new OurHashMapStorageStrategy(), 10000);
        testStrategy(new FileStorageStrategy(), 10);
        testStrategy(new OurHashBiMapStorageStrategy(), 10000);
        testStrategy(new HashBiMapStorageStrategy(), 10000);
        testStrategy(new DualHashBidiMapStorageStrategy(), 10000);
    }

    public static Set<Long> getIds(Shortener shortener, Set<String> strings){
        Set<Long> ids = new HashSet<>();
        for (String str :
                strings) {
            ids.add(shortener.getId(str));
        }
        return ids;
    }

    public static Set<String> getStrings(Shortener shortener, Set<Long> keys){
        Set<String> strings = new HashSet<>();
        for (Long key :
                keys) {
            strings.add(shortener.getString(key));
        }
        return strings;
    }

    public static void testStrategy(StorageStrategy strategy, long elementNumber){
        //6.2.3.1
        Helper.printMessage(strategy.getClass().getSimpleName());
        //6.2.3.2
        Set<String> strings = new HashSet<>();
        for (long i = 0L; i < elementNumber; i++) {
            strings.add(Helper.generateRandomString());
        }
        //6.2.3.3
        Shortener shortener = new Shortener(strategy);

        //6.2.3.4
        long moment1 = new Date().getTime();
        Set<Long> longs = getIds(shortener, strings);
        long moment2 = new Date().getTime();
        Helper.printMessage(Long.toString(moment2-moment1));

        //6.2.3.5
        long moment3 = new Date().getTime();
        Set<String> stringFromMethod = getStrings(shortener, longs);
        long moment4 = new Date().getTime();
        Helper.printMessage(Long.toString(moment4-moment3));

        //6.2.3.6
        if(strings.equals(stringFromMethod)){
            Helper.printMessage("Тест пройден.");
        }else{
            Helper.printMessage("Тест не пройден.");
        }
    }
}
