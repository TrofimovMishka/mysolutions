package com.javarush.task.task33.task3310.strategy;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileBucket {
    private Path path;

    public FileBucket(){
        try {
            this.path = Files.createTempFile("tempFile", null);
            Files.deleteIfExists(path);
            Files.createFile(path);
            path.toFile().deleteOnExit();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public long getFileSize(){
        long size = 0;
        try{
            size = Files.size(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return size;
    }

    public void putEntry(Entry entry){
        try (OutputStream outputStream = Files.newOutputStream(path);
             ObjectOutputStream oos = new ObjectOutputStream(outputStream)) {
            oos.writeObject(entry);
        } catch (IOException e) {
        }
    }

    public Entry getEntry(){
        Entry entry = null;
        try (InputStream inputStream = Files.newInputStream(path);
             ObjectInputStream ois = new ObjectInputStream(inputStream)) {

            if(getFileSize() > 0){
                entry = (Entry) ois.readObject();
            }

        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        } catch (ClassNotFoundException e) {
        }
        return entry;
    }

    public void remove(){
        try {
            Files.delete(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
