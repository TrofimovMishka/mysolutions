package com.javarush.task.task33.task3310.tests;

import com.javarush.task.task33.task3310.Shortener;
import com.javarush.task.task33.task3310.strategy.*;
import org.junit.Assert;
import org.junit.Test;

public class FunctionalTest {
    public void testStorage(Shortener shortener){
        String one = "Hello bro";
        String two = "Some krakaziabra";
        String three = "Hello bro";

        long idForOne = shortener.getId(one);
        long idForTwo = shortener.getId(two);
        long idForThree = shortener.getId(three);

        Assert.assertNotEquals(idForTwo, idForOne);
        Assert.assertNotEquals(idForTwo, idForThree);

        Assert.assertEquals(idForOne, idForThree);

        String strOne = shortener.getString(idForOne);
        String strTwo = shortener.getString(idForTwo);
        String strThree = shortener.getString(idForThree);

        Assert.assertEquals(one, strOne);
        Assert.assertEquals(two, strTwo);
        Assert.assertEquals(three, strThree);
    }

    @Test
    public void testHashMapStorageStrategy(){
        StorageStrategy strategy = new HashMapStorageStrategy();
        Shortener shortener = new Shortener(strategy);
        testStorage(shortener);
    }
    @Test
    public void testOurHashMapStorageStrategy(){
        StorageStrategy strategy = new OurHashMapStorageStrategy();
        Shortener shortener = new Shortener(strategy);
        testStorage(shortener);
    }
    @Test
    public void testFileStorageStrategy(){
        StorageStrategy strategy = new FileStorageStrategy();
        Shortener shortener = new Shortener(strategy);
        testStorage(shortener);
    }
    @Test
    public void testHashBiMapStorageStrategy(){
        StorageStrategy strategy = new HashBiMapStorageStrategy();
        Shortener shortener = new Shortener(strategy);
        testStorage(shortener);
    }
    @Test
    public void testDualHashBidiMapStorageStrategy(){
        StorageStrategy strategy = new DualHashBidiMapStorageStrategy();
        Shortener shortener = new Shortener(strategy);
        testStorage(shortener);
    }
    @Test
    public void testOurHashBiMapStorageStrategy(){
        StorageStrategy strategy = new OurHashBiMapStorageStrategy();
        Shortener shortener = new Shortener(strategy);
        testStorage(shortener);
    }
}