package com.javarush.task.task33.task3310.tests;

import com.javarush.task.task33.task3310.Helper;
import com.javarush.task.task33.task3310.Shortener;
import com.javarush.task.task33.task3310.Solution;
import com.javarush.task.task33.task3310.strategy.HashBiMapStorageStrategy;
import com.javarush.task.task33.task3310.strategy.HashMapStorageStrategy;
import org.junit.Test;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class SpeedTest {
    public long getTimeToGetIds(Shortener shortener, Set<String> strings, Set<Long> ids){
        long moment1 = new Date().getTime();
        ids = Solution.getIds(shortener, strings);
        long moment2 = new Date().getTime();
        return moment2-moment1;
    }

    public long getTimeToGetStrings(Shortener shortener, Set<Long> ids, Set<String> strings){
        long moment3 = new Date().getTime();
        strings = Solution.getStrings(shortener, ids);
        long moment4 = new Date().getTime();
        return moment4-moment3;
    }

    @Test
    public void testHashMapStorage(){
        Shortener shortener1 = new Shortener(new HashMapStorageStrategy());
        Shortener shortener2 = new Shortener(new HashBiMapStorageStrategy());

        Set<String> origStrings = new HashSet<>();
        for (int i = 0; i < 10000; i++) {
            origStrings.add(Helper.generateRandomString());
        }

        Set<Long> idsForShortener1 = new HashSet<>();
        Set<Long> idsForShortener2 = new HashSet<>();

        long timeIds1 = getTimeToGetIds(shortener1, origStrings, idsForShortener1);
        long timeIds2 = getTimeToGetIds(shortener2, origStrings, idsForShortener2);
        assertTrue(timeIds1>timeIds2);

        Set<String> strForShortener1 = new HashSet<>();
        Set<String> strForShortener2 = new HashSet<>();

        long timeStr1 = getTimeToGetStrings(shortener1, idsForShortener1, strForShortener1);
        long timeStr2 = getTimeToGetStrings(shortener2, idsForShortener2, strForShortener2);
        assertEquals(timeStr1, timeStr2, 40);

    }

}