package com.javarush.task.task20.task2028;

import java.io.Serializable;
import java.util.*;

/* 
Построй дерево(1)
*/

public class CustomTree extends AbstractList<String> implements Cloneable, Serializable {
    Entry<String> root;
    List<Entry<String>> entries = new ArrayList<Entry<String>>();

    public CustomTree() {
        root = new Entry<>("0");
        entries.add(root);
    }

    public boolean remove(Object o){
        List<Entry<String>> entriesCopy = entries;
        if(o instanceof String){
            String needRemove =(String) o;
            for (int i = 0; i < entriesCopy.size(); i++) {
                Entry node = entries.get(i);
                if(node.elementName.equals(needRemove)){
                    Entry nodeParent = node.parent;
                    if(nodeParent.leftChild == node){
                        nodeParent.leftChild = null;
                    }else {
                        nodeParent.rightChild = null;
                    }
                    delete(node);
                    return true;
                }
            }

        }else{
            throw new UnsupportedOperationException();
        }
        return false;
    }
    public void delete (Entry<String> node){
        node.parent = null;
        entries.remove(node);
        //  создаем рекурсивное прохождение по веткам от него
        if(node.leftChild != null){
            delete(node.leftChild);
        }
        node.leftChild = null;
        if(node.rightChild != null){
            delete(node.rightChild);
        }
        node.rightChild = null;
    }

    public boolean add(String s) {
        List<Entry<String>> entriesCopy = entries;
        Entry<String> newNode = new Entry<>(s);
        for (int i = 0; i < entriesCopy.size(); i++) {

            Entry current = entries.get(i);

            if(current.leftChild == null){
                current.leftChild = newNode;
                newNode.parent = current;
                entries.add(newNode);
                return true;

            }else if(current.rightChild == null){
                current.rightChild = newNode;
                newNode.parent = current;
                entries.add(newNode);
                return true;
            }
        }
        return false;
    }

    public String getParent(String s) {
        String result = null;
        Optional<String> optional = entries.stream().filter(current -> current.elementName.equals(s))
                .map(current -> current.parent.elementName).findFirst();
        if(optional.isPresent()){
            result = optional.get();
        }
        return result;
    }

    public void getAllElement(){
        entries.forEach(node -> System.out.print(node.elementName+" | "));
    }

    @Override
    public String get(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int size() {
        return entries.size()-1;
    }

    @Override
    public String set(int index, String element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void add(int index, String element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String remove(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<String> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected void removeRange(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(int index, Collection<? extends String> c) {
        throw new UnsupportedOperationException();
    }

    static class Entry<T> implements Serializable {

        String elementName;
        boolean availableToAddLeftChildren;
        boolean availableToAddRightChildren;
        Entry<T> parent;
        Entry<T> leftChild;
        Entry<T> rightChild;

        public Entry(String elementName) {
            this.elementName = elementName;
            this.availableToAddLeftChildren = true;
            this.availableToAddRightChildren = true;
        }

        public boolean isAvailableToAddChildren() {// дизъюнкция boolean - это OR
            return availableToAddLeftChildren || availableToAddRightChildren;
        }
    }

}
