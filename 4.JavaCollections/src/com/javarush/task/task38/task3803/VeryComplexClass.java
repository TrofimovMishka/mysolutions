package com.javarush.task.task38.task3803;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/* 
Runtime исключения (unchecked exception)
*/

public class VeryComplexClass {
    public void methodThrowsClassCastException() {
       Object b = 12;
       String s = (String) b;

    }

    public void methodThrowsNullPointerException() {
            String str = null;
            str.substring(0,1);
    }

    public static void main(String[] args) {

    }
}
