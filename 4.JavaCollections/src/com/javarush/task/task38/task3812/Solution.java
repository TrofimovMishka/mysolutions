package com.javarush.task.task38.task3812;

/* 
Обработка аннотаций
*/

import java.lang.annotation.Annotation;

public class Solution {
    public static void main(String[] args) {
        printFullyQualifiedNames(Solution.class);
        printFullyQualifiedNames(SomeTest.class);

        printValues(Solution.class);
        printValues(SomeTest.class);
    }

    public static boolean printFullyQualifiedNames(Class c) {
        boolean isAnnotation = false;

        if (c.isAnnotationPresent(PrepareMyTest.class)) {
            PrepareMyTest annotation = (PrepareMyTest) c.getAnnotation(PrepareMyTest.class);
            for (String str :
                    annotation.fullyQualifiedNames()) {
                System.out.println(str);
            }
            isAnnotation = true;
        }
        return isAnnotation;
    }

    public static boolean printValues(Class c) {
        boolean isAnnotation = false;
        if (c.isAnnotationPresent(PrepareMyTest.class)) {
            PrepareMyTest annotation = (PrepareMyTest) c.getAnnotation(PrepareMyTest.class);
            for (Class clazz :
                 annotation.value()) {
                System.out.println(clazz.getSimpleName());
            }
            isAnnotation = true;
        }
        return isAnnotation;
    }
}
