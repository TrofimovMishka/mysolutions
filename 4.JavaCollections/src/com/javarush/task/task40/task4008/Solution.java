package com.javarush.task.task40.task4008;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;

/* 
Работа с Java 8 DateTime API
*/

public class Solution {
    public static void main(String[] args) {
        printDate("9.10.2017 5:56:45");
        System.out.println();
        printDate("21.4.2014");
        System.out.println();
        printDate("17:33:40");
    }

    public static void printDate(String date) {

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("d.M.y");
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("H:m:s");
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("d.M.y H:m:s");

        try {
            LocalDate actualDate = LocalDate.parse(date, dateFormatter);
            printActualDate(actualDate);
        } catch (Exception ignore) {
        }

        try {
            LocalTime time = LocalTime.parse(date, timeFormatter);
            printTime(time);
        } catch (Exception ignore) {
        }

        try {
            LocalDateTime dateTime = LocalDateTime.parse(date, dateTimeFormatter);
            printActualDate(dateTime.toLocalDate());
            printTime(dateTime.toLocalTime());
        } catch (Exception ignore) {
        }
    }

    private static void printTime(LocalTime time) {

        System.out.println("AM или PM: " + time.format(DateTimeFormatter.ofPattern("a")));
        System.out.println("Часы: " + time.get(ChronoField.CLOCK_HOUR_OF_AMPM));
        System.out.println("Часы дня: " + time.getHour());
        System.out.println("Минуты: " + time.getMinute());
        System.out.println("Секунды: " + time.getSecond());
    }

    private static void printActualDate(LocalDate actualDate) {
        System.out.println("День: " + actualDate.getDayOfMonth());
        System.out.println("День недели: " + actualDate.getDayOfWeek().getValue());
        System.out.println("День месяца: " + actualDate.getDayOfMonth());
        System.out.println("День года: " + actualDate.getDayOfYear());
        System.out.println("Неделя месяца: " + actualDate.format(DateTimeFormatter.ofPattern("W")));
        System.out.println("Неделя года: " + actualDate.format(DateTimeFormatter.ofPattern("w")));
        System.out.println("Месяц: " + actualDate.getMonthValue());
        System.out.println("Год: " + actualDate.getYear());
    }
}
