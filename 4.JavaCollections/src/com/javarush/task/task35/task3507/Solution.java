package com.javarush.task.task35.task3507;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

/* 
ClassLoader - что это такое?
defineClass работает с реальным именем класса - нужно указывать полное имя класса (с пакетом).
Его можно получить с помощью relativize() - разница между исходным каталогом
  (Solution.class.getProtectionDomain().getCodeSource().getLocation().getPath()) и полным путем к файлу .class.
*/

public class Solution {
    public static void main(String[] args) {
        Set<? extends Animal> allAnimals = getAllAnimals(Solution.class.getProtectionDomain()
                .getCodeSource().getLocation().getPath() + Solution.class.getPackage()
                .getName().replaceAll("[.]", "/") + "/data");
        System.out.println(allAnimals);
    }


    public static Set<? extends Animal> getAllAnimals(String pathToAnimals) {
        // Получаем список доступных класов
        HashSet<Animal> animals = new HashSet<>();
        try {
            MyClassLoader loader = new MyClassLoader();
            File dir = new File(pathToAnimals);
            String[] allClasses = dir.list();
            for (String oneClass :
                    allClasses) {
                Path path = Paths.get(pathToAnimals+"/"+oneClass);
                Class<?> clazz = loader.defineClass(oneClass, Files.readAllBytes(path));
                animals.add((Animal) clazz.getDeclaredConstructor().newInstance());
            }
        } catch (IOException | NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return animals;
    }

    // Self ClassLoader
    static class MyClassLoader extends ClassLoader {
        public Class<?> defineClass(String name, byte[] classBytes) {
            return defineClass(null, classBytes, 0, classBytes.length);
        }
    }
}