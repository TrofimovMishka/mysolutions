package com.javarush.task.task35.task3512;

import java.lang.reflect.InvocationTargetException;

public class Generator<T> {
    private Class<T> type;

    public Generator(Class<T> type) {
        this.type = type;
    }

    T newInstance() {
        T newObject = null;
        try {
            newObject = type.getDeclaredConstructor().newInstance();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return newObject; // type.newInstance() - is deprecated in java 9
    }
}
