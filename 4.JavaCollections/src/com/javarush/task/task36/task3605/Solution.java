package com.javarush.task.task36.task3605;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.TreeSet;

/* 
Использование TreeSet
"C:\Users\Mike\OneDrive\Рабочий стол\Files_for_tasks\allFilesContent.txt"
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        if(args.length<1){
            return;
        }
        TreeSet<Character> treeSet = new TreeSet<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(args[0]))) {
            while (reader.ready()){
                char ch = (char)reader.read();
                if(Character.isLetter(ch))
                treeSet.add(Character.toLowerCase(ch));
            }
        }
        treeSet.stream().limit(5).forEach(System.out::print);
    }
}
