package com.javarush.task.task36.task3606;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/* 
Осваиваем ClassLoader и Reflection
*/

public class Solution {
    private List<Class> hiddenClasses = new ArrayList<>();
    private String packageName;

    public Solution(String packageName) {
        this.packageName = packageName;
    }

    public static void main(String[] args) throws ClassNotFoundException {
        Solution solution = new Solution(Solution.class.getProtectionDomain()
                .getCodeSource().getLocation().getPath() + "com/javarush/task/task36/task3606/data/second");
        solution.scanFileSystem();
        System.out.println(solution.getHiddenClassObjectByKey("secondhiddenclassimpl"));
        System.out.println(solution.getHiddenClassObjectByKey("firsthiddenclassimpl"));
        System.out.println(solution.getHiddenClassObjectByKey("packa"));
    }

    public void scanFileSystem() throws ClassNotFoundException {
        try {
            MyClassLoader loader = new MyClassLoader();
            File dir = new File(packageName);
            String[] allClasses = dir.list();
            for (String oneClass :
                    allClasses) {
                if (oneClass.endsWith(".class")) {
                    Path path = Paths.get(packageName + "/" + oneClass);
                    Class<?> clazz = loader.defineClass(oneClass, Files.readAllBytes(path));
                    hiddenClasses.add(clazz);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public HiddenClass getHiddenClassObjectByKey(String key) {
        HiddenClass object = null;
        for (Class clazz :
                hiddenClasses) {
            if ((clazz.getSimpleName().toLowerCase().startsWith(key.toLowerCase()))) {
                try {
                    Constructor constructor = clazz.getDeclaredConstructor();
                    constructor.setAccessible(true);
                    object = (HiddenClass) constructor.newInstance();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return object;
    }

    static class MyClassLoader extends ClassLoader {
        public Class<?> defineClass(String name, byte[] classBytes) {
            return defineClass(null, classBytes, 0, classBytes.length);
        }
    }
}
