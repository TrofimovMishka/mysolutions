package com.javarush.task.task36.task3610;

import java.io.Serializable;
import java.util.*;

public class MyMultiMap<K, V> extends HashMap<K, V> implements Cloneable, Serializable {
    static final long serialVersionUID = 123456789L;
    private HashMap<K, List<V>> map;
    private int repeatCount;

    public MyMultiMap(int repeatCount) {
        this.repeatCount = repeatCount;
        map = new HashMap<>();
    }

    @Override
    public int size() {
        //напишите тут ваш код
        int size = 0;
        Iterator<K> iterator = map.keySet().iterator();
        while (iterator.hasNext()) {
            List<V> list = map.get(iterator.next());
            size += list.size();
        }
        return size;
    }

    @Override
    public V put(K key, V value) {
        //напишите тут ваш код
        V obj = null;
        //Если в мапе такой ключ уже есть
        if (map.containsKey(key)) {
            List<V> list = map.get(key);
            //и количество значений по этому ключу меньше, чем repeatCount
            if (list.size() < repeatCount) {
                list.add(value);
            } else if (list.size() == repeatCount) {
                list.remove(0);
                list.add(value);
            }
            obj = list.get(list.size() - 2);
        } else {
            List<V> list = new ArrayList<>();
            list.add(value);
            map.put(key, list);
        }
        return obj;
    }

    @Override
    public V remove(Object key) {
        //напишите тут ваш код
        if (!map.containsKey(key)) {
            return null;
        }

        List<V> list = map.get(key);
        V obj = null;
        if (list.size() > 0) {
            obj = list.get(0);
            list.remove(0);
        }
        if (list.size() == 0) {
            map.remove(key, list);
        }
        return obj;
    }

    @Override
    public Set<K> keySet() {
        //напишите тут ваш код
        return map.keySet();
    }

    @Override
    public Collection<V> values() {
        //напишите тут ваш код
        List<V> arrayList = new ArrayList<>();

        Iterator<K> iterator = map.keySet().iterator();
        while (iterator.hasNext()) {
            arrayList.addAll(map.get(iterator.next()));
        }
        return arrayList;
    }

    @Override
    public boolean containsKey(Object key) {
        //напишите тут ваш код
        return map.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        //напишите тут ваш код
        boolean isHere = false;
        Iterator<K> iterator = map.keySet().iterator();
        while (iterator.hasNext()) {
            List<V> list = map.get(iterator.next());
            if (list.contains(value)) {
                isHere = true;
                return isHere;
            }
        }
        return isHere;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("{");
        for (Map.Entry<K, List<V>> entry : map.entrySet()) {
            sb.append(entry.getKey());
            sb.append("=");
            for (V v : entry.getValue()) {
                sb.append(v);
                sb.append(", ");
            }
        }
        String substring = sb.substring(0, sb.length() - 2);
        return substring + "}";
    }
}