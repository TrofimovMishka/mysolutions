package com.javarush.task.task28.task2810;

import com.javarush.task.task28.task2810.model.*;
import com.javarush.task.task28.task2810.view.HtmlView;

public class Aggregator {
    public static void main(String[] args) {
        HtmlView view = new HtmlView();
        Strategy plStrategy = new PracaPLStrategy(); // work not finish - need some details fixing bag in this class
        Strategy hhStrategy = new HHStrategy();
        Strategy habrStrategy = new HabrCareerStrategy();

        Model model = new Model(view, new Provider(habrStrategy));
        Controller controller = new Controller(model);

        view.setController(controller);
        view.userCitySelectEmulationMethod();
    }
}
