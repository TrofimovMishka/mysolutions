package com.javarush.task.task28.task2810.view;

import com.javarush.task.task28.task2810.Controller;
import com.javarush.task.task28.task2810.vo.Vacancy;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.util.List;

public class HtmlView implements View {
    private Controller controller;
    private final String filePath = "./4.JavaCollections/src/" + this.getClass()
            .getPackage().getName().replaceAll("\\.", "/") + "/vacancies.html";

    @Override
    public void update(List<Vacancy> vacancies) {
        try {
            String str = getUpdatedFileContent(vacancies);
            updateFile(str);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }

    public void userCitySelectEmulationMethod() {
        controller.onCitySelect("Odessa");
    }

    private String getUpdatedFileContent(List<Vacancy> vacancies) {
        Document doc = null;
        try {
            doc = getDocument();

            Elements elements = doc.getElementsByClass("template"); // получи элементы, у которых есть класс "template".
            Elements cloneElements = elements.clone().removeAttr("style").removeClass("template"); // Сделай копию этого объекта, удали из нее атрибут "style" и класс "template".

            Element element = cloneElements.first().getElementsByClass("vacancy").first();// Получи первый элемент и используй его в качестве шаблона для добавления новой строки в таблицу вакансий.
            doc.select("tr[class=vacancy]").remove();; //  Удали все добавленные ранее вакансии. У них единственный класс "vacancy".

            for (Vacancy vacancy :
                    vacancies) {
                Element cloneElement = element.clone();
                cloneElement.getElementsByClass("city").first().appendText(vacancy.getCity());
                cloneElement.getElementsByClass("companyName").first().appendText(vacancy.getCompanyName());
                cloneElement.getElementsByClass("salary").first().appendText(vacancy.getSalary());
                cloneElement.getElementsByTag("a").first().appendText(vacancy.getTitle()).attr("href", vacancy.getUrl());

                cloneElement.getElementsByClass("skills").html(vacancy.getSkills()); // delete for validation

                elements.before(cloneElement.outerHtml());
            }
        } catch (IOException e) {
            e.printStackTrace();
            return "Some exception occurred";
        }
        return doc.html();
    }

    private void updateFile(String content) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath));) {
            writer.write(content);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected Document getDocument() throws IOException {
        return Jsoup.parse(new File(filePath), "UTF-8");
    }
}
