package com.javarush.task.task28.task2810.model;

import com.javarush.task.task28.task2810.vo.Vacancy;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HHStrategy implements Strategy {
//    private static final String URL_FORMAT = "https://grc.ua/search/vacancy?text=java+%s&page=%d";
    private static final String URL_FORMAT = "https://grc.ua/search/vacancy?clusters=true&ored_clusters=true&enable_snippets=true&salary=&text=java&page=%d&hhtmFrom=vacancy_search_list";
//    private static final String URL_FORMAT = "https://javarush.ru/testdata/big28data.html";

    @Override
    public List<Vacancy> getVacancies(String searchString) {
        List<Vacancy> vacancies = new ArrayList<>();
        try {
            int page = 0;
            while (true) {
                Document document = getDocument(searchString, page);
//                Elements elements = document.getElementsByAttributeValue("data-qa", "vacancy-serp__vacancy"); // ("data-qa", "vacancy-serp__vacancy vacancy-serp__vacancy_standard");
                Elements elements = document.getElementsByClass("vacancy-serp-item vacancy-serp-item_redesigned");
                // <div class="vacancy-serp-item vacancy-serp-item_redesigned" data-qa="vacancy-serp__vacancy vacancy-serp__vacancy_standard_plus"><div class="vacancy-serp-item-body"><div class="vacancy-serp-item-body__main-info"><div class=""><div class=""><h3 data-qa="bloko-header-3" class="bloko-header-section-3"><span class="resume-search-item__name"><span class="g-user-content" data-page-analytics-experiment-event="vacancy_search_suitable_item"><a data-qa="vacancy-serp__vacancy-title" target="_blank" class="bloko-link" href="https://grc.ua/vacancy/52951179?from=vacancy_search_list&amp;hhtmFrom=vacancy_search_list&amp;query=java%20kiev">Специалист по IT (создание видеокурсов)</a></span></span></h3><div class="bloko-v-spacing bloko-v-spacing_base-1"></div></div><span data-qa="vacancy-serp__vacancy-compensation" class="bloko-header-section-3">5 000 – 10 000 <!-- -->грн.</span><div class="bloko-v-spacing bloko-v-spacing_base-3"></div></div><div class=""><div class="vacancy-serp-item__info"><div data-qa="vacancy-serp__vacancy-work-schedule" class="bloko-text bloko-text_tertiary">Можно работать из дома</div><div class="bloko-text"><div class="vacancy-serp-item__meta-info-company"><a data-qa="vacancy-serp__vacancy-employer" class="bloko-link bloko-link_kind-tertiary" href="/employer/5959639?hhtmFrom=vacancy_search_list">CleverYou</a></div><div class="vacancy-serp-item__meta-info-badges"><div class="vacancy-serp-item__meta-info-link"><a target="_blank" class="bloko-link" href="/article/20099?hhtmFrom=vacancy_search_list"><span class="vacancy-serp-bage-trusted-employer"></span></a></div></div></div><div data-qa="vacancy-serp__vacancy-address" class="bloko-text bloko-text_no-top-indent">Киев</div><div class="bloko-v-spacing bloko-v-spacing_base-3"></div></div></div></div></div><div class=""><div class="vacancy-serp-item__label" data-qa="vacancy-label-item-first"><div class="search-result-label search-result-label_few-responses" data-qa="vacancy-label-be-first"><div class="bloko-text">Будьте первыми</div></div><div class="bloko-v-spacing bloko-v-spacing_base-2"></div></div><div class="bloko-v-spacing bloko-v-spacing_base-1"></div></div><div class=""><div class="vacancy-serp-item__info"><div class="g-user-content"><div data-qa="vacancy-serp__vacancy_snippet_responsibility" class="bloko-text"><span>Нас интересуют следующие тематики курсов: </span><span class="highlighted highlighted_short">Java</span><span>, JS, C#, Kotlin, SQL, Python, C, Go, HTML, CSS, администрирование сетей, кибербезопасность и тд.</span></div><div data-qa="vacancy-serp__vacancy_snippet_requirement" class="bloko-text bloko-text_no-top-indent"><span>Понимания структуры предмета, формирование четкой программы курса и умение доносить информацию до аудитории. Составление тестов для автоматической проверкой знаний по...</span></div></div></div><div class="bloko-v-spacing bloko-v-spacing_base-3"></div></div><span data-qa="vacancy-serp__vacancy-date" class="bloko-text bloko-text_tertiary">07.03.2022</span><div class="bloko-v-spacing bloko-v-spacing_base-4"></div><div class="vacancy-serp-item__row vacancy-serp-item__row_controls"><div class="vacancy-serp-item__controls-item vacancy-serp-item__controls-item_response"><a class="bloko-button bloko-button_kind-primary bloko-button_scale-small" data-qa="vacancy-serp__vacancy_response" href="/applicant/vacancy_response?vacancyId=52951179&amp;hhtmFrom=vacancy_search_list"><span>Откликнуться</span></a></div></div></div>
                if(elements.isEmpty()){
                    break;
                }
                page++;
                for (Element element :
                        elements) {
                    Vacancy vacancy = new Vacancy();

                    String title = element.getElementsByAttributeValue("data-qa", "vacancy-serp__vacancy-title").text();
                    vacancy.setTitle(title);

                    String salary = element.getElementsByAttributeValue("data-qa", "vacancy-serp__vacancy-compensation").text();
                    vacancy.setSalary(salary);

                    String city = element.getElementsByAttributeValue("data-qa", "vacancy-serp__vacancy-address").text();
                    vacancy.setCity(city);

                    String companyName = element.getElementsByAttributeValue("data-qa", "vacancy-serp__vacancy-employer").text();
                    vacancy.setCompanyName(companyName);

                    vacancy.setSiteName(URL_FORMAT);

                    String url = element.getElementsByAttributeValue("data-qa", "vacancy-serp__vacancy-title").attr("href");
                    vacancy.setUrl(url);


                    String skills = element.getElementsByAttributeValue("data-qa", "vacancy-serp__vacancy_snippet_requirement").text();
                    vacancy.setSkills(skills);

                    vacancies.add(vacancy);
                }
            }
        } catch (IOException ignore) {
            // one of step in task
        }
        return vacancies;
    }

    protected Document getDocument(String searchString, int page) throws IOException {
        Document doc = Jsoup.connect(String.format(URL_FORMAT, page))
                .userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36")
                .referrer("https://grc.ua/search/vacancy?area=5&fromSearchLine=true&text=java+kiev&page=1&hhtmFrom=vacancy_search_list")
                .get();

        return doc;
    }
}
