package com.javarush.task.task28.task2810.model;

import com.javarush.task.task28.task2810.vo.Vacancy;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HabrCareerStrategy implements Strategy {
//    private static final String URL_FORMAT = "https://career.habr.com/vacancies?q=java+%s&page=%d"; // for validation
    private static final String URL_FORMAT = "https://career.habr.com/vacancies?page=%d&q=java&type=all"; // for all city
//        private static final String URL_FORMAT = "https://javarush.ru/testdata/big28data2.html"; // cache for site

    @Override
    public List<Vacancy> getVacancies(String searchString) {
        List<Vacancy> vacancies = new ArrayList<>();
        try {
            int page = 0;
            while (true) { // true
                Document document = getDocument(searchString, page);
//                Elements elements = document.getElementsByAttributeValue("class", "job"); // for validation
//                elements.addAll(document.getElementsByAttributeValue("class", "job marked")); // for validation

                // For really site
                Elements elements = document.getElementsByClass( "vacancy-card__info");
                if (elements.isEmpty()) {
                    break;
                }
                page++;
                for (Element element :
                        elements) {
                    Vacancy vacancy = new Vacancy();
                    String title = element.getElementsByAttributeValue("class", "vacancy-card__title").text();//title
                    vacancy.setTitle(title);

                    String salary = element.getElementsByAttributeValue("class", "vacancy-card__salary").text(); //salary
                    vacancy.setSalary(salary);

                    String city = element.getElementsByAttributeValue("class", "vacancy-card__meta").text(); //location
                    vacancy.setCity(city);

                    String companyName = element.getElementsByAttributeValue("class", "vacancy-card__company-title").text(); //company_name
                    vacancy.setCompanyName(companyName);

                    vacancy.setSiteName(URL_FORMAT);

                    String url = "https://career.habr.com"+element.getElementsByClass( "vacancy-card__title").first().getElementsByTag("a").attr("href"); // title
                    vacancy.setUrl(url);

                    String skills = element.getElementsByAttributeValue("class", "vacancy-card__skills").text();
                    vacancy.setSkills(skills);

                    vacancies.add(vacancy);
                }
            }
        } catch (IOException ignore) {
            // one of step in task
        }
        return vacancies;
    }

    protected Document getDocument(String searchString, int page) throws IOException {
        Document doc = Jsoup.connect(String.format(URL_FORMAT, page)) // for validation add searchString in parameter
                .userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36")
                .referrer("https://career.habr.com/")
                .get();
        return doc;
    }
}
