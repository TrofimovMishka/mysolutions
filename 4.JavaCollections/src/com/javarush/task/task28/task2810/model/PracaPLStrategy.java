package com.javarush.task.task28.task2810.model;

import com.javarush.task.task28.task2810.vo.Vacancy;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


// work not finish - need some details fixing bag
public class PracaPLStrategy implements Strategy{
    private static final String URL_FORMAT = "https://www.praca.pl/s-java,developer_%d.html?p=java+developer";

    @Override
    public List<Vacancy> getVacancies(String searchString) {
        List<Vacancy> vacancies = new ArrayList<>();
        try {
            int page = 1;
            while (page < 3) { // true
                Document document = getDocument(searchString, page);
//                Elements elements = document.getElementsByAttributeValue("class", "job"); // for validation
//                elements.addAll(document.getElementsByAttributeValue("class", "job marked")); // for validation

                // For really site
                Elements elements = document.getElementsByClass( "listing__item");
                if (elements.isEmpty()) {
                    break;
                }
                page++;
                for (Element element :
                        elements) {
                    Vacancy vacancy = new Vacancy();
                    String title = element.getElementsByAttributeValue("class", "listing__title").text();//title
                    vacancy.setTitle(title);

//                    String salary = element.getElementsByAttributeValue("class", "vacancy-card__salary").text(); //salary
//                    vacancy.setSalary(salary);

                    String city = element.getElementsByClass("fas fa-circle listing__dot").first().text(); //
                    vacancy.setCity(city);

                    String companyName = element.getElementsByAttributeValue("class", "listing__origin").text(); //company_name
                    vacancy.setCompanyName(companyName);

                    vacancy.setSiteName(URL_FORMAT);

                    String url = element.getElementsByClass( "listing__title").first().attr("href"); // title
                    vacancy.setUrl(url);

                    String skills = element.getElementsByAttributeValue("class", "listing__main-details").text();
                    vacancy.setSkills(skills);

                    vacancies.add(vacancy);
                }
            }
        } catch (IOException ignore) {
            // one of step in task
        }
        return vacancies;
    }

    protected Document getDocument(String searchString, int page) throws IOException {
        Document doc = Jsoup.connect(String.format(URL_FORMAT, page))
                .userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36")
                .referrer("https://career.habr.com/")
                .get();
        return doc;
    }
}