package com.javarush.task.task32.task3213;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

/* 
Шифр Цезаря
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        StringReader reader = new StringReader("Khoor#Dpljr#&C,₷B'3");
        System.out.println(decode(reader, -3));  //Hello Amigo #@)₴?$0
    }

    public static String decode(StringReader reader, int key){
        StringBuilder sb;
        String result = "";
        try (BufferedReader bufferedReader = new BufferedReader(reader)) {
            sb = new StringBuilder();
            int b;
            while ((b = bufferedReader.read()) > 0) {
                sb.append((char) (b + key));
            }
            result = sb.toString();
        } catch (Exception e) {

        }
        return result;
    }
}
