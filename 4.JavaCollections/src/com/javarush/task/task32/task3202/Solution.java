package com.javarush.task.task32.task3202;

import java.io.*;

/* 
Читаем из потока
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        StringWriter writer = getAllDataFromInputStream(new FileInputStream("testFile.log"));
        System.out.println(writer.toString());
    }

    public static StringWriter getAllDataFromInputStream(InputStream is) {
        StringWriter writer = new StringWriter();
        String fromFile = "";
        try (BufferedInputStream inputStream = new BufferedInputStream(is)) {
            byte[] buffer = new byte[inputStream.available()];
            inputStream.read(buffer);

            StringBuilder sb = new StringBuilder();
            for (byte b :
                    buffer) {
                sb.append((char) b);
            }
            fromFile = sb.toString();
            writer.write(fromFile);

        } catch (IOException e) {

        }
        return writer;
    }
}
