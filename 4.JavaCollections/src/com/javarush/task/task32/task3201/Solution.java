package com.javarush.task.task32.task3201;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/* 
Запись в существующий файл
"C:\Users\Mike\OneDrive\Рабочий стол\Files_for_tasks\RandomAF.txt" "15" "BBBBBBBBBBB"
*/

public class Solution {
    public static void main(String... args) throws FileNotFoundException {
        if (args.length < 2){
            return;
        }
        String fileName = args[0];
        long number = Long.parseLong(args[1]);
        String text = args[2];

        try (RandomAccessFile accessFile = new RandomAccessFile(fileName, "rw")) {
            if(accessFile.length() < number){
                accessFile.seek(accessFile.length());
                accessFile.write(text.getBytes());
            }else{
                accessFile.seek(number);
                accessFile.write(text.getBytes());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
