package com.javarush.task.task32.task3210;

import java.io.IOException;
import java.io.RandomAccessFile;

/* 
Используем RandomAccessFile
"C:\Users\Mike\OneDrive\Рабочий стол\Files_for_tasks\RandomAF.txt" "5" "Hello"
*/

public class Solution {
    public static void main(String... args) {
        if (args.length < 2) {
            return;
        }
        String fileName = args[0];
        long number = Long.parseLong(args[1]);
        String text = args[2];

        try (RandomAccessFile accessFile = new RandomAccessFile(fileName, "rw")) {
            byte[] buffer = new byte[text.length()];
            //read(byte[] b, int off, int len)
            accessFile.seek(number);
            accessFile.read(buffer, 0, text.length());

            StringBuilder sb = new StringBuilder();
            for (byte b :
                    buffer) {
                sb.append((char)b);
            }
            String fromFile = sb.toString();
            System.out.println(fromFile);
            accessFile.seek(accessFile.length());
            if (fromFile.equals(text)) {
                accessFile.write("true".getBytes());
            } else {
                accessFile.write("false".getBytes());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
