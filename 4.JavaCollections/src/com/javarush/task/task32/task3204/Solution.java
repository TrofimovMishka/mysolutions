package com.javarush.task.task32.task3204;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* 
Генератор паролей
*/

public class Solution {
    public static void main(String[] args) {
        ByteArrayOutputStream password = getPassword();
        System.out.println(password.toString());
    }

    public static ByteArrayOutputStream getPassword() {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        String pasword = "";
        while (!(isCorrect(pasword))){
            pasword = getString();
        }
        try {
            stream.write(pasword.getBytes());
        } catch (IOException e) {

        }
        return stream;
    }

    public static boolean isCorrect(String pasword) {
        boolean isNumber = false;
        boolean isUpper = false;
        boolean isLower = false;

        if (pasword.matches(".*[0-9].*")) {
            isNumber = true;
        }
        if (pasword.matches(".*[A-Z].*")) {
            isUpper = true;
        }
        if (pasword.matches(".*[a-z].*")) {
            isLower = true;
        }

        if (isLower && isNumber && isUpper) {
            return true;
        }
        return false;
    }

    static String getString() {
        StringBuilder sb = new StringBuilder();
        List list = new ArrayList();
        String pasword = "";

        for (int i = 0; i < 15; i++) {
            int number = (int) Math.round(Math.random() * 9);
            list.add(number);
            list.add((char) (int) Math.round(Math.random() * (90 - 65) + 65));
            list.add((char) (int) Math.round(Math.random() * (122 - 97) + 97));
        }

        Collections.shuffle(list);
        for (int i = 0; i < list.size(); i++) {
            sb.append(list.get(i));
        }

        String result1 = sb.toString();
        int y = (int) Math.round(Math.random() * 9);
        int x = 8 + y;
        pasword = result1.substring(y, x);

        return pasword;
    }
}
