package com.javarush.task.task32.task3205;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class CustomInvocationHandler implements InvocationHandler {
   private SomeInterfaceWithMethods someObj;

    public CustomInvocationHandler(SomeInterfaceWithMethods someObj) {
        this.someObj = someObj;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        System.out.println(method.getName()+" in");
        Object obj = method.invoke(someObj, args);
        System.out.println(method.getName()+" out");

        return obj;
    }
}
