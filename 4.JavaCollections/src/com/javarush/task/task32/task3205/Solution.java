package com.javarush.task.task32.task3205;

import java.lang.reflect.Proxy;

/* 
Создание прокси-объекта
*/

public class Solution {
    public static void main(String[] args) {
        SomeInterfaceWithMethods obj = getProxy();
        obj.stringMethodWithoutArgs();
        obj.voidMethodWithIntArg(1);

        /* expected output
        stringMethodWithoutArgs in
        inside stringMethodWithoutArgs
        stringMethodWithoutArgs out
        voidMethodWithIntArg in
        inside voidMethodWithIntArg
        inside voidMethodWithoutArgs
        voidMethodWithIntArg out
        */
    }

    public static SomeInterfaceWithMethods getProxy() {
        SomeInterfaceWithMethodsImpl withMethod = new SomeInterfaceWithMethodsImpl();
        ClassLoader siwm = withMethod.getClass().getClassLoader();
        Class [] interfaces = withMethod.getClass().getInterfaces();

        SomeInterfaceWithMethods proxy = (SomeInterfaceWithMethods)Proxy
                .newProxyInstance(siwm, interfaces, new CustomInvocationHandler(withMethod));
        return proxy;
    }
}
