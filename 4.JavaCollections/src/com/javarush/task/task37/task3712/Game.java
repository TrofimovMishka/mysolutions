package com.javarush.task.task37.task3712;

abstract class Game {
    abstract void prepareForTheGame();
    abstract void playGame();
    abstract void congratulateWinner();
    public void run(){
        prepareForTheGame();
        playGame();
        congratulateWinner();
    }
}
