package com.javarush.task.task37.task3707;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.*;

class AmigoSet<E> extends AbstractSet<E> implements Set<E>, Serializable, Cloneable {
    private static final Object PRESENT = new Object();
    private transient HashMap<E, Object> map;

    public AmigoSet() {
        this.map = new HashMap<>();
    }

    @Override
    public boolean add(E e) {
        return  map.put(e, PRESENT) == null;
    }

    public AmigoSet(Collection<? extends E> collection) {
        this.map = new HashMap<E, Object>((int)Math.max(16, Math.ceil(collection.size()/0.75F)));
        addAll(collection);
    }

    @Override
    public Iterator iterator() {
        return map.keySet().iterator();
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return map.containsKey(o);
    }

    @Override
    public boolean remove(Object o) {
        return map.remove(o)!=null;
    }

    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public Object clone() {
        try{
            AmigoSet<E> newSet = (AmigoSet<E>) super.clone();
            newSet.map = (HashMap<E, Object>) map.clone();
            return newSet;
        }catch (Exception e){
            throw  new InternalError(e);
        }
    }

    private void writeObject(ObjectOutputStream o){
        try{
            o.defaultWriteObject();
            o.writeInt(HashMapReflectionHelper.callHiddenMethod(map, "capacity"));
            o.writeFloat(HashMapReflectionHelper.callHiddenMethod(map, "loadFactor"));
            o.writeInt(map.size());
            for (E e :
                    map.keySet()) {
                o.writeObject(e);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readObject(ObjectInputStream ois){
        try{
            ois.defaultReadObject();
            int capacity = ois.readInt();
            float loadFactor = ois.readFloat();
            int size = ois.readInt();

            this.map = new HashMap<>(capacity, loadFactor);
            for (int i = 0; i < size; i++) {
                map.put((E)ois.readObject(), PRESENT);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
