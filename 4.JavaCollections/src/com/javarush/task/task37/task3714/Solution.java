package com.javarush.task.task37.task3714;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/* 
Древний Рим - I обозначает 1, V обозначает 5, X — 10, L — 50, C — 100, D — 500, M — 1000.
XXIX - 29
XIV- 14

*/

public class Solution {
    private static Map<String, Integer> map = new HashMap<>();
    static{
        map.put("I", 1);
        map.put("V", 5);
        map.put("X", 10);
        map.put("L", 50);
        map.put("C", 100);
        map.put("D", 500);
        map.put("M", 1000);
    }

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Input a roman number to be converted to decimal: ");
        String romanString = bufferedReader.readLine();
        System.out.println("Conversion result equals " + romanToInteger(romanString));
//        test();
    }

    public static int romanToInteger(String s) {
        List<Integer> integers = new ArrayList<>();
        String[] arr = s.split("");
        for (String str :
                arr) {
            integers.add(map.get(str));
        }
        int result = 0;
        for (int i = integers.size()-1; i >= 0; i--) {
            int element = integers.get(i);
            boolean isLess = false;
            for (int j = i; j < integers.size(); j++) {
                if(element < integers.get(j)){
                    isLess = true;
                    break;
                }
            }
            if(isLess){
                result -= element;
            }else{
                result += element;
            }
        }
        return result;
    }

    private static void test() {
        Object[][] tests = new Object[][]{
                {1, "I"},
                {2, "II"},
                {3, "III"},
                {4, "IV"},
                {5, "V"},
                {9, "IX"},
                {12, "XII"},
                {16, "XVI"},
                {29, "XXIX"},
                {44, "XLIV"},
                {45, "XLV"},
                {68, "LXVIII"},
                {83, "LXXXIII"},
                {97, "XCVII"},
                {99, "XCIX"},
                {500, "D"},
                {501, "DI"},
                {649, "DCXLIX"},
                {798, "DCCXCVIII"},
                {891, "DCCCXCI"},
                {1000, "M"},
                {1004, "MIV"},
                {1006, "MVI"},
                {1023, "MXXIII"},
                {2014, "MMXIV"},
                {3999, "MMMCMXCIX"}
        };

        Arrays.stream(tests)
                .forEach(objects -> {
                            final int expected = (Integer) objects[0];
                            final String input = (String) objects[1];
                            final int real = romanToInteger(input);

                            System.out.printf(
                                    "input: %10s\t\texpected: %5s\t\treal: %5s\t\tTest: %5s%n",
                                    input, expected, real, expected == real);
                        }
                );
    }
}
