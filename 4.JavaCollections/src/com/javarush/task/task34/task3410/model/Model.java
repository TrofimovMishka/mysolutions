package com.javarush.task.task34.task3410.model;

import com.javarush.task.task34.task3410.controller.EventListener;

import java.nio.file.Paths;

public class Model {
    public static final int FIELD_CELL_SIZE = 20;
    private EventListener eventListener;
    private GameObjects gameObjects;
    private int currentLevel = 1;
    private LevelLoader levelLoader = new LevelLoader(Paths.get("C:\\Users\\Mike\\Documents\\JavaRushTasks\\4.JavaCollections\\src\\com\\javarush\\task\\task34\\task3410\\res\\levels.txt"));

    public void setEventListener(EventListener eventListener) {
        this.eventListener = eventListener;
    }

    public GameObjects getGameObjects() {
        return gameObjects;
    }

    public void restartLevel(int level) {
        gameObjects = levelLoader.getLevel(level);
    }

    public void restart() {
        restartLevel(currentLevel);
    }

    public void startNextLevel() {
        currentLevel++;
        restart();
    }

    public void move(Direction direction) {
        Player player = gameObjects.getPlayer();
        if (!checkWallCollision(player, direction) && !checkBoxCollisionAndMoveIfAvailable(direction)) {
            int[] coordinates = getNewCoordinateAfterMove(player, direction);
            player.move(coordinates[0], coordinates[1]);
            checkCompletion();
        }
    }

    public boolean checkWallCollision(CollisionObject gameObject, Direction direction) {
        return gameObjects
                .getWalls()
                .stream()
                .anyMatch(wall -> gameObject.isCollision(wall, direction));
    }

    public boolean checkBoxCollisionAndMoveIfAvailable(Direction direction) {
        Player player = gameObjects.getPlayer();
        GameObject collisionObject = getCollisionObject(player, direction);

        if (collisionObject instanceof Box
                && (checkWallCollision((Box) collisionObject, direction)
                || checkBoxCollision((Box) collisionObject, direction))) {
            return true;
        }

        if (collisionObject instanceof Box) {
            int[] boxCoordinates = getNewCoordinateAfterMove(collisionObject, direction);
            int boxX = boxCoordinates[0];
            int boxY = boxCoordinates[1];
            ((Box) collisionObject).move(boxX, boxY);
            return false;
        }
        return false;
    }

    public void checkCompletion() {
        long homesWithoutBox = gameObjects
                .getHomes()
                .stream()
                .filter(home -> {
                    for (Box box : gameObjects.getBoxes()) {
                        if (box.getX() == home.getX() && box.getY() == home.getY()) {
                            return false;
                        }
                    }
                    return true;
                }).count();

        if (homesWithoutBox == 0) {
            eventListener.levelCompleted(currentLevel);
        }
    }

    private int[] getNewCoordinateAfterMove(GameObject object, Direction direction) {
        int x = 0;
        int y = 0;

        switch (direction) {
            case UP:
                x = object.getX();
                y = object.getY() - FIELD_CELL_SIZE;
                break;
            case DOWN:
                x = object.getX();
                y = object.getY() + FIELD_CELL_SIZE;
                break;
            case LEFT:
                x = object.getX() - FIELD_CELL_SIZE;
                y = object.getY();
                break;
            case RIGHT:
                x = object.getX() + FIELD_CELL_SIZE;
                y = object.getY();
                break;
        }
        return new int[]{x, y};
    }

    private boolean checkBoxCollision(CollisionObject gameObject, Direction direction) {
        return gameObjects
                .getBoxes()
                .stream()
                .anyMatch(box -> gameObject.isCollision(box, direction));
    }

    private GameObject getCollisionObject(Player player, Direction direction) {
        return gameObjects
                .getAll()
                .stream()
                .filter(object -> player.isCollision(object, direction) && !(object instanceof Home))
                .findFirst().orElse(null);
    }
}
