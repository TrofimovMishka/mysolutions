package com.javarush.task.task34.task3410.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class LevelLoader {
    private static final char WALL = 'X';
    private static final char BOX = '*';
    private static final char HOME = '.';
    private static final char BOX_IN_HOME = '&';
    private static final char PLAYER = '@';
    private static final int MAX_LEVEL = 60;
    private final Path levels;
    private final List<String> stringsLevels = new ArrayList<>();

    public LevelLoader(Path levels) {
        this.levels = levels;
    }

    public GameObjects getLevel(int level) {
        if(level > MAX_LEVEL){ level = level - MAX_LEVEL; }
        readGameObjectsFromFile(level);
        return createGameObjects();
    }

    private void readGameObjectsFromFile(int level) {
        //BufferedReader reader = Files.newBufferedReader(levels)
        //For validator:
        try (BufferedReader reader = new BufferedReader(new FileReader(levels.toString()))) {
            boolean isLevelWhatNeed = false;
            while (!isLevelWhatNeed) {
                String line = reader.readLine();
                if (line.contains("Maze: " + level)) {
                    String skipLine = reader.readLine();
                    while (!skipLine.isEmpty()) {
                        skipLine = reader.readLine();
                    }
                    loadGameObjectsAsStringFromFile(reader);
                    isLevelWhatNeed = true;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private GameObjects createGameObjects() {
        int x = Model.FIELD_CELL_SIZE / 2;
        int y = Model.FIELD_CELL_SIZE / 2;

        Set<Wall> walls = new HashSet<>();
        Set<Box> boxes = new HashSet<>();
        Set<Home> homes = new HashSet<>();
        Player player = null;

        for (String str : stringsLevels) {
            char[] objectsAsChars = str.toCharArray();
            for (char object : objectsAsChars) {
                switch (object) {
                    case WALL:
                        walls.add(new Wall(x, y));
                        break;
                    case BOX:
                        boxes.add(new Box(x, y));
                        break;
                    case HOME:
                        homes.add(new Home(x, y));
                        break;
                    case PLAYER:
                        player = new Player(x, y);
                        break;
                    case BOX_IN_HOME:
                        homes.add(new Home(x, y));
                        boxes.add(new Box(x, y));
                        break;
                }
                x += Model.FIELD_CELL_SIZE;
            }
            x = Model.FIELD_CELL_SIZE / 2;
            y += Model.FIELD_CELL_SIZE;
        }
        return new GameObjects(walls, boxes, homes, player);
    }

    private void loadGameObjectsAsStringFromFile(BufferedReader reader) throws IOException {
        String data = reader.readLine();
        while (!data.isEmpty()) {
            stringsLevels.add(data);
            data = reader.readLine();
        }
    }
}
