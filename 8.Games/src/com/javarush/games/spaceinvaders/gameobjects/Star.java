package com.javarush.games.spaceinvaders.gameobjects;

import com.javarush.engine.cell.*;

public class Star extends GameObject {
    public Star(double x, double y) {
        super(x, y);
    }
    private static final String STAR_SIGN = "\u2736"; // \u2605 - by default

    public void draw(Game game){
        game.setCellValueEx((int)this.x, (int)this.y, Color.NONE, STAR_SIGN, Color.RED, 100);
    }
}
