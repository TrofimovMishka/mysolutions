package com.javarush.games.spaceinvaders;

import com.javarush.engine.cell.*;
import com.javarush.games.spaceinvaders.gameobjects.Star;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class SpaceInvadersGame extends Game {
    public static final int WIDTH = 64;
    public static final int HEIGHT = 64;
    private List<Star> stars;

    @Override
    public void initialize() {
        setScreenSize(WIDTH, HEIGHT);
        createGame();
    }

    private void drawField(){
        for (int y = 0; y < HEIGHT; y++) {
            for (int x = 0; x < WIDTH; x++) {
                setCellValueEx(x, y, Color.BLACK, "");
            }
        }
        stars.forEach(ele -> ele.draw(this));
    }
    private void createGame(){
        createStars();
        drawScene();
    }

    private void drawScene(){drawField();}

    private void createStars(){
        stars = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            int x = ThreadLocalRandom.current().nextInt(5, 59);
            int y = ThreadLocalRandom.current().nextInt(5, 59);
            stars.add(new Star(x, y));
        }
    }

}
