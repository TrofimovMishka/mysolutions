package com.javarush.games.minesweeper;

import com.javarush.engine.cell.Color;
import com.javarush.engine.cell.Game;

import java.util.ArrayList;
import java.util.List;

public class MinesweeperGame extends Game {
    private static final int SIDE = 9;
    private GameObject[][] gameField = new GameObject[SIDE][SIDE];
    private int countMinesOnField;
    private static final String MINE = "\uD83D\uDCA3"; // picture for mine
    private static final String FLAG = "\uD83D\uDEA9"; // picture for flag
    private int countFlags;
    private boolean isGameStopped;
    private int countClosedTiles = SIDE * SIDE;
    private int score;

    @Override
    public void initialize() {
        setScreenSize(SIDE, SIDE);
        createGame();
    }

    private void createGame() {
//        isGameStopped = false;
        for (int y = 0; y < SIDE; y++) {
            for (int x = 0; x < SIDE; x++) {
                boolean isMine = getRandomNumber(10) < 1;
                if (isMine) {
                    countMinesOnField++;
                }
                gameField[y][x] = new GameObject(x, y, isMine);
                setCellColor(x, y, Color.ORANGE);
                setCellValue(x, y, "");
            }
        }
        countFlags = countMinesOnField;
        countMineNeighbors();
    }

    private List<GameObject> getNeighbors(GameObject gameObject) {
        List<GameObject> result = new ArrayList<>();
        for (int y = gameObject.y - 1; y <= gameObject.y + 1; y++) {
            for (int x = gameObject.x - 1; x <= gameObject.x + 1; x++) {
                if (y < 0 || y >= SIDE) {
                    continue;
                }
                if (x < 0 || x >= SIDE) {
                    continue;
                }
                if (gameField[y][x] == gameObject) {
                    continue;
                }
                result.add(gameField[y][x]);
            }
        }
        return result;
    }

    private void countMineNeighbors() {
        for (int y = 0; y < SIDE; y++) {
            for (int x = 0; x < SIDE; x++) {
                GameObject obj = gameField[y][x];
                if (!obj.isMine) {
                    List<GameObject> result = getNeighbors(gameField[y][x]);
                    for (int i = 0; i < result.size(); i++) {
                        if (result.get(i).isMine) {
                            obj.countMineNeighbors++;
                        }
                    }
                }
            }
        }
    }

    private void openTile(int x, int y) {
        GameObject obj = gameField[y][x];
        if (obj.isOpen || obj.isFlag || isGameStopped) {
            return;
        }
        obj.isOpen = true;
        countClosedTiles--;
        setCellColor(x, y, Color.GREEN);
        if (obj.isMine) {
            setCellValueEx(x, y, Color.RED, MINE);
            gameOver();
        } else {
            score += 5;
            setScore(score);
            setCellNumber(x, y, obj.countMineNeighbors);
            if (obj.countMineNeighbors == 0) {
                setCellValue(obj.x, obj.y, "");
                List<GameObject> list = getNeighbors(obj);
                for (GameObject obj2 :
                        list) {
                    if (!obj2.isOpen) {
                        openTile(obj2.x, obj2.y);
                    }
                }
            }
            if (countClosedTiles == countMinesOnField){
                win();
            }
        }
    }

    @Override
    public void onMouseLeftClick(int x, int y) {
        super.onMouseLeftClick(x, y);
        if(isGameStopped){
            restart();
            return;
        }
        openTile(x, y);
    }

    private void markTile(int x, int y) {
        GameObject obj = gameField[y][x];
        if (obj.isOpen || (countFlags == 0 && !obj.isFlag) || isGameStopped) {
            return;
        }
        if (!obj.isFlag) {
            obj.isFlag = true;
            countFlags--;
            setCellValue(x, y, FLAG);
            setCellColor(x, y, Color.YELLOW);
        } else {
            obj.isFlag = false;
            countFlags++;
            setCellValue(x, y, "");
            setCellColor(x, y, Color.ORANGE);
        }
    }

    @Override
    public void onMouseRightClick(int x, int y) {
        super.onMouseRightClick(x, y);
        markTile(x, y);
    }

    private void gameOver() {
        if (isGameStopped) {
            return;
        }
        isGameStopped = true;
        showMessageDialog(Color.RED, "GAME OVER", Color.WHITE, 100);
    }

    private void win() {
        isGameStopped = true;
        showMessageDialog(Color.AQUAMARINE, "WINNER", Color.GREEN, 150);
    }

    private void restart(){
        isGameStopped = false;
        countClosedTiles = SIDE*SIDE;
        score = 0;
        setScore(score);
        countMinesOnField = 0;
        createGame();
    }
}
